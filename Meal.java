
public class Meal {

	
	
	public String type;
	public String food;
	public String drink;
	
	public Meal(){
	
	}

	public Meal(String type, String food, String drink) {
		
		this.type = type;
		this.food = food;
		this.drink = drink;
	}

	public Meal(String type, String food) {
		this.type = type;
		this.food = food;
	}

	public Meal(String drink) {
		super();
		this.drink = drink;
	}
	
	public void getCalorie(){
		//This needs to be implemented when database does.
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFood() {
		return food;
	}

	public void setFood(String food) {
		this.food = food;
	}

	public String getDrink() {
		return drink;
	}

	public void setDrink(String drink) {
		this.drink = drink;
	}

	@Override
	public String toString() {
		return "Meal [type=" + type + ", food=" + food + ", drink=" + drink
				+ "]";
	}
	
	
	
	
}