﻿DROP TABLE ArchiveActivity;
DROP TABLE History;
DROP TABLE Membership;
DROP TABLE Groups;
DROP TABLE Meal;
DROP TABLE Activity;
DROP TABLE Achievments;
DROP TABLE Goal;
DROP TABLE User_Data;
DROP TABLE Glossary;

CREATE TABLE User_Data

(

	username	VARCHAR(20) NOT NULL  PRIMARY KEY,

	password	VARCHAR(150),

	firstName	VARCHAR(20),

	lastName	VARCHAR(20),

	email		VARCHAR(30),

	sex		VARCHAR(2)
			CHECK(sex = 'm' OR sex = 'f' OR sex = 'o'),

	age		INTEGER,

	weight		INTEGER DEFAULT 0,

	height		INTEGER DEFAULT 0,

	heartrate	INTEGER DEFAULT 0

);
ALTER TABLE user_data
OWNER TO dbuser;

INSERT INTO User_Data VALUES('odie', 'xxx', 'Buster', 'McThunderStick', 'BM@yahoo.com', 'm', 18, 65, 184, 132);
-- SELECT * FROM user_data;


CREATE TABLE Goal

(

	goalOwner		VARCHAR(20) NOT NULL,

	goalName		VARCHAR(40) NOT NULL,

	startDate		Date NOT NULL,

	dueDate			Date NOT NULL,

	goalType		VARCHAR(2) NOT NULL
				CHECK(goalType = 'F' OR goalType = 'W' OR goalType = 'C'OR goalType = 'D'),

	targetWeight		INTEGER DEFAULT 0,

	targetDistance		INTEGER DEFAULT 0,

	targetHeartRate		INTEGER DEFAULT 0,

	targetBMI		INTEGER DEFAULT 0,

	activityType     	VARCHAR(20) DEFAULT 'NA'
				CHECK(activityType = 'W' or activityType = 'R' or activityType = 'B' or activityType = 'custom' or activityType = 'NA'),

	achievedDistance	INTEGER DEFAULT 0,

	isGroup INTEGER DEFAULT 0,

	CONSTRAINT Goal_PrimaryKey PRIMARY KEY(goalOwner, goalName)
	
	-- goalOwner is conceptually a foreign key as it is either a username for a user's goal or a group name for a group goal. --
	-- We cannot explicitly add the foreign key constraint because it would reference from 2 tables User or Group. --
	-- But just keep in mind it is. --

);
ALTER TABLE Goal
OWNER TO dbuser;

-- Parameters are username, distance covered and activityType
CREATE OR REPLACE FUNCTION updateAchievedDistance(TEXT, INTEGER, TEXT) RETURNS VOID AS

$$

	UPDATE Goal SET achievedDistance = achievedDistance + $2 WHERE goalOwner = $1 AND targetDistance != 0 AND activityType = $3; -- this factors in custom goals

$$

LANGUAGE SQL;


CREATE TABLE Activity

(

	username		VARCHAR(20) NOT NULL,

	distance		INTEGER NOT NULL,

	duration		INTEGER NOT NULL,

	calories_burned		INTEGER,

	date			Date NOT NULL,

	activityType     	VARCHAR(20)
				CHECK(activityType = 'W' or activityType = 'R' or activityType = 'B' or activityType = 'custom'),

	activityID		SERIAL,

	CONSTRAINT Activity_PrimaryKey PRIMARY KEY(username, activityID),

	FOREIGN KEY(username) REFERENCES User_Data(username) ON DELETE CASCADE ON UPDATE CASCADE
	

);
ALTER TABLE Activity
OWNER TO dbuser;


--Archive table for activities for once it has updated a users goal
CREATE TABLE ArchiveActivity

(

	username		VARCHAR(20) NOT NULL,

	distance		INTEGER NOT NULL,

	duration		INTEGER NOT NULL,

	--speed			INTEGER,

	calories_burned		INTEGER,

	date			Date NOT NULL,

	activityType     	VARCHAR(20)
				CHECK(activityType = 'W' or activityType = 'R' or activityType = 'B' or activityType = 'custom'),

	activityID		SERIAL,

	FOREIGN KEY(username) REFERENCES User_Data(username) ON DELETE CASCADE ON UPDATE CASCADE
	

);
ALTER TABLE ArchiveActivity
OWNER TO dbuser;


-- Meal stores the foods and drinks that the user can select (in addition to custom foods/drinks added by users)
CREATE TABLE Meal

(

	username		VARCHAR(20) NOT NULL,

	mealName		VARCHAR(20) NOT NULL,

	type			VARCHAR(5) NOT NULL
				CHECK(type = 'food' OR type = 'drink'),

	calories		INTEGER NOT NULL,

	CONSTRAINT Meal_PrimaryKey PRIMARY KEY(username, mealName)

);




ALTER TABLE Meal
OWNER TO dbuser;

INSERT INTO MEAL VALUES('DEFAULT', 'Rice', 'food', 111);
INSERT INTO MEAL VALUES('DEFAULT', 'Chicken', 'food', 172);
INSERT INTO MEAL VALUES('DEFAULT', 'Chips', 'food', 480);
INSERT INTO MEAL VALUES('DEFAULT', 'Fish', 'food', 87);
INSERT INTO MEAL VALUES('DEFAULT', 'Water', 'drink', 0);
INSERT INTO MEAL VALUES('DEFAULT', 'Coke', 'drink', 184);
INSERT INTO MEAL VALUES('DEFAULT', 'Milk', 'drink', 42);
INSERT INTO MEAL VALUES('DEFAULT', 'Orange Juice', 'drink', 45);


CREATE TABLE Groups

(

	groupName		VARCHAR(40) NOT NULL PRIMARY KEY,

	numberOfMembers		INTEGER

);
ALTER TABLE Groups
OWNER TO dbuser;


-- Membership is an intersection entity to resolve the m:n relationship between the tables User_Data and Group --
CREATE TABLE Membership

(

	username		VARCHAR(20) NOT NULL,

	groupName		VARCHAR(40) NOT NULL,

	role			VARCHAR(2) DEFAULT 'U'
				CHECK(role = 'U' OR role = 'A'),

	permissionsLevel	INTEGER DEFAULT 1
				CHECK(permissionsLevel <= 5),

	CONSTRAINT Membership_PrimaryKey PRIMARY KEY(username, groupName),

	FOREIGN KEY(username) REFERENCES User_Data(username) ON DELETE CASCADE ON UPDATE CASCADE

);
ALTER TABLE Membership
OWNER TO dbuser;

CREATE TABLE Achievments

(

	goalOwner		VARCHAR(20) NOT NULL,

	goalName		VARCHAR(20) NOT NULL,

	achievmentDate		Date NOT NULL,

	goalType		VARCHAR(2) NOT NULL
				CHECK(goalType = 'F' OR goalType = 'W' OR goalType = 'C'OR goalType = 'D'),

	targetWeight		INTEGER DEFAULT 0,

	targetDistance		INTEGER DEFAULT 0,

	targetHeartRate		INTEGER DEFAULT 0,

	targetBMI		INTEGER DEFAULT 0,

	activityType     	VARCHAR(20) DEFAULT 'NA'
				CHECK(activityType = 'W' or activityType = 'R' or activityType = 'B' or activityType = 'custom' or activityType = 'NA'),

	CONSTRAINT Achievment_PrimaryKey PRIMARY KEY(goalOwner, goalName)
	
	-- goalOwner is conceptually a foreign key as it is either a username for a user's goal or a group name for a group goal. --
	-- We cannot explicitly add the foreign key constraint because it would reference from 2 tables User or Group. --
	-- But just keep in mind it is. --

);
ALTER TABLE Achievments
OWNER TO dbuser;

CREATE TABLE History

(

	username 	VARCHAR(20) NOT NULL,

	text		VARCHAR(150) NOT NULL,

	date		Date NOT NULL,

	CONSTRAINT History_PrimaryKey PRIMARY KEY(username, text, date)


	-- goalOwner is conceptually a foreign key as it is either a username for a user's goal or a group name for a group goal. --
	-- We cannot explicitly add the foreign key constraint because it would reference from 2 tables User or Group. --
	-- But just keep in mind it is. --

);
ALTER TABLE History
OWNER TO dbuser;


CREATE TABLE GroupComments

(

	username 	VARCHAR(20) NOT NULL,

	groupName	VARCHAR(20) NOT NULL,
	
	comment		VARCHAR(150) NOT NULL,

	date		Date NOT NULL, 

	CONSTRAINT GroupName_PrimaryKey PRIMARY KEY(username, groupName, comment, date)
);
ALTER TABLE GroupComments
OWNER TO dbuser;


CREATE TABLE Glossary

(
	
	definitionName VARCHAR(30) NOT NULL PRIMARY KEY,

	definition VARCHAR (500) NOT NULL
	
);
ALTER TABLE Glossary
OWNER TO dbuser;

CREATE TABLE Notification

(
	
	
);
ALTER TABLE Notification
OWNER TO dbuser;


-- Tests --

SELECT * FROM Meal;
SELECT * FROM Goal;
SELECT * FROM Activity;
SELECT * FROM Goal WHERE goalOwner = 'odie' AND goalType = 'W' AND dueDate >= '2014-03-08';
SELECT * FROM Groups;
SELECT * FROM Membership;
SELECT * FROM Achievments;
SELECT * FROM GroupComments;
SELECT * FROM History;

SELECT * FROM updateAchievedDistance('odie', 50);
INSERT INTO History VALUES('odie', 'You failed to achieve your Weight Goal Super Goal. But dont give up!', '2014-03-15');

--INSERT INTO Achievments VALUES('odie', 'my distance goal', '2014-03-11', 'D',0,50,0,0,'W');

--DELETE FROM Goal WHERE goalName = 'my distance goal'AND goalOwner = 'odie'

--CREATE TRIGGER archiveActivity AFTER
--SELECT ON Activity FOR EACH STATEMENT
--EXECUTE PROCEDURE archiveActivityFunc();


--CREATE OR REPLACE FUNCTION archiveActivityFunc() 
--RETURNS void AS $$
--	BEGIN
--		INSERT INTO ArchiveActivity VALUES
--		(SELECT * FROM Archive WHERE username = OLD.username);
--	END && LANGUAGE PLPGSQL; 


--All the glossary definitions in INSERT Statements.
INSERT INTO Glossary VALUES ('Body Mass Index (BMI)', 'An approximate measure of whether someone is over- or underweight, calculated by dividing their weight in kilograms by the square of their height in metres.');
INSERT INTO Glossary VALUES ('Diet','The kinds of food that a person, animal, or community habitually eats. ');
INSERT INTO Glossary VALUES ('Calories','Small calorie, abbreviation: cal - the energy needed to raise the temperature of 1 gram of water through 1 °C. Large calorie, abbreviation: Cal) the energy needed to raise the temperature of 1 kilogram of water through 1 °C, equal to one thousand small calories ');
INSERT INTO Glossary VALUES ('Height','The measurement of someone or something from head to foot or from base to top.');
INSERT INTO Glossary VALUES ('Weight','A body’s relative mass or the quantity of matter contained by it, giving rise to a downward force; the heaviness of a person or thing.');
INSERT INTO Glossary VALUES ('Health','The state of being free from illness or injury');
INSERT INTO Glossary VALUES ('Lifestyle','The way in which a person lives.');
INSERT INTO Glossary VALUES ('Exercise','Activity requiring physical effort, carried out to sustain or improve health and fitness.');
INSERT INTO Glossary VALUES ('Food Groups','Carbohydrates, Proteins, Fats, Fibre, Minerals,	Vitamins e.g. vitamin A, B, C, D, E');
INSERT INTO Glossary VALUES ('Aerobic Exercise','Exercise fitness low-intensity, sustained activity that relies on oxygen for energy. Aerobic activity builds endurance, burns fat and conditions the cardiovascular system. To attain an aerobic effect for exercise fitness, you must increase your heart rate to 60-80 percent of your maximum heart rate, and maintain that for at least 20 minutes. Examples of aerobic exercise include running, brisk walking, bicycling, swimming and aerobic dance.');
INSERT INTO Glossary VALUES ('Anaerobic Exercise','High-intensity exercise that burns glycogen for energy, instead of oxygen. Anaerobic exercise creates a temporary oxygen debt by consuming more oxygen than the body can supply. An example of anaerobic exercise includes weight lifting, fitness.');
INSERT INTO Glossary VALUES ('Maximum Heart Rate','The fastest rate at which your heart should beat during exercise. To find your maximum rate, subtract your age from 220.');
INSERT INTO Glossary VALUES ('Target Heart Rate','In aerobics, the speed at which you want to maintain your heartbeat during exercise. Your heart rate should stay between 60 and 80 percent of your maximum heart rate for at least 20 minutes.');
INSERT INTO Glossary VALUES ('Resting Heart Rate','The rate at which your heart beats at rest (while sitting or being inactive), low resting heart rates are a good measure of health and fitness.');
INSERT INTO Glossary VALUES ('Endomorph','A body shape characterized by a round face, short neck, wide hips, and heavy fat storage.');
INSERT INTO Glossary VALUES ('Ectomorph','A body shape characterized by a narrow chest, narrow shoulders and long, thin muscles.');
INSERT INTO Glossary VALUES ('Mesomorph','A body shape characterized by a large chest, long torso, solid muscle structure and significant strength.');
INSERT INTO Glossary VALUES ('Obesity','A weight disorder generally defined as an accumulation of fat beyond that considered normal for a person based on age, sex, and body type.');

