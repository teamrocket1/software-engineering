<%-- 
    Document   : GroupGoals
    Created on : 30-Mar-2014, 14:39:03
    Author     : jsz12txu
--%>

<%@page import="Model.DistanceGoal"%>
<%@page import="Model.CustomGoal"%>
<%@page import="Model.FitnessGoal"%>
<%@page import="java.util.Iterator"%>
<%@page import="Model.WeightGoal"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <script type="text/javascript">

            function goalCheck() {
                var dropdown = document.getElementById("goalSelect");
                var selected = dropdown.options[dropdown.selectedIndex].value;

                if (selected == 0) {
                    document.getElementById('weightForm').style.display = 'block';
                    document.getElementById('fitnessForm').style.display = 'none';
                    document.getElementById('customForm').style.display = 'none';
                    document.getElementById('distanceForm').style.display = 'none';


                    document.getElementById("weightType").value = "set";
                    document.getElementById("fitnessType").value = "!set";
                    document.getElementById("customType").value = "!set";
                    document.getElementById("customType").value = "!set";
                }

                if (selected == 1) {
                    document.getElementById('weightForm').style.display = 'none';
                    document.getElementById('fitnessForm').style.display = 'block';
                    document.getElementById('customForm').style.display = 'none';
                    document.getElementById('distanceForm').style.display = 'none';

                    document.getElementById("weightType").value = "!set";
                    document.getElementById("fitnessType").value = "set";
                    document.getElementById("customType").value = "!set";
                    document.getElementById("customType").value = "!set";
                }

                if (selected == 2) {
                    document.getElementById('weightForm').style.display = 'none';
                    document.getElementById('fitnessForm').style.display = 'none';
                    document.getElementById('customForm').style.display = 'block';
                    document.getElementById('distanceForm').style.display = 'none';

                    document.getElementById("weightType").value = "!set";
                    document.getElementById("fitnessType").value = "!set";
                    document.getElementById("customType").value = "set";
                    document.getElementById("distanceType").value = "!set";
                }

                if (selected == 3) {
                    document.getElementById('weightForm').style.display = 'none';
                    document.getElementById('fitnessForm').style.display = 'none';
                    document.getElementById('customForm').style.display = 'none';
                    document.getElementById('distanceForm').style.display = 'block';

                    document.getElementById("weightType").value = "!set";
                    document.getElementById("fitnessType").value = "!set";
                    document.getElementById("customType").value = "!set";
                    document.getElementById("distanceType").value = "set";
                }

            }

        </script>
        
        
        <jsp:useBean id="aUser" type="Model.User" scope="session" />
        <jsp:useBean id="aGroup" type="Model.Group" scope="session" />
        <jsp:useBean id="weightGoals" type="ArrayList<WeightGoal>" scope="session" />
        <jsp:useBean id="distanceGoals" type="ArrayList<DistanceGoal>" scope="session" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Group Goals Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        
        
        
        
        
        <!--Displaying and deleting goals-->
        
        <form action="/HealthTracker/GroupGoalController" method="POST">
            <input type="hidden" name="groupName" value="${aGroup.groupName}"/>
        <table border="1">                
            <tr>
                <td>Goal Name</td>
                <td>Start Date</td>
                <td>Due Date</td>
                <td>Goal Type</td>
                <td>Percent Complete</td>
                <td>Delete Goal?</td>
            </tr>
            <%
                if (session.getAttribute("weightGoals") != null) {

                    ArrayList<WeightGoal> wg = (ArrayList<WeightGoal>) session.getAttribute("weightGoals");
                    Iterator it = wg.iterator();
                    while (it.hasNext()) {

                        WeightGoal g = (WeightGoal) it.next();

                        double v = (double) g.getTargetWeight();
                        double u = (double) aUser.getWeight();
                        double completion = 0.0;

                        if (u > v) {
                            completion = 100 - (((u - v + 1) / u) * 100);
                        } else {
                            completion = 100 + (((u - v + 1) / u) * 100);
                        }


            %>

            <tr>
                <td><a href="/HealthTracker/GroupGoalController?type=clickedGroupGoal&goalName=<%=g.getGoalName()%>&goalOwner=${aGroup.groupName}"><%out.println(g.getGoalName());%></a></td>
                <td><%=g.getStartDate()%></td>
                <td><%=g.getDueDate()%></td>
                <td>Weight Goal</td>
                <td><%=(int) completion%>%</td>
                <td><input value="<%=g.getGoalName()%>" type="checkbox" name="delete"></td>
            </tr>


            <%
                }
            %>

            <%
                }
            %>

            <%
                if (session.getAttribute("distanceGoals") != null) {

                    ArrayList<DistanceGoal> wg = (ArrayList<DistanceGoal>) session.getAttribute("distanceGoals");
                    Iterator it = wg.iterator();
                    while (it.hasNext()) {

                        DistanceGoal g = (DistanceGoal) it.next();
                        double v = (double) g.getTargetDistance();
                        double u = (double) g.getAchievedDistance();
                        double completion = 0.0;

                        completion = 100 - ((v - u) / v) * 100;
                        
                        Object o = g;
                        


            %>
<%=o.getClass()%>
            <tr>
                <td><a href="/HealthTracker/GroupGoalController?type=clickedGroupGoal&goalName=<%=g.getGoalName()%>&goalOwner=${aGroup.groupName}"><%out.println(g.getGoalName());%></a></td>
                <td><%=g.getStartDate()%></td>
                <td><%=g.getDueDate()%></td>
                <td>Distance Goal</td>
                <td><%=(int) completion%>%</td>
                <td><input value="<%=g.getGoalName()%>" type="checkbox" name="delete"></td>
            </tr>


            <%
                }
            %>

            <%
                }
            %>
            
            <%
                if (session.getAttribute("fitnessGoals") != null) {

                    ArrayList<FitnessGoal> wg = (ArrayList<FitnessGoal>) session.getAttribute("fitnessGoals");
                    Iterator it = wg.iterator();
                    while (it.hasNext()) {

                        FitnessGoal g = (FitnessGoal) it.next();
                        double v = (double) g.getTargetHeartRate();
                        double u = (double) aUser.getHeartRate();
                        double completion = 0.0;

                        completion = 100 - ((v - u) / v) * 100;
                        
                        Object o = g;
                        


            %>
<%=o.getClass()%>
            <tr>
                <td><a href="/HealthTracker/GroupGoalController?type=clickedGroupGoal&goalName=<%=g.getGoalName()%>&goalOwner=${aGroup.groupName}"><%out.println(g.getGoalName());%></a></td>
                <td><%=g.getStartDate()%></td>
                <td><%=g.getDueDate()%></td>
                <td>Fitness Goal</td>
                <td><%=(int) completion%>%</td>
                <td><input value="<%=g.getGoalName()%>" type="checkbox" name="delete"></td>
           </tr>


            <%
                }
            %>

            <%
                }
            %>

            <%
                if (session.getAttribute("customGoals") != null) {

                    ArrayList<CustomGoal> wg = (ArrayList<CustomGoal>) session.getAttribute("customGoals");
                    Iterator it = wg.iterator();
                    while (it.hasNext()) {

                        double completion1 = 0.0;
                        double completion2 = 0.0;
                        double completion3 = 0.0;
                        int n = 0;
                        
                        CustomGoal g = (CustomGoal) it.next();
                        if(g.getTargetDistance() != 0){
                            double v = (double) g.getTargetDistance();
                        double u = (double) g.getAchievedDistance();
                        completion1 = 0.0;
                        completion1 = 100 - ((v - u) / v) * 100;
                        n++;
                        }
                        
                        if(g.getTargetWeight() != 0){
                            double v = (double) g.getTargetWeight();
                        double u = (double) aUser.getWeight();
                        completion2 = 0.0;
                        completion2 = 100 - ((v - u) / v) * 100;
                        n++;
                        }
                        
                        if(g.getTargetHeartRate() != 0){
                            double v = (double) g.getTargetHeartRate();
                        double u = (double) aUser.getHeartRate();
                        completion3 = 0.0;
                        completion3 = 100 - ((v - u) / v) * 100;
                        n++;
                        }
                        
                        double completion = (completion1 + completion2 + completion3)/n;

                        Object o = g;
                        


            %>
<%=o.getClass()%>
            <tr>
               <td><a href="/HealthTracker/GroupGoalController?type=clickedGroupGoal&goalName=<%=g.getGoalName()%>&goalOwner=${aGroup.groupName}"><%out.println(g.getGoalName());%></a></td>
                <td><%=g.getStartDate()%></td>
                <td><%=g.getDueDate()%></td>
                <td>Custom Goal</td>
                <td><%=(int) completion%>%</td>
                <td><input value="<%=g.getGoalName()%>" type="checkbox" name="delete"></td>
           </tr>


            <%
                }
            %>

            <%
                }
            %>
            
        </table>
            <input type="submit" value="Delete" name ="type">
            </form>
        <br /><br />

        <!-- Create Goal ... This code should be moved to separate page later -->

        <form ACTION="/HealthTracker/GroupGoalController" method="POST">
            <input type="hidden" name="groupName" 
                   value="<%=request.getParameter("groupName")%>"/>
            Select Type of Goal

            <select id="goalSelect" onclick="javascript:goalCheck();">
                <option value="0">Weight Goal</option>
                <option value="1">Fitness Goal</option>
                <option value="3">Distance Goal</option>
                <option value="2">Custom Goal</option>

                <option value="unselected" selected="selected">Select a Goal Type</option>
            </select>
            <br />

            <div id="weightForm" style="display:none">
                <input type="text" name="goalName" placeholder="Goal Name"/> <br />
                <input type="text" name="targetWeight" placeholder="Target Weight" />
                <select name="weightUnit">
                                                <option value="Kg">Kg</option>
                                                <option value="lbs">lbs</option>
                                            </select><br />       
                day: <input type="text" name="day_weight" size="3" maxlength="2" >
                month: <select size="1" name="month_weight" >
                    <option value="01">January</option>
                    <option value="02">February</option>
                    <option value="03">March</option>
                    <option value="04">April</option>
                    <option value="05" >May</option>
                    <option value="06">June</option>
                    <option value="07">July</option>
                    <option value="08">August</option>
                    <option value="09">September</option>
                    <option value="10" >October</option>
                    <option value="11">November</option>
                    <option value="12" >December</option>
                    <option value="unselected" selected="selected">Select a month</option>
                </select>
                year: <select size="1" name="year_weight">
                    <option value="2014">2014</option>
                    <option value="2015">2015</option>
                    <option value="2016">2016</option>
                    <option value="unselected" selected="selected">Select a year</option>
                </select><br />

                <input id = "weightType" type = "hidden" value = "W" name = "type1" />
                <input class="sub" type="submit" value="Submit" />
            </div>



            <div id="fitnessForm" style="display:none">
                <label for="name">Goal Name:</label>
                <input type="text" name="goalName_fitness" placeholder="Goal Name"/> <br />
                <label for="targetHeartRate">Target Heart Rate:</label>
                <input type="text" name="targetHeartRate" placeholder="Target Heart Rate" /> <br />
                <label for="Due Date">Due Date:</label><br />

                day: <input type="text" name="day_fitness" size="3" maxlength="2">
                month: <select size="1" name="month_fitness">
                    <option value="01">January</option>
                    <option value="02">February</option>
                    <option value="03">March</option>
                    <option value="04">April</option>
                    <option value="05" >May</option>
                    <option value="06">June</option>
                    <option value="07">July</option>
                    <option value="08">August</option>
                    <option value="09">September</option>
                    <option value="10" >October</option>
                    <option value="11">November</option>
                    <option value="12" >December</option>
                    <option value="unselected" selected="selected">Select a month</option>
                </select>
                year: <select size="1" name="year_fitness">
                    <option value="2014">2014</option>
                    <option value="2015">2015</option>
                    <option value="2016">2016</option>
                    <option value="unselected" selected="selected">Select a year</option>
                </select><br />
                <input id = "fitnessType" type = "hidden" value = "F" name = "type2" />
                <input class="sub" type="submit" value="Submit" />
            </div>

            <div id="customForm" style="display:none">
                <input type="text" name="goalName_custom" placeholder="Goal Name"/> <br />
                <input type="text" name="targetWeight_custom" placeholder="Target Weight" />
                <select name="weightUnit">
                                                <option value="Kg">Kg</option>
                                                <option value="lbs">lbs</option>
                                            </select><br /> 
                <input type="text" name="targetHeartRate_custom" placeholder="Target Heart Rate" value="0"/> <br />
                <input type="text" name="targetDistance_custom" placeholder="Target Distance" value="0" /> 
                <select name="customDistanceUnit">
                        <option value="Km">Km</option>
                        <option value="mi">mi</option>
                    </select><br />
                    While: <select size="1" name="activityType_custom">
                    <option value="W">Walking</option>
                    <option value="R">Running</option>
                    <option value="B">Cycling</option>
                </select><br />
                <label for="Due Date">Due Date:</label><br />        
                day: <input type="text" name="day_custom" size="3" maxlength="2">
                month: <select size="1" name="month_custom">
                    <option value="01">January</option>
                    <option value="02">February</option>
                    <option value="03">March</option>
                    <option value="04">April</option>
                    <option value="05" >May</option>
                    <option value="06">June</option>
                    <option value="07">July</option>
                    <option value="08">August</option>
                    <option value="09">September</option>
                    <option value="10" >October</option>
                    <option value="11">November</option>
                    <option value="12" >December</option>
                    <option value="unselected" selected="selected">Select a month</option>
                </select>
                year: <select size="1" name="year_custom">
                    <option value="2014">2014</option>
                    <option value="2015">2015</option>
                    <option value="2016">2016</option>
                    <option value="unselected" selected="selected">Select a year</option>
                </select><br />
                <input id = "customType" type = "hidden" value = "C" name = "type3" />
                <input class="sub" type="submit" value="Submit" />


                <!-- This hidden field is used to check whether entering a controller from a form or link -->
                <input type = "hidden" value = "..." name = "type" /> 


            </div>

            <div id="distanceForm" style="display:none">
                <label for="name">Goal Name:</label>
                <input type="text" name="goalName_distance" placeholder="Goal Name" /> <br />
                <label for="targetDistance">Target Distance:</label>
                <input type="text" name="targetDistance_distance" placeholder="Target Distance" /> 
                <select name="distanceDistanceUnit">
                        <option value="Km">Km</option>
                        <option value="mi">mi</option>
                    </select><br />
                While: <select size="1" name="activityType">
                    <option value="W">Walking</option>
                    <option value="R">Running</option>
                    <option value="B">Cycling</option>
                </select><br />
                <label for="Due Date">Due Date:</label><br />        
                day: <input type="text" name="day_distance" size="3" maxlength="2">
                month: <select size="1" name="month_distance">
                    <option value="01">January</option>
                    <option value="02">February</option>
                    <option value="03">March</option>
                    <option value="04">April</option>
                    <option value="05" >May</option>
                    <option value="06">June</option>
                    <option value="07">July</option>
                    <option value="08">August</option>
                    <option value="09">September</option>
                    <option value="10" >October</option>
                    <option value="11">November</option>
                    <option value="12" >December</option>
                    <option value="unselected" selected="selected">Select a month</option>
                </select>
                year: <select size="1" name="year_distance">
                    <option value="2014">2014</option>
                    <option value="2015">2015</option>
                    <option value="2016">2016</option>
                    <option value="unselected" selected="selected">Select a year</option>
                </select><br />
                <input id = "distanceType" type = "hidden" value = "C" name = "type4" />
                <input class="sub" type="submit" value="Submit" />


                <!-- This hidden field is used to check whether entering a controller from a form or link -->
                <input type = "hidden" value = "..." name = "type" /> 


            </div>
        </form>
        
        
        
        
        
        
        
        
    </body>
</html>
