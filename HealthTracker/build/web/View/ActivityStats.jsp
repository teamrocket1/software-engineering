<%-- 
    Document   : ActivityStats
    Created on : 26-Mar-2014, 11:34:47
    Author     : jsz12txu
--%>

<%@page import="Model.Bike"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.sql.Date"%>
<%@page import="Model.Run"%>
<%@page import="Model.Walk"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Model.Meal"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:useBean id="aUser" type="Model.User" scope="session" />
        <jsp:useBean id="walks" type="ArrayList<Walk>" scope="session" />
        <jsp:useBean id="runs" type="ArrayList<Run>" scope="session" />
        <jsp:useBean id="bikes" type="ArrayList<Bike>" scope="session" />

        <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400italic' rel='stylesheet' type='text/css'>

        <!-- My old nav bar CSS -->
        <link rel="stylesheet" href="css/style2.css" type="text/css" />

        <!-- Graph CSS -->
        <link rel="stylesheet" href="css/05.css">
        <!--<link rel="stylesheet" href="css/common.css">-->

        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!---strat-slider---->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/slider-style.css" />
        <script type="text/javascript" src="js/modernizr.custom.28468.js"></script>
        <!---//start-slider---->
        <!---start-login-script-->
        <script src="js/login.js"></script>
        <!---//End-login-script--->
        <!--768px-menu---->
        <link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
        <script type="text/javascript" src="js/jquery.mmenu.js"></script>
        <script type="text/javascript">
            //	The menu on the left
            $(function() {
            $('nav#menu-left').mmenu();
            });
        </script>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Activity Stats Page</title>
    </head>
    <body>

        <div class="header">
            <div class="wrap">
                <!--<div class="header-left">
                        <div class="logo">
                                <a href="index.jsp">HealthTracker</a>
                        </div>
                </div>-->
                <div class="header-right2">
                    <div class="top-nav">
                        <ul>
                            <%                                                String username = aUser.getUsername();
                                String firstLetter = username.substring(0, 1);
                                String remainingLetters = username.substring(1, username.length());
                                username = firstLetter.toUpperCase() + remainingLetters;

                            %>

                            <li><a href="/HealthTracker/ProfilePageController?pageOwner=<%=aUser.getUsername()%>"><%= username%></a></li>
                            <li><a href="/HealthTracker/DataCaptureController?type=clicked">Update Activities</a></li>
                            <li><a href="/HealthTracker/GoalController?type=clicked">Goals</a></li>
                            <li><a href="/HealthTracker/GroupController?type=clicked">Groups</a></li>
                            <li><a href="/HealthTracker/HistoryController?type=history">History</a></li>
                            <li><a href="/HealthTracker/AchievmentController?type=clicked">Achievements</a></li>
                            <li><a href="/HealthTracker/HomeController?type=clickedEdit">Edit Profile</a></li>
                            <li><a href="/HealthTracker/HistoryController?type=activityStats">Activity Stats</a></li>
                        </ul>
                    </div>

                    <div class="clear"> </div>
                    </ul>
                </div>
                <div class="clear"> </div>
            </div>
            <div class="clear"> </div>
        </div>

        <br />



        <div style="margin-left: 30%; margin-right: 30%;">Press the arrows on the keyboard to rotate the graph </div>
        <div class="toggles"><a href="#" id="reset-graph-button">Reset graph</a></div>

        <div id="wrapper">
            <div class="chart">
                <h2>Your Activities</h2>
                <table id="data-table" border="1" cellpadding="10" cellspacing="0" summary="The effects of the zombie outbreak on the populations of endangered species from 2012 to 2016">
                    <caption>Distance in km</caption>
                    <thead>
                        <tr>

                            <%

                            // getting walk dates
                               // if (session.getAttribute("walks") != null) {
                                    ArrayList<Walk> walkList = (ArrayList<Walk>) session.getAttribute("walks");
                                    Iterator it =walkList.iterator();
                                    int index = 0;
                                    Hashtable<Date, int[]> dictionary = new Hashtable<Date, int[]>();
                                    
                                    while (it.hasNext()) {

                                        Walk w = (Walk) it.next();
                                        
                                        if(dictionary.get(w.getDate()) == null){
                                            int[] arr = new int[3];
                                            dictionary.put(w.getDate(), arr);
                                        }
                                        
                                        int distance = w.getDistance();
                                        int[] arr = dictionary.get(w.getDate());
                                        arr[0] = arr[0] + distance; 
                                        dictionary.put(w.getDate(), arr);
            
                                    }
                                    
                                    it = runs.iterator();
                                    while (it.hasNext()) {

                                        Run r = (Run) it.next();
                                        
                                        if(dictionary.get(r.getDate()) == null){
                                            int[] arr = new int[3];
                                            dictionary.put(r.getDate(), arr);
                                        }
                                        
                                        int distance = r.getDistance();
                                        int[] arr = dictionary.get(r.getDate());
                                        arr[1] = arr[1] + distance; 
                                        dictionary.put(r.getDate(), arr);
            
                                    }
                                    
                                    it = bikes.iterator();
                                    while (it.hasNext()) {

                                        Bike b = (Bike) it.next();
                                        
                                        if(dictionary.get(b.getDate()) == null){
                                            int[] arr = new int[3];
                                            dictionary.put(b.getDate(), arr);
                                        }
                                        
                                        int distance = b.getDistance();
                                        int[] arr = dictionary.get(b.getDate());
                                        arr[2] = arr[2] + distance; 
                                        dictionary.put(b.getDate(), arr);
            
                                    }
                                     
                                    Enumeration<Date> dates = dictionary.keys();
                                    
                                    for(int i = 0; i < dictionary.size(); i++){
                                        
                                        
                                        
                            %>

                            <td>&nbsp;</td>
                            <th scope="col"><%= dates.nextElement() %></th>
                            <!-- Other activities dates here -->
<%
                                        }
                                    //}
%>
                            
                               </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">Walks</th>
                            <%
                                    Collection<int[]> coll = dictionary.values();
                                    Iterator it2 = coll.iterator();
                                    while(it2.hasNext()){
                                        int[] arr = (int[]) it2.next();
                            %>

                            <td><%= arr[0] %></td>
                            
                            <%
                                    }   
                                        %>
                        </tr>
                        
                        <tr>
                            <th scope="row">Runs</th>
                            <%
                                    //Collection<int[]> coll = dictionary.values();
                                    it2 = coll.iterator();
                                    while(it2.hasNext()){
                                        int[] arr = (int[]) it2.next();
                            %>

                            <td><%= arr[1] %></td>
                            
                            <%
                                    }   
                                        %>
                        </tr>
                        
                        <tr>
                            <th scope="row">Cycles</th>
                            <%
                                    //Collection<int[]> coll = dictionary.values();
                                    it2 = coll.iterator();
                                    while(it2.hasNext()){
                                        int[] arr = (int[]) it2.next();
                            %>

                            <td><%= arr[2] %></td>
                            
                            <%
                                    }   
                                        %>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>




        <!-- JavaScript at the bottom for fast page loading -->

        <!-- Grab jQuery from Google -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>

        <!-- Example JavaScript -->
        <script src="/HealthTracker/View/js/05.js"></script>
    </body>
</html>
