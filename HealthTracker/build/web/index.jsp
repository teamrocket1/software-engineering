<%-- 
    Document   : index
    Created on : Mar 4, 2014, 6:14:34 PM
    Author     : Odie
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Health Tracker</title>
        <script>
            var patt = new RegExp("e");
            var passwordPatt = new RegExp("e");
            var pat = /[0-9A-Za-z]+/;
        </script>
        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    </script>
    <!---strat-slider---->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/slider-style.css" />
    <script type="text/javascript" src="js/modernizr.custom.28468.js"></script>
    <!---//start-slider---->
    <!---start-login-script-->
    <script src="js/login.js"></script>
    <!---//End-login-script--->
    <!--768px-menu---->
    <link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
    <script type="text/javascript" src="js/jquery.mmenu.js"></script>
    <script type="text/javascript">
        //	The menu on the left
        $(function() {
        $('nav#menu-left').mmenu();
        });
    </script>

    <!-- @start of script/hrefs for sign up pop up-->

    <link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="css/demo.css">
    <link rel="stylesheet" href="css/avgrund.css">

    <script>
        function openDialog() {
        Avgrund.show( "#default-popup" );
        }
        function closeDialog() {
        Avgrund.hide();
        }
    </script>
    <!-- @ end of script for sign up pop up-->            

    <!--//768px-menu-->
</head>
<body>
    <!---start-wrap---->
    <!------start-768px-menu---->
    <div id="page">
        <div id="header">
            <a class="navicon" href="#menu-left"> </a>
        </div>
        <nav id="menu-left">
            <ul>
               
                <li><a href="index.jsp">Home</a></li>
                <li><a href="View/about.jsp">About</a></li>
                <li><a href="View/Achievments.jsp">Features</a></li>
                <div class="clear"> </div>
            </ul>
        </nav>
    </div>
    <!------start-768px-menu---->
    <!---start-header---->
    <div class="header">
        <div class="wrap">
            <div class="header-left">
                <div class="logo">
                    <a href="index.jsp">Health Tracker</a>
                </div>
            </div>
            <div class="header-right">
                <div class="top-nav">
                    <ul>
                        <li><a href="index.html">Home</a></li>
                        <li><a href="View/about.jsp">About</a></li>
                        <li><a href="View/features.jsp">Features</a></li>
                    </ul>
                </div>
                <div class="sign-ligin-btns">
                    <ul>
                        <!--<li id="signupContainer"><a class="signup" id="signupButton" href="/HealthTracker/SignupPage.jsp"><span><i>Signup</i></span></a>
                            <div class="clear"> </div>-->
                        <!----------------------------------------------------->
                        <!-- Where the sign up form pops up -->

                        
                        <li id="signupContainer" ><a class="signup" id="signupButton"  onclick="javascript:openDialog()" ><span><i>Signup</i></span></a>
                           
                            <div class="clear"> </div>
                            <aside id="default-popup" class="avgrund-popup">
                                <h2>here you go oddy and james it kinda works</h2>
                                <p>
                                    click  ESC or click outside to close the pop up modal 
                                </p>

                                <button onclick="javascript:closeDialog();">Close</button>
                            </aside>

		
                            <!--<button onclick="javascript:openDialog();">Open popup</button>-->

                            <div class="avgrund-cover"></div>

                            <script type="text/javascript" src="js/avgrund.js"></script>


                            <!----------------------------------------------------->
                            <!-- Where the sign up form ends -->


                            <!-- Login starts Here -->


                        </li>
                        <li id="loginContainer"><a class="login" id="loginButton" href="#"><span><i>Login</i></span></i></a>
                            <div class="clear"> </div>
                            <div id="loginBox">                
                                <form id="loginForm" action="/HealthTracker/LoginController" method="POST">
                                    <fieldset id="body">
                                        <fieldset>
                                            <label for="username">Username</label>
                                            <input type="text" name="username" placeholder="username" 
                                                   maxlength ="20">
                                        </fieldset>
                                        <fieldset>
                                            <label for="password">Password</label>
                                            <input type="password" name="password" placeholder="password"
                                                   maxlength ="20" id="password">
                                            <input type = "hidden" value = "login" name = "type" />
                                        </fieldset>
                                        <label class="remeber" for="checkbox"><input type="checkbox" id="checkbox" />Remember me</label>
                                        <input type="submit" id="login" value="Log in" />
                                    </fieldset>
                                    <span><a href="#">Forgot your password?</a></span>
                                </form>
                            </div>
                            <!-- Login Ends Here -->
                        </li>
                        <div class="clear"> </div>
                    </ul>
                </div>
                <div class="clear"> </div>
            </div>
            <div class="clear"> </div>
        </div>
    </div>
    <!---//End-header---->
    <!----start-banner---->
    <div class="text-slider">
        <div class="wrap"> 
            <!--tart-da-slider-->
            <div id="da-slider" class="da-slider">
                <div class="da-slide">
                    <h2>Log Your Activities</h2>
                    <p> Track your Progress Compete with your Friends</p>
                    <a href="about.html" class="da-link">Find out More</a>
                </div>
                <div class="da-slide">
                    <h2>Log Your Meals</h2>
                    <p> Keep track of your Diet Upgrade your state of Health</p>
                    <a href="about.html" class="da-link">Find out More</a>
                </div>
                <div class="da-slide">
                    <h2>Log Your Activities</h2>
                    <p> Track your Progress Compete with your Friends</p>
                    <a href="about.html" class="da-link">Find out More</a>
                </div>
                <div class="da-slide">
                    <h2>Log Your Meals</h2>
                    <p> Keep track of your Diet Upgrade your state of Health</p>
                    <a href="about.html" class="da-link">Find out More</a>
                </div>

            </div>
            <nav class="da-arrows">
                <span class="da-arrows-prev"> </span>
                <span class="da-arrows-next"> </span>
            </nav>
        </div>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript">
            $(function() {
            $('#da-slider').cslider({
            autoplay	: true,
            bgincrement	: 450
            });

            });
        </script>
    </div>
</div><!--<img src="/HealthTracker/Images/screen.png" title="watch" />-->
<!---//End-da-slider----->
<!----//End-banner---->
<!--start-content-->
<div class="content">
    <div class="wrap">
        <%
            /*<!---- start-top-grids----->
             <div class="top-grids">
             <div class="top-grid">
             <div class="product-pic frist-product-pic">
             <img src="/HealthTracker/images/shaq.gif" title="watch" />
             </div>
             <span><label>1</label></span>
             <div class="border"> </div>
             <a href="#">Get a GPS Device</a>
             </div>
             <div class="top-grid">
             <div class="product-pic">
             <img src="images/shoe-img.png" title="shoe" />
             </div>
             <span><label>2</label></span>
             <div class="border hide"> </div>
             <a href="#">Go for a run</a>
             </div>
             <div class="top-grid hide">
             <div class="product-pic">
             <img src="images/lap-img.png" title="laptop" />
             </div>
             <span><label>3</label></span>
             <a href="#">View your results</a>
             </div>
             <div class="clear"> </div>
             </div>
             </div>
                                                
             <!--- start-top-grids---->
             <!--start-mid-grids-->
             <div class="mid-grids">
             <div class="wrap">
             <div class="mid-grids-left">
             <img src="images/app-divices.jpg" title="divices" />
             <span> </span>
             </div>
             <div class="mid-grids-right">
             <h3> Get <span>Runkeeper</span> for your mobile</h3>
             <p>The Runkeeper app is available for both i<big>OS</big> and <big>Android</big> devices.</p>
             <ul class="fea">
             <li><a href="#"><i>Log</i> your runs</a></li>
             <li><a href="#"><i>Track</i> your Progress </a></li>
             <li><a href="#">Get a <i>Virtual trainer</i></a></li>
             <li><a href="#"><i>Complete</i> with your friends </a></li>
             <li><a href="#"><i>Share</i> your routes</a></li>
             </ul>
             <div class="big-btns">
             <ul>
             <li><a class="andr" href="#"> </a></li>
             <li><a class="iphone" href="#"> </a></li>
             <div class="clear"> </div>
             </ul>
             </div>
             </div>
             <div class="clear"> </div>
             </div>
             </div>
             <!---//End-mid-grids--->
             <!---start-bottom-footer-grids---->*/
        %>
        <div class="footer-grids">
            <div class="wrap">
                <div class="footer-grid">
                    <h3>Quick Links</h3>
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About Features</a></li>
                        <li><a href="#">Login</a></li>
                        <li><a href="#">Sign Up</a></li>
                    </ul>
                </div>
                <div class="footer-grid">
                    <h3>More</h3>
                    <ul>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Support</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Terms and Conditions</a></li>
                    </ul>
                </div>
                <div class="footer-grid">
                    <h3>Connect With Us</h3>
                    <ul class="social-icons">
                        <li><a class="facebook" href="#"> </a></li>
                        <li><a class="twitter" href="#"> </a></li>
                        <li><a class="youtube" href="#"> </a></li>
                    </ul>
                </div>
                <div class="footer-grid">
                    <h3>Newsletter</h3>
                    <p>Subscribe to our newsletter to keep up-to-date with all the latest news.</p>
                    <form>
                        <input type="text" class="text" value="Your Name" onfocus="this.value = '';" onblur="if (this.value == '') {
                                            this.value = 'Your Name';
                                        }">
                        <input type="text" class="text" value="Your Email" onfocus="this.value = '';" onblur="if (this.value == '') {
                                            this.value = 'Your Email';
                                        }">
                        <input type="submit" value="subscribe" />
                    </form>
                </div>
                <div class="clear"> </div>
            </div>
        </div>

        <p class="copy-right"><it><b>Disclaimer:</b> This application is not a commercial application and does not provide insurance. 
            This is a study project that is part of a Computing Science module taught at the University of East Anglia,
            Norwich, UK. If you have any questions, please contact the module coordinator,
            Joost Noppen, at j.noppen@uea.ac.uk</it></p>
        <!---//End-bottom-footer-grids---->
    </div>
    <!----//End-content--->
    <!---//End-wrap---->
    
     <h3>SIGN UP</h3>
		
		<!-- Signup form -->
        <form action="/HealthTracker/LoginController" method="POST">

            <input type="text" name="firstname" placeholder="firstname"
                   maxlength ="20"> <br />
            <input type="text" name="lastname" placeholder="lastname"
                   maxlength ="20"> <br />
            <input type="text" name="username" placeholder="username" 
                   maxlength ="20"> <br />
            <input type="password" name="password" placeholder="password"
                   maxlength ="20" title="Your password must conatin at least 
                   eight symbols containing at least one number,
                   one lower, and one upper letter. Thank You."
                   pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" required> <br />
            <input type="email" name="email" placeholder="email"
                   maxlength ="30"> <br />
            <input title="Please enter 'm' for male, 'f' for femal or 'o' for
                    other. Thank You."
                   type="text" name="sex" placeholder="sex"
                   maxlength ="1" pattern="[mfo]"> <br />
            <input type="text" name="age" placeholder="age"
                   maxlength ="3"> <br />
            <input type="text" name="weight" placeholder="weight"
                   maxlength ="10"> 
            <select name="weightUnit">
                <option value="Kg">Kg</option>
                <option value="lbs">lbs</option>
            </select><br />
            <input type="text" name="height" placeholder="height"
                   maxlength ="10">cm <br />
            <input type="text" name="heartrate" placeholder="heartrate"
                   maxlength ="10"> <br />
            <input type = "hidden" value = "signUp" name = "type" /> 
            <input class="sub" type="submit" value="Sign Up" />

            <!--<button>L</button>-->

        </form>
</body>
</html>