/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * * Test Case
 * @author Takomborerwa
 */
public class WalkTest {
    
    public WalkTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getHeartRate method, of class Walk.
     */
    @Test
    public void testGetHeartRate() {
        System.out.println("getHeartRate");
        Walk instance = new Walk(100, 200, "joe","m",120,20,130);
        int expResult = 130;
        int result = instance.getHeartRate();
        assertEquals(expResult, result);
      
    }

    /**
     * Test of setHeartRate method, of class Walk.
     */
    @Test
    public void testSetHeartRate() {
        System.out.println("setHeartRate");
        int heartRate = 0;
        Walk instance = new Walk(100, 200, "joe","m",120,20,130);
        instance.setHeartRate(heartRate);
        int result = instance.getHeartRate();
        assertEquals(heartRate, result);
       
    }

    /**
     * Test of getCaloriesBurnt method, of class Walk.
     */
    @Test
    public void testGetCaloriesBurnt() {
        System.out.println("getCaloriesBurnt");
        Walk instance = new Walk(100, 200, "joe","m",120,20,130);
        int expResult = 89;
        int result = instance.getCaloriesBurnt();
        assertEquals(expResult, result);
    }

  
}
