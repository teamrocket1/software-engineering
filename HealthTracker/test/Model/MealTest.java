/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * * Test Case
 * @author Takomborerwa
 */
public class MealTest {
    
    public MealTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getType method, of class Meal.
     */
    @Test
    public void testGetType() {
        System.out.println("getType");
        Meal instance = new Meal("Meat","Chicken", 200);
        String expResult = "Meat";
        String result = instance.getType();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setType method, of class Meal.
     */
    @Test
    public void testSetType() {
        System.out.println("setType");
        Meal instance = new Meal("Meat","Chicken", 200);
        String type = "Veg";
        instance.setType(type);
        assertEquals(type, instance.getType());
    }

    /**
     * Test of getName method, of class Meal.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
       Meal instance = new Meal("Meat","Chicken", 200);
        String expResult = "Chicken";
        String result = instance.getName();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setName method, of class Meal.
     */
    @Test
    public void testSetName() {
        System.out.println("setType");
        Meal instance = new Meal("Meat","Chicken", 200);
        String name = "Corn";
        instance.setName(name);
        assertEquals(name, instance.getName());
    }

    /**
     * Test of getCalories method, of class Meal.
     */
    @Test
    public void testGetCalories() {
        System.out.println("getCalories");
       Meal instance = new Meal("Meat","Chicken", 200);
        int expResult = 200;
        int result = instance.getCalories();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setCalories method, of class Meal.
     */
    @Test
    public void testSetCalories() {
        System.out.println("setCalories");
        int calories = 0;
        Meal instance = new Meal("Meat","Chicken", 200);
        instance.setCalories(calories);
        assertEquals(calories, instance.getCalories());
    }

 
}
