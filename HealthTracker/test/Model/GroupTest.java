/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Model.Membership.Role;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * * Test Case
 * * Test Case
 * @author Takomborerwa
 */
public class GroupTest {
    
    public GroupTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createGroup method, of class Group.
     */
    @Test
    public void testCreateGroup() {
        System.out.println("createGroup");
        String username = "joe";
        Group instance = new Group(username);
        //if database isn't running.
        boolean expResult = true;
        boolean result = instance.createGroup(username);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of joinGroup method, of class Group.
     */
    @Test
    public void testJoinGroup() {
        System.out.println("joinGroup");
        String username = "joe";
        Group instance = new Group(username);
        Role r = Role.U;
        int permissions = 1;
        boolean expResult = true;
        boolean result = instance.joinGroup(username, r, permissions);
        assertEquals(expResult, result);
        
    }
}
