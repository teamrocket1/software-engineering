/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 ** Test Case
 * @author Odie
 */
public class WeightGoalTest {
    
    public WeightGoalTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of getTargetWeight method, of class WeightGoal.
     */
    @Test
    public void testGetTargetWeight() {
        System.out.println("getTargetWeight");
        WeightGoal instance = new WeightGoal("joe","fitness Test");
        int expResult = 0;
        int result = instance.getTargetWeight();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setTargetWeight method, of class WeightGoal.
     */
    @Test
    public void testSetTargetWeight() {
        System.out.println("setTargetWeight");
        int targetWeight = 100;
        WeightGoal instance = new WeightGoal("joe","fitness Test");
        instance.setTargetWeight(targetWeight);
        int result = instance.getTargetWeight();
        assertEquals(targetWeight, result);
      
    }

  
}