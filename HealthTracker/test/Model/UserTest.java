/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * * Test Case
 * @author Odie
 */
public class UserTest {
    
    public UserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of login method, of class User.
     */
    @Test
    public void testLogin() throws Exception {
        System.out.println("login");
        String u = "joe";
        String p = "Password1";
        User instance = new User("Joe","Moore","eroomeoj", "password", "test@fake.com", "m",19,180,120,130);
        boolean expResult = true;
        boolean result = instance.login(u, p);
        assertEquals(expResult, result);
      
    }

    /**
     * Test of signUp method, of class User.
     
    @Test
    public void testSignUp() {
        System.out.println("signUp");
        User instance = new User("Joe","Moore","eroomeoj", "password", "test@fake.com", "m",19,180,120,130);
        boolean expResult = true;
        boolean result = instance.signUp();
        assertEquals(expResult, result);
        
    }
    */

    /**
     * Test of getBMI method, of class User.
     */
    @Test
    public void testGetBMI() {
        System.out.println("getBMI");
        User instance = new User("Joe","Moore","eroomeoj", "password", "test@fake.com", "m",19,180,120,130);
        double expResult = 180;
        double result = instance.getBMI();
        assertEquals(expResult, result,0.0);
        
    }

    /**
     * Test of getFirstName method, of class User.
     */
    @Test
    public void testGetFirstName() {
        System.out.println("getFirstName");
        User instance = new User("Joe","Moore","eroomeoj", "password", "test@fake.com", "m",19,180,120,130);
        String expResult = "Joe";
        String result = instance.getFirstName();
        assertEquals(expResult, result);
  
    }

    /**
     * Test of setFirstName method, of class User.
     */
    @Test
    public void testSetFirstName() {
        System.out.println("setFirstName");
        String firstName = "Jim";
        User instance = new User("Joe","Moore","eroomeoj", "password", "test@fake.com", "m",19,180,120,130);
        instance.setFirstName(firstName);
        String result = instance.getFirstName();
        assertEquals(firstName, result);
    }

    /**
     * Test of getLastName method, of class User.
     */
    @Test
    public void testGetLastName() {
        System.out.println("getLastName");
        User instance = new User("Joe","Moore","eroomeoj", "password", "test@fake.com", "m",19,180,120,130);
        String expResult = "Moore";
        String result = instance.getLastName();
        assertEquals(expResult, result);
        }

    /**
     * Test of setLastName method, of class User.
     */
    @Test
    public void testSetLastName() {
        System.out.println("setLastName");
        String lastName = "Matthews";
        User instance = new User("Joe","Moore","eroomeoj", "password", "test@fake.com", "m",19,180,120,130);
        instance.setLastName(lastName);
        String result = instance.getLastName();
        assertEquals(lastName, result);
    }

    /**
     * Test of getUsername method, of class User.
     */
    @Test
    public void testGetUsername() {
        System.out.println("getUsername");
        User instance = new User("Joe","Moore","eroomeoj", "password", "test@fake.com", "m",19,180,120,130);
        String expResult = "eroomeoj94";
        String result = instance.getUsername();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setUsername method, of class User.
     */
    @Test
    public void testSetUsername() {
        System.out.println("setUsername");
        String username = "Stevens";
        User instance = new User("Joe","Moore","eroomeoj", "password", "test@fake.com", "m",19,180,120,130);
        instance.setUsername(username);
        String result = instance.getLastName();
        assertEquals(username, result);
    }

    /**
     * Test of getPassword method, of class User.
     */
    @Test
    public void testGetPassword() {
        System.out.println("getPassword");
        User instance = new User("Joe","Moore","eroomeoj", "password", "test@fake.com", "m",19,180,120,130);
        String expResult = "password";
        String result = instance.getPassword();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setPassword method, of class User.
     */
    @Test
    public void testSetPassword() {
        System.out.println("setPassword");
        String password = "password1";
        User instance = new User("Joe","Moore","eroomeoj", "password", "test@fake.com", "m",19,180,120,130);
        instance.setPassword(password);
        String result = instance.getPassword();
        assertEquals(password, result);
    }

    /**
     * Test of getEmail method, of class User.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
        User instance = new User("Joe","Moore","eroomeoj", "password", "test@fake.com", "m",19,180,120,130);
        String expResult = "test@fake.com";
        String result = instance.getEmail();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setEmail method, of class User.
     */
    @Test
    public void testSetEmail() {
        System.out.println("setEmail");
        String email = "joe@real.com";
        User instance = new User("Joe","Moore","eroomeoj", "password", "test@fake.com", "m",19,180,120,130);
        instance.setEmail(email);
        String result = instance.getEmail();
        assertEquals(email, result);
    }

    /**
     * Test of getSex method, of class User.
     */
    @Test
    public void testGetSex() {
        System.out.println("getSex");
        User instance = new User("Joe","Moore","eroomeoj", "password", "test@fake.com", "m",19,180,120,130);
        String expResult = "m";
        String result = instance.getSex();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setSex method, of class User.
     */
    @Test
    public void testSetSex() {
        System.out.println("setSex");
        String sex = "f";
        User instance = new User("Joe","Moore","eroomeoj", "password", "test@fake.com", "m",19,180,120,130);
        instance.setSex(sex);
        String result = instance.getSex();
        assertEquals(sex, result);
    }

    /**
     * Test of getAge method, of class User.
     */
    @Test
    public void testGetAge() {
        System.out.println("getAge");
        User instance = new User("Joe","Moore","eroomeoj", "password", "test@fake.com", "m",19,180,120,130);
        int expResult = 19;
        int result = instance.getAge();
        assertEquals(expResult, result);
        
    }

 
}
