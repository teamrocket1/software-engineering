/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 ** Test Case
 * * Test Case
 * @author Takomborerwa
 */
public class CustomGoalTest {
    
    public CustomGoalTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setDueDate method, of class CustomGoal.
     */
    @Test
    public void testSetDueDate() {
        System.out.println("setDueDate");
        Date dueDate = null;
        String owner = "joe";
        String goalName = "custom test goal";
        CustomGoal instance = new CustomGoal(owner, goalName);
        instance.setDueDate(dueDate);
        assertEquals(dueDate, instance.getDueDate());
    }

    /**
     * Test of getTargetWeight method, of class CustomGoal.
     */
    @Test
    public void testGetTargetWeight() {
        System.out.println("getTargetWeight");
         String owner = "joe";
        String goalName = "custom test goal";
        CustomGoal instance = new CustomGoal(owner, goalName);
        int expResult = 0;
        int result = instance.getTargetWeight();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setTargetWeight method, of class CustomGoal.
     */
    @Test
    public void testSetTargetWeight() {
        System.out.println("setTargetWeight");
        int targetWeight = 0;
         String owner = "joe";
        String goalName = "custom test goal";
        CustomGoal instance = new CustomGoal(owner, goalName);
        instance.setTargetWeight(targetWeight);
        assertEquals(targetWeight, instance.getTargetWeight());
    
    }

    /**
     * Test of getTargetBMI method, of class CustomGoal.
     */
    @Test
    public void testGetTargetBMI() {
        System.out.println("getTargetBMI");
        String owner = "joe";
        String goalName = "custom test goal";
        CustomGoal instance = new CustomGoal(owner, goalName);
        int expResult = 0;
        int result = instance.getTargetBMI();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTargetBMI method, of class CustomGoal.
     */
    @Test
    public void testSetTargetBMI() {
        System.out.println("setTargetBMI");
        int targetBMI = 0;
         String owner = "joe";
        String goalName = "custom test goal";
        CustomGoal instance = new CustomGoal(owner, goalName);
        instance.setTargetBMI(targetBMI);
        assertEquals(targetBMI, instance.getTargetBMI());
    
    }

    /**
     * Test of getExersise method, of class CustomGoal.
     */
    @Test
    public void testGetExersise() {
        System.out.println("getExersise");
         String owner = "joe";
        String goalName = "custom test goal";
        CustomGoal instance = new CustomGoal(owner, goalName);
        Exercise expResult = null;
        Exercise result = instance.getExersise();
        assertEquals(expResult, result);
   
    }

    /**
     * Test of setExersise method, of class CustomGoal.
     */
    @Test
    public void testSetExersise() {
        System.out.println("setExersise");
        Exercise exersise = null;
         String owner = "joe";
        String goalName = "custom test goal";
        CustomGoal instance = new CustomGoal(owner, goalName);
        instance.setExersise(exersise);
        assertEquals(exersise, instance.getExersise());
       
    }
}
