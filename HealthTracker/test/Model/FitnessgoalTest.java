/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 ** Test Case
 * @author Takomborerwa
 */
public class FitnessgoalTest {
    
    public FitnessgoalTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    

    /**
     * Test of getTargetHeartRate method, of class FitnessGoal.
     */
    @Test
    public void testGetTargetHeartRate() {
        System.out.println("getTargetHeartRate");
        FitnessGoal instance = new FitnessGoal("joe","fitness Test");
        int expResult = 0;
        int result = instance.getTargetHeartRate();
        assertEquals(expResult, result);
      
    }

    /**
     * Test of setTargetHeartRate method, of class FitnessGoal.
     */
    @Test
    public void testSetTargetHeartRate() {
        System.out.println("setTargetHeartRate");
        int targetHeartRate = 10;
        FitnessGoal instance = new FitnessGoal("joe","fitness Test");
        instance.setTargetHeartRate(targetHeartRate);
        assertEquals(targetHeartRate, instance.getTargetHeartRate());
      
    }

    /**
     * Test of getExercise method, of class FitnessGoal.
     */
    @Test
    public void testGetExercise() {
        System.out.println("getExercise");
        FitnessGoal instance = new FitnessGoal("joe","fitness Test");
        Exercise expResult = null;
        Exercise result = instance.getExercise();
        assertEquals(expResult, result);
     
    }

    /**
     * Test of setExercise method, of class FitnessGoal.
     */
    @Test
    public void testSetExercise() {
        System.out.println("setExercise");
        Exercise exercise = null;
        FitnessGoal instance = new FitnessGoal("joe","fitness Test");
        instance.setExercise(exercise);        
    }
}
