/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test Case
 * @author Takomborerwa
 */
public class BikeTest {
    
    public BikeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getCaloriesBurnt method, of class Bike.
     */
    @Test
    public void testGetCaloriesBurnt() {
        System.out.println("getCaloriesBurnt");
        Bike instance = new Bike(100, 200, "joe","m",120,20,130);
        int expResult = 89;
        int result = instance.getCaloriesBurnt();
        assertEquals(expResult, result);
    }
}
