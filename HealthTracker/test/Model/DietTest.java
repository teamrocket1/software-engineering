/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.Iterator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 ** Test Case
 * @author Takomborerwa
 */
public class DietTest {
    
    public DietTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of get method, of class Diet.
     */
    @Test
    public void testGet() {
        System.out.println("get");
        
        
        Meal testMeal = new Meal("Meat","Chicken", 200);
        ArrayList<Meal> mealList = new ArrayList<Meal>();
        mealList.add(testMeal);
        Diet instance = new Diet(mealList);
        Meal expResult = new Meal("Meat","Chicken", 200);
        Meal result = instance.get(0);
        if(expResult.getName().equals(result.getName())){
            if(expResult.getType().equals(result.getType())){
                assertEquals(expResult.getCalories(), expResult.getCalories());
                }
        } else{
            fail("The meals don't equal");
        }
        
   
    }


    /**
     * Test of remove method, of class Diet.
     */
    @Test
    public void testRemove() {
        System.out.println("remove");
       Meal testMeal = new Meal("Meat","Chicken", 200);
        ArrayList<Meal> mealList = new ArrayList<Meal>();
        mealList.add(testMeal);
        Diet instance = new Diet(mealList);
        
        instance.remove(0);
    }

    /**
     * Test of size method, of class Diet.
     */
    @Test
    public void testSize() {
        System.out.println("size");
        Meal testMeal = new Meal("Meat","Chicken", 200);
        ArrayList<Meal> mealList = new ArrayList<Meal>();
        mealList.add(testMeal);
        Diet instance = new Diet(mealList);
        
        int expResult = 1;
        int result = instance.size();
        assertEquals(expResult, result);

    }

}
