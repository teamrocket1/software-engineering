<%-- 
    Document   : GoalPage
    Created on : Mar 26, 2014, 3:26:21 PM
    Author     : Odie
--%>


<%@page import="java.util.Calendar"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.sql.Date"%>
<%@page import="Model.WeightGoal"%>
<%@page import="Model.DistanceGoal"%>
<%@page import="Model.CustomGoal"%>
<%@page import="Model.FitnessGoal"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:useBean id="aUser" type="Model.User" scope="session" />
        <jsp:useBean id="goal" type="Model.Goal" scope="session" />


        <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400italic' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="css/style2.css" type="text/css" />

        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!---strat-slider---->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/slider-style.css" />
        <script type="text/javascript" src="js/modernizr.custom.28468.js"></script>
        <!---//start-slider---->
        <!---start-login-script-->
        <script src="js/login.js"></script>
        <!---//End-login-script--->
        <!--768px-menu---->
        <link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
        <script type="text/javascript" src="js/jquery.mmenu.js"></script>
        <script type="text/javascript">
            //	The menu on the left
            $(function() {
            $('nav#menu-left').mmenu();
            });
        </script>


        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${goal.goalName}</title>
    </head>
    <body>

        <div class="header">
            <div class="wrap">
                <!--<div class="header-left">
                        <div class="logo">
                                <a href="index.jsp">HealthTracker</a>
                        </div>
                </div>-->
                <div class="header-right2">
                    <div class="top-nav">
                        <ul>
                            <%  String username = aUser.getUsername();
                                String firstLetter = username.substring(0, 1);
                                String remainingLetters = username.substring(1, username.length());
                                username = firstLetter.toUpperCase() + remainingLetters;

                            %>

                            <li><a href="/HealthTracker/ProfilePageController?pageOwner=<%=aUser.getUsername()%>"><%= username%></a></li>
                            <li><a href="/HealthTracker/DataCaptureController?type=clicked">Update Activities</a></li>
                            <li><a href="/HealthTracker/GoalController?type=clicked">Goals</a></li>
                            <li><a href="/HealthTracker/GroupController?type=clicked">Groups</a></li>
                            <li><a href="/HealthTracker/HistoryController?type=history">History</a></li>
                            <li><a href="/HealthTracker/AchievmentController?type=clicked">Achievements</a></li>
                            <li><a href="/HealthTracker/HomeController?type=clickedEdit">Edit Profile</a></li>
                            <li><a href="/HealthTracker/HistoryController?type=activityStats">Activity Stats</a></li>
                        </ul>
                    </div>

                    <div class="clear"> </div>
                    </ul>
                </div>
                <div class="clear"> </div>
            </div>
            <div class="clear"> </div>
        </div>
        <br />

        <a href="/HealthTracker/HomeController?type=clicked"><h3>Home</h3></a> <a style="margin-left: 70%" href="/HealthTracker/LoginController?type=logout">Log out</a><br />

        <h3>${goal.goalName}</h3>

        Goal Name: ${goal.goalName} <br />

        Start Date: ${goal.startDate} <br />

        Due Date: ${goal.dueDate} <br />

        <%

            if (goal.getClass() == FitnessGoal.class) {
                FitnessGoal fg = (FitnessGoal) goal;

        %>     

        Target Heart Rate: <%=fg.getTargetHeartRate()%> <br />

        Current Heart Rate: ${aUser.heartRate} <br />

        <%                    }
        %>


        <%

            if (goal.getClass() == CustomGoal.class) {
                CustomGoal cg = (CustomGoal) goal;

        %>     

        Target Heart Rate: <%=cg.getTargetHeartRate()%> <br />

        Current Heart Rate: ${aUser.heartRate} <br />

        Target Weight: <%=cg.getTargetWeight()%> <br />

        Current Weight: ${aUser.weight} <br />

        Target Distance: <%=cg.getTargetDistance()%> km while <%=cg.getActivityType()%><br />

        Achieved Distance: <%=cg.getAchievedDistance()%> km <br />

        <%                    }
        %>

        <%

            if (goal.getClass() == DistanceGoal.class) {
                DistanceGoal dg = (DistanceGoal) goal;

        %>     

        Target Distance: <%=dg.getTargetDistance()%> km while <%=dg.getActivityType()%><br />

        Achieved Distance: <%=dg.getAchievedDistance()%> km <br />

        <%                    }
        %>

        <%

            if (goal.getClass() == WeightGoal.class) {
                WeightGoal wg = (WeightGoal) goal;

        %>     

        Target Weight: <%=wg.getTargetWeight()%> <br />

        Current Weight: ${aUser.weight} <br />

        <%                    }
        %>
        <script type="text/javascript">

            function modifyGoal() {
            if (document.getElementById('modifyWeight').style.display === 'block') {
            document.getElementById('modifyWeight').style.display = 'none';
            } else {
            document.getElementById('modifyWeight').style.display = 'block';
                }

            }
            
            function setMonth(month) {
            var dropdown = document.getElementById("monthBox");
            for ( var i = 0; i < dropdown.options.length; i++ ) {
            if ( dropdown.options[i].value == month+1 ) {
            dropdown.options[i].selected = true;
            return;
                    }
                }
            }
            
            function setYear(year) {
            var dropdown = document.getElementById("yearBox");
            for ( var i = 0; i < dropdown.options.length; i++ ) {
            if ( dropdown.options[i].value == year ) {
            dropdown.options[i].selected = true;
            return;
                    }
                }
            }

        </script>

        <button onclick="modifyGoal();">Modify Goal...</button>
        <%!int year;%>
        <%!Date startDate;%>
        <%
            if (goal.getClass() == WeightGoal.class) {
                WeightGoal wg = (WeightGoal) goal;                
                int day = wg.getDueDate().getDate();
                year = wg.getDueDate().getYear() +1900;
                int month = wg.getDueDate().getMonth();
                startDate = wg.getStartDate();
        %>

        -
        <div id="modifyWeight" style="display:none" >

            <form action="/HealthTracker/GoalController" method="POST">
                <input type="hidden" name="type" value="modifyWeightGoal">
                <label for="name">Goal Name:</label>
                <input type="text" name="goalName" value="<%=wg.getGoalName()%>" readOnly/> <br />
                <label for="targetWeight">Target Weight:</label>
                <input type="text" name="targetWeight" value="<%=wg.getTargetWeight()%>" /> <br />
                <label for="Date">Due Date:</label><br />        
                day: <input type="text" name="day_weight" size="3" maxlength="2"
                            value="<%=day%>">
                month: <select size="1" name="month_weight" id="monthBox">
                    <option value="01">January</option>
                    <option value="02">February</option>
                    <option value="03">March</option>
                    <option value="04">April</option>
                    <option value="05" >May</option>
                    <option value="06">June</option>
                    <option value="07">July</option>
                    <option value="08">August</option>
                    <option value="09">September</option>
                    <option value="10" >October</option>
                    <option value="11">November</option>
                    <option value="12" >December</option>
                </select>
                year: <select size="1" name="year_weight" id="yearBox">
                    <option value="2014">2014</option>
                    <option value="2015">2015</option>
                    <option value="2016">2016</option>
                </select><br />

                <input id = "weightType" type = "hidden" value = "W" name = "type1" />
                <input type="hidden" name="startDate" value="<%=startDate%>"/>
                <input class="sub" type="submit" value="Submit" />
                
            </form> 
                <script type="text/javascript">
                    setMonth(<%=month%>);
                    setYear(<%=year%>);
                </script>
        </div>                
        <%
            }
        %>

    </body>
</html>
