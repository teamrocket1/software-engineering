<%-- 
    Document   : SearchResults
    Created on : Mar 25, 2014, 2:53:06 PM
    Author     : Odie
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:useBean id="aUser" type="Model.User" scope="session" />
        <jsp:useBean id="usernames" type="ArrayList<String>" scope="session" />
        <jsp:useBean id="goalnames" type="ArrayList<String>" scope="session" />
        <jsp:useBean id="glossaryNames" type="ArrayList<String>" scope="session" />
        <jsp:useBean id="glossaryDefinitions" type="ArrayList<String>" scope="session" />
        
        
        <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400italic' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="css/style2.css" type="text/css" />

        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!---strat-slider---->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/slider-style.css" />
        <script type="text/javascript" src="js/modernizr.custom.28468.js"></script>
        <!---//start-slider---->
        <!---start-login-script-->
        <script src="js/login.js"></script>
        <!---//End-login-script--->
        <!--768px-menu---->
        <link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
        <script type="text/javascript" src="js/jquery.mmenu.js"></script>
        <script type="text/javascript">
            //	The menu on the left
            $(function() {
                $('nav#menu-left').mmenu();
            });
        </script>
        
        <script type="text/javascript">
            
            function showDefinition(){
                
            }
            
        </script>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search Results</title>
    </head>
    <body>
        
        <div class="header">
            <div class="wrap">
                <!--<div class="header-left">
                        <div class="logo">
                                <a href="index.jsp">HealthTracker</a>
                        </div>
                </div>-->
                <div class="header-right2">
                    <div class="top-nav">
                        <ul>
                            <%                                                String username = aUser.getUsername();
                                String firstLetter = username.substring(0, 1);
                                String remainingLetters = username.substring(1, username.length());
                                username = firstLetter.toUpperCase() + remainingLetters;

                            %>

                            <li><a href="/HealthTracker/ProfilePageController?pageOwner=<%=aUser.getUsername()%>"><%= username%></a></li>
                            <li><a href="/HealthTracker/DataCaptureController?type=clicked">Update Activities</a></li>
                            <li><a href="/HealthTracker/GoalController?type=clicked">Goals</a></li>
                            <li><a href="/HealthTracker/GroupController?type=clicked">Groups</a></li>
                            <li><a href="/HealthTracker/HistoryController?type=history">History</a></li>
                            <li><a href="/HealthTracker/AchievmentController?type=clicked">Achievements</a></li>
                            <li><a href="/HealthTracker/HomeController?type=clickedEdit">Edit Profile</a></li>
                            <li><a href="/HealthTracker/HistoryController?type=activityStats">Activity Stats</a></li>
                        </ul>
                    </div>

                    <div class="clear"> </div>
                    </ul>
                </div>
                <div class="clear"> </div>
            </div>
            <div class="clear"> </div>
        </div>
                            
        <!-- Search Form -->
        <form style="margin-left: 70%" action="/HealthTracker/SearchController" method="POST">
            <input type="text" name="query" placeholder="Search for..."><br />
            On Site
            <input type="radio" onclick="javascript:searchWhere();" id="searchSite" />Google
            <input type="radio" onclick="javascript:searchWhere();" id="searchGoogle"/>
            <input type = "hidden" value = "site" name = "placeToSearch" id="decision" /> 
            <input class="sub" type="submit" value="Submit" />
        </form>
        
        <h1>Search Results</h1>
        
        <%
            // SHow user results
            if (session.getAttribute("usernames") != null){
                
                ArrayList<String> list = (ArrayList<String>) session.getAttribute("usernames");
                Iterator it = list.iterator();

                String name = "";
                
                while (it.hasNext()){
                    
                    name = (String) it.next();
                    
                    
                    %>
                   
                    <a href="/HealthTracker/ProfilePageController?pageOwner=<%=name%>"><%out.println(name);%></a>
                    
                    <%
                }
                
                
            }
            
            
            %>
            
            <%
            
            // show goal results
            if (session.getAttribute("goalnames") != null){
                
                ArrayList<String> list = (ArrayList<String>) session.getAttribute("goalnames");
                Iterator it = list.iterator();

                String goal = "";
                
                while (it.hasNext()){
                    
                    goal = (String) it.next();
                    
                    
                    %>
                   
                    <a href="/HealthTracker/GoalPageController?goalName=<%=goal%>&goalOwner=${aUser.username}"><%out.println(goal);%></a>
                    
                    <%
                }
                
                
            }
            
            
            %>
            
            <%
            
            // show glossary results
            if (session.getAttribute("glossaryNames") != null){
                
                ArrayList<String> list = (ArrayList<String>) session.getAttribute("glossaryNames");
                ArrayList<String> definitions = (ArrayList<String>) session.getAttribute("glossaryDefinitions");
                Iterator it = list.iterator();

                String term = "";
                int i = -1;
                while (it.hasNext()){
                    
                    term = (String) it.next();
                    
                    i++;
                    %>
                   
                    <a href="javascript:alert('<%=definitions.get(i)%>')"><%out.println(term);%></a>
                    
                    <%
                }
                
                
            }
            
            
            %>
        
    </body>
</html>
