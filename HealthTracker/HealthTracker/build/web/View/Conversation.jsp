<%-- 
    Document   : Conversation
    Created on : Mar 31, 2014, 7:11:46 PM
    Author     : Odie
--%>

<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:useBean id="aUser" type="Model.User" scope="session" />
        <jsp:useBean id="messages" type="ArrayList<String>" scope="session" />
        <jsp:useBean id="ordering" type="ArrayList<String>" scope="session" />


        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

        <style>

            #message {
                width: 100%;
            }

            .received {
                width: 100%;
                margin-left: 0%;

            }

            .reply {
                width: 100%;
                margin-left: 35%;

            }

            #convoform {
                width: 100%;
            }

            #submit {
                width: 100%;
                margin-left: 80%;
            }

            input {
                align: right;
            }

        </style>

    </head>
    <body>
        <h1>Convo wid a Ho!</h1>


        <%

            for(int i = 0; i < messages.size(); i++){
                
                if(ordering.get(i).equalsIgnoreCase("sent")){
                   %>
                    
                   <div class="received" style="background: lightgreen; border-color:hotpink; height:75px; width: 65%">
                    <%
                    out.println(messages.get(i));
                    %>
                </div><br /> 
                   
                   <%
                }
                
                else if(ordering.get(i).equalsIgnoreCase("received")){
                    %>
                    
                    <div class="reply" style="background:cyan; height:75px; width: 65%">
                    <%
                    out.println(messages.get(i));
                    %>
                </div><br /> 
                            
                            
                            <%
                    
                }
                
            }
            
        %>


        <form class ="convoform" action="MessageController" method="POST">
            <textarea style="width: 80%; margin-left: 10%; margin-right: 10%;" name="msg" cols="78" rows="5" ></textarea>
            <input type="hidden" value="postMessage" name="type"/>
            <input type="hidden" value="<%= aUser.getUsername()%>" name="sender" />
            <input type="hidden" value="<%= request.getParameter("sender")%>" name="firstSender" />
            <input type="hidden" value="<%= request.getParameter("sender")%>" name="recipient" />
            <input type="hidden" value="<%=request.getParameter("party")%>" name="party" />
            <div id="submit">
                <input  type="submit" value="REPLY" />
            </div>
        </form>
    </body>
</html>
