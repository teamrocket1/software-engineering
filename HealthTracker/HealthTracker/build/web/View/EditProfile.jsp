<%-- 
    Document   : EditProfile
    Created on : 12-Mar-2014, 16:18:52
    Author     : James
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:useBean id="aUser" type="Model.User" scope="session" />

        <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400italic' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="css/style2.css" type="text/css" />

        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!---strat-slider---->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/slider-style.css" />
        <script type="text/javascript" src="js/modernizr.custom.28468.js"></script>
        <!---//start-slider---->
        <!---start-login-script-->
        <script src="js/login.js"></script>
        <!---//End-login-script--->
        <!--768px-menu---->
        <link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
        <script type="text/javascript" src="js/jquery.mmenu.js"></script>
        <script type="text/javascript">
            //	The menu on the left
            $(function() {
                $('nav#menu-left').mmenu();
            });
        </script>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Profile</title>
    </head>
    <body>

        <div class="header">
            <div class="wrap">
                <!--<div class="header-left">
                        <div class="logo">
                                <a href="index.jsp">HealthTracker</a>
                        </div>
                </div>-->
                <div class="header-right2">
                    <div class="top-nav">
                        <ul>
                            <%
                                String username = aUser.getUsername();
                                String firstLetter = username.substring(0, 1);
                                String remainingLetters = username.substring(1, username.length());
                                username = firstLetter.toUpperCase() + remainingLetters;

                            %>

                            <li><a href="/HealthTracker/ProfilePageController?pageOwner=<%=aUser.getUsername()%>"><%= username%></a></li>
                            <li><a href="/HealthTracker/DataCaptureController?type=clicked">Update Activities</a></li>
                            <li><a href="/HealthTracker/GoalController?type=clicked">Goals</a></li>
                            <li><a href="/HealthTracker/GroupController?type=clicked">Groups</a></li>
                            <li><a href="/HealthTracker/HistoryController?type=history">History</a></li>
                            <li><a href="/HealthTracker/AchievmentController?type=clicked">Achievements</a></li>
                            <li><a href="/HealthTracker/HomeController?type=clickedEdit">Edit Profile</a></li>
                            <li><a href="/HealthTracker/HistoryController?type=activityStats">Activity Stats</a></li>
                        </ul>
                    </div>

                    <div class="clear"> </div>
                    </ul>
                </div>
                <div class="clear"> </div>
            </div>
            <div class="clear"> </div>
        </div>
        <br />

       

        <div class="user3">
             <h1>Edit Your Profile</h1>
            <div class="user">
                <div class ="profilepic">
                    <img src="http://www.sportspickle.com/wp-content/uploads/2012/11/169e1e366bf6a0a16ed19e3eb0bdf2ba.jpg" alt="some_text">
                </div>

                <ul>Hello <b>${aUser.firstName} ${aUser.lastName}</b></ul>
                <ul><b>${aUser.username}</b></ul>
                <!--<ul>and ... I wont tell anyone but ..your password. (>_>) (<_<) Its <b>${aUser.password}</b> (o_o) <br /><br /></ul>-->
                <!--<ul>Your email is <b>${aUser.email}</b> and you're a <b>${aUser.age}</b> year old <b>${aUser.sex}</b>. <br /><br /></ul>
                <ul>Your weight is <b>${aUser.weight}</b>, your height is <b>${aUser.height}</b> and your resting heart rate is <b>${aUser.heartRate}</b>.<br /><br/></ul>
                <ul>How'd I know all this? SESSIONS! THATS HOW BITCH! \(^_^)/</ul>-->
                <ul><b>Goals Achieved: 198</b></ul>
                <ul>
                    <%  double w = (double) aUser.getWeight();
                        double h = (double) aUser.getHeight();
                        double BMI = 0;

                        if (w != 0 && h != 0) {
                            BMI = w / Math.pow(h, 2);
                        }
                        String message = null;

                        if (BMI < 18.5) {
                            message = "Underweight";
                        } else if (BMI >= 18.5 && BMI < 25) {
                            message = "Healthy";
                        } else if (BMI >= 25 && BMI < 30) {
                            message = "Overweight";
                        } else {
                            message = "Very overweight";
                        }

                    %>
                    Your BMI is <b><%out.println(BMI);%></b>, this means you are <%out.println(message);%></ul><br>
                <hr>
                <br>

                <ul><li><a href="/HealthTracker/HomeController?type=clickedEdit">Edit Profile</a></li></ul>
                <ul>My Groups</ul>
                <ul><p>Uni Group</p><ul>
                        <ul><p>Family Group</p><ul>        
                                <br/>
                                <hr>
                                <br>
                                <ul><h3>Recent Achievements</h3></ul>
                                <ul>Ran 1000 miles</ul>
                                <ul>Walked a mile in my shoes</ul>
                                <ul>Lost 1 KG in weight</ul>

                                </div>
                <div class="userEditProfile">
                    
                     <a href="/HealthTracker/HomeController?type=clicked"><h3>Home</h3></a>

                                <form action="/HealthTracker/UserController" method="POST">
                                    
                                    <p><label for="name">Name</label> <input type="text" id="name" /></p>

                                    <p><label for="name">User Name</label><input type="text" name="username" value="${aUser.username}" readonly></p>
                                    
                                    
                                    <p><label for="name">First name:</label><input type="text" name="firstname" value="${aUser.firstName}"></p>
                                    
                                    <p><label for="name">Last name:</label><input type="text" name="lastname" value="${aUser.lastName}"></p>      
                                    
                                    <p><label for="name">Password:</label><input type="password" name="password" value="${aUser.password}"
                                           maxlength ="20" title="Uour password must conatin at least eight symbols containing at least one number, one lower, and one upper letter. Thank You"
                                           pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" required></p>
                                    <p><label for="name">Email:</label><input type="email" name="email" value="${aUser.email}"
                                           maxlength="30"></p>
                                    <p><label for="name">Sex:</label><input title="Please enter 'm' for male, 'f' for femal or 'o' for
                                           other. Thank You." type="text" name="sex" 
                                           value="${aUser.sex}" maxlength="1" pattern="[mfo]"></p>
                                    <p><label for="name">Age:</label><input type="text" name="age" value="${aUser.age}"></p>
                                    <p><label for="name">Weight:</label><input type="text" name="weight" value="${aUser.weight}"> 
                                    <select name="weightUnit">
                                        <option value="Kg">Kg</option>
                                        <option value="lbs">lbs</option>
                                    </select></p>
                                    <p><label for="name">Height:</label><input type="text" name="height" value="${aUser.height}"></p>
                                    <p><label for="name">Heart Rate:</label><input type="text" name="heartrate" value="${aUser.heartRate}"></p>
                                    <input type = "hidden" value = "UpdateProfile" name = "type">
                                    <input type="submit" value="Update Profile"> 

                                    <!--<button>L</button>-->
                                </form>
                    
                </div>
                </div>




                               
                                <img src="/HealthTracker/Images/that-booty-had-me-like-gif.gif" /><br />
                                </body>
                                </html>