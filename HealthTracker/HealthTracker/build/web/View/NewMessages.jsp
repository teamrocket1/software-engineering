<%-- 
    Document   : NewMessages
    Created on : Mar 31, 2014, 5:33:45 PM
    Author     : Odie
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400italic' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="css/styles.css" type="text/css" />

        <!--[if IE]>
                <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]--> 

        <jsp:useBean id="aUser" type="Model.User" scope="session" />
        <title>JSP Page</title>
    </head>
    <body>

        <%
            if (session.getAttribute("sent") == null) {
        %>
        <form action="/HealthTracker/MessageController" method="POST">
            Send to: <input type="text" name="recipient" /><br />
            Message: <br /> <textarea name="msg" cols="40" rows="15" ></textarea><br />
            <input type="hidden" value="firstMessage" name="type" />
            <input type="hidden" value="<% out.print(aUser.getUsername());%>" name="sender" />
            <input type="submit" value="submit" />
        </form>
            <%
                           }
            %>
        
 
        <%  if (session.getAttribute("sent") != null) {
                int sent = (Integer) session.getAttribute("sent");
                session.removeAttribute("sent");
                if(sent == 1) {
                    %>
                    
                    Your Message has been sent. You may close this window.

                    <%
                }
                if(sent == 2) {
                    %>
                    
                    There was an error sending your message. Please try again later.<br /> 
                    You may close this window.
                    
                    <%
                }
            }
        %>

        
    </body>
</html>

