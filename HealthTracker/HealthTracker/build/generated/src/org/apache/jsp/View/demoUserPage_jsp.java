package org.apache.jsp.View;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import Model.DistanceGoal;
import Model.User;
import java.util.ArrayList;
import Model.WeightGoal;
import Utilities.DBAccess;
import java.sql.Connection;

public final class demoUserPage_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("    <head>\r\n");
      out.write("        ");
      Model.User aUser = null;
      aUser = (Model.User) _jspx_page_context.getAttribute("aUser", PageContext.SESSION_SCOPE);
      if (aUser == null){
        throw new java.lang.InstantiationException("bean aUser not found within scope");
      }
      out.write("\r\n");
      out.write("        ");
      ArrayList<WeightGoal> weightGoals = null;
      weightGoals = (ArrayList<WeightGoal>) _jspx_page_context.getAttribute("weightGoals", PageContext.SESSION_SCOPE);
      if (weightGoals == null){
        throw new java.lang.InstantiationException("bean weightGoals not found within scope");
      }
      out.write("\r\n");
      out.write("        ");
      ArrayList<DistanceGoal> distanceGoals = null;
      distanceGoals = (ArrayList<DistanceGoal>) _jspx_page_context.getAttribute("distanceGoals", PageContext.SESSION_SCOPE);
      if (distanceGoals == null){
        throw new java.lang.InstantiationException("bean distanceGoals not found within scope");
      }
      out.write("\r\n");
      out.write("\r\n");
      out.write("    <head>\r\n");
      out.write("        <title>Health Tracker</title>\r\n");
      out.write("        <script>\r\n");
      out.write("            var patt = new RegExp(\"e\");\r\n");
      out.write("            var passwordPatt = new RegExp(\"e\");\r\n");
      out.write("            var pat = /[0-9A-Za-z]+/;\r\n");
      out.write("        </script>\r\n");
      out.write("        <link href=\"css/style.css\" rel='stylesheet' type='text/css' />\r\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n");
      out.write("        <link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"images/fav-icon.png\" />\r\n");
      out.write("        <script type=\"application/x-javascript\"> addEventListener(\"load\", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>\r\n");
      out.write("    </script>\r\n");
      out.write("    <!---strat-slider---->\r\n");
      out.write("    <script type=\"text/javascript\" src=\"js/jquery.min.js\"></script>\r\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" href=\"css/slider-style.css\" />\r\n");
      out.write("    <script type=\"text/javascript\" src=\"js/modernizr.custom.28468.js\"></script>\r\n");
      out.write("    <!---//start-slider---->\r\n");
      out.write("    <!---start-login-script-->\r\n");
      out.write("    <script src=\"js/login.js\"></script>\r\n");
      out.write("    <!---//End-login-script--->\r\n");
      out.write("    <!--768px-menu---->\r\n");
      out.write("    <link type=\"text/css\" rel=\"stylesheet\" href=\"css/jquery.mmenu.all.css\" />\r\n");
      out.write("    <script type=\"text/javascript\" src=\"js/jquery.mmenu.js\"></script>\r\n");
      out.write("    <script type=\"text/javascript\">\r\n");
      out.write("            //\tThe menu on the left\r\n");
      out.write("            $(function() {\r\n");
      out.write("                $('nav#menu-left').mmenu();\r\n");
      out.write("            });\r\n");
      out.write("    </script>\r\n");
      out.write("\r\n");
      out.write("    <!-- @start of script/hrefs for sign up pop up-->\r\n");
      out.write("\r\n");
      out.write("    <link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic' rel='stylesheet' type='text/css'>\r\n");
      out.write("\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/demo.css\">\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"css/avgrund.css\">\r\n");
      out.write("\r\n");
      out.write("    <script>\r\n");
      out.write("        function openDialog() {\r\n");
      out.write("            Avgrund.show(\"#default-popup\");\r\n");
      out.write("        }\r\n");
      out.write("        function closeDialog() {\r\n");
      out.write("            Avgrund.hide();\r\n");
      out.write("        }\r\n");
      out.write("    </script>\r\n");
      out.write("    <!-- @ end of script for sign up pop up-->            \r\n");
      out.write("\r\n");
      out.write("    <!--//768px-menu-->\r\n");
      out.write("\r\n");
      out.write("    <script type=\"text/javascript\">\r\n");
      out.write("\r\n");
      out.write("        function searchWhere() {\r\n");
      out.write("            if (document.getElementById('searchSite').checked) {\r\n");
      out.write("                document.getElementById(\"decision\").value = \"site\";\r\n");
      out.write("            } else {\r\n");
      out.write("                document.getElementById(\"decision\").value = \"google\";\r\n");
      out.write("            }\r\n");
      out.write("        }\r\n");
      out.write("\r\n");
      out.write("    </script>\r\n");
      out.write("\r\n");
      out.write("    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("    <title>User Page (^_^)</title>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("    <!--insert Doughnut-->\r\n");
      out.write("    <title>Doughnut Chart</title>\r\n");
      out.write("    <script src=\"js/Chart.js\"></script>\r\n");
      out.write("    <style>\r\n");
      out.write("        canvas{\r\n");
      out.write("        }\r\n");
      out.write("    </style>\r\n");
      out.write("\r\n");
      out.write("    <!--end of chart.js-->\r\n");
      out.write("\r\n");
      out.write("</head>\r\n");
      out.write("<body <!--style=\"padding-top: 70px;-->\">\r\n");
      out.write("\r\n");
      out.write("    <!---start-wrap---->\r\n");
      out.write("    <!------start-768px-menu---->\r\n");
      out.write("\r\n");
      out.write("    <!------start-768px-menu---->\r\n");
      out.write("    <!---start-header---->\r\n");
      out.write("\r\n");
      out.write("    <!--<div class=\"header\">\r\n");
      out.write("        <div class=\"wrap\">\r\n");
      out.write("            <div class=\"header-left\">\r\n");
      out.write("                <div class=\"logo\">\r\n");
      out.write("                    <a href=\"index.jsp\">Health Tracker</a>\r\n");
      out.write("                </div>                \r\n");
      out.write("            </div>\r\n");
      out.write("        </div>\r\n");
      out.write("    </div>\r\n");
      out.write("    -->\r\n");
      out.write("    <!---//End-header---->\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("    ");

        // Checks if time for goal has run out. This had to happen periodically and because we can have a thread for each
        // user, It checks everytime the user goes to their home page.
        try {

            java.util.Date utilDate = new java.util.Date();
            java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

            ArrayList<WeightGoal> weightGoalss = WeightGoal.getWeightGoalsForUser(aUser.getUsername());

            for (WeightGoal wg : weightGoalss) {
                if (wg.getDueDate().before(currentDate)) {
                    String sql = "DELETE FROM Goal WHERE goalOwner = '" + aUser.getUsername()
                            + "' AND goalName = '" + wg.getGoalName() + "'";

                    String sql2 = "INSERT INTO History VALUES('" + aUser.getUsername() + "', 'You failed to achieve your Weight Goal " + wg.getGoalName()
                            + ". But dont give up!', '" + currentDate + "')";

                    DBAccess.insertStatement(sql);
                    DBAccess.insertStatement(sql2);
                }
            }
        } catch (Exception e) {
        } finally {
        }
    
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("    <div class=\"header\">\r\n");
      out.write("        <div class=\"wrap\">\r\n");
      out.write("            <div class=\"header-right2\">\r\n");
      out.write("                <div class=\"top-nav\">\r\n");
      out.write("                    <ul>\r\n");
      out.write("                        ");
                                                String username = aUser.getUsername();
                            String firstLetter = username.substring(0, 1);
                            String remainingLetters = username.substring(1, username.length());
                            username = firstLetter.toUpperCase() + remainingLetters;

                        
      out.write("\r\n");
      out.write("\r\n");
      out.write("                        <li><a href=\"/HealthTracker/ProfilePageController?pageOwner=");
      out.print(aUser.getUsername());
      out.write('"');
      out.write('>');
      out.print( username);
      out.write("</a></li>\r\n");
      out.write("                        <li><a href=\"/HealthTracker/DataCaptureController?type=clicked\">Update Activities</a></li>\r\n");
      out.write("                        <li><a href=\"/HealthTracker/GoalController?type=clicked\">Goals</a></li>\r\n");
      out.write("                        <li><a href=\"/HealthTracker/GroupController?type=clicked\">Groups</a></li>\r\n");
      out.write("                        <li><a href=\"/HealthTracker/HistoryController?type=history\">History</a></li>\r\n");
      out.write("                        <li><a href=\"/HealthTracker/AchievmentController?type=clicked\">Achievements</a></li>\r\n");
      out.write("                        <li><a href=\"/HealthTracker/HomeController?type=clickedEdit\">Edit Profile</a></li>\r\n");
      out.write("                        <li><a href=\"/HealthTracker/HistoryController?type=activityStats\">Activity Stats</a></li>\r\n");
      out.write("                    </ul>\r\n");
      out.write("                </div>\r\n");
      out.write("\r\n");
      out.write("                <div class=\"clear\"> </div>\r\n");
      out.write("                </ul>\r\n");
      out.write("            </div>\r\n");
      out.write("            <div class=\"clear\"> </div>\r\n");
      out.write("        </div>\r\n");
      out.write("        <div class=\"clear\"> </div>\r\n");
      out.write("    </div>\r\n");
      out.write("    <br />\r\n");
      out.write("    <br />\r\n");
      out.write("\r\n");
      out.write("    <div class=\"user3\">\r\n");
      out.write("    <div class=\"user\">\r\n");
      out.write("        <div class =\"profilepic\">\r\n");
      out.write("            <img src=\"http://www.sportspickle.com/wp-content/uploads/2012/11/169e1e366bf6a0a16ed19e3eb0bdf2ba.jpg\" alt=\"some_text\">\r\n");
      out.write("        </div>\r\n");
      out.write("\r\n");
      out.write("        <ul>Hello <b>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${aUser.firstName}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(' ');
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${aUser.lastName}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</b></ul>\r\n");
      out.write("        <ul><b>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${aUser.username}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</b></ul>\r\n");
      out.write("        <!--<ul>and ... I wont tell anyone but ..your password. (>_>) (<_<) Its <b>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${aUser.password}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</b> (o_o) <br /><br /></ul>-->\r\n");
      out.write("        <!--<ul>Your email is <b>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${aUser.email}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</b> and you're a <b>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${aUser.age}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</b> year old <b>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${aUser.sex}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</b>. <br /><br /></ul>\r\n");
      out.write("        <ul>Your weight is <b>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${aUser.weight}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</b>, your height is <b>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${aUser.height}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</b> and your resting heart rate is <b>");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${aUser.heartRate}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</b>.<br /><br/></ul>\r\n");
      out.write("        <ul>How'd I know all this? SESSIONS! THATS HOW BITCH! \\(^_^)/</ul>-->\r\n");
      out.write("        <ul><b>Goals Achieved: 198</b></ul>\r\n");
      out.write("        <ul>\r\n");
      out.write("            ");
  double w = (double) aUser.getWeight();
                double h = (double) aUser.getHeight();
                double BMI = 0;

                if (w != 0 && h != 0) {
                    BMI = w / Math.pow(h, 2);
                }
                String message = null;

                if (BMI < 18.5) {
                    message = "Underweight";
                } else if (BMI >= 18.5 && BMI < 25) {
                    message = "Healthy";
                } else if (BMI >= 25 && BMI < 30) {
                    message = "Overweight";
                } else {
                    message = "Very overweight";
                }

            
      out.write("\r\n");
      out.write("            Your BMI is <b>");
out.println(BMI);
      out.write("</b>, this means you are ");
out.println(message);
      out.write("</ul><br>\r\n");
      out.write("        <hr>\r\n");
      out.write("        <br>\r\n");
      out.write("\r\n");
      out.write("        <ul><li><a href=\"/HealthTracker/HomeController?type=clickedEdit\">Edit Profile</a></li></ul>\r\n");
      out.write("        <ul>My Groups</ul>\r\n");
      out.write("        <ul><p>Uni Group</p><ul>\r\n");
      out.write("                <ul><p>Family Group</p><ul>        \r\n");
      out.write("                        <br/>\r\n");
      out.write("                        <hr>\r\n");
      out.write("                        <br>\r\n");
      out.write("                        <ul><h3>Recent Achievements</h3></ul>\r\n");
      out.write("                        <ul>Ran 1000 miles</ul>\r\n");
      out.write("                        <ul>Walked a mile in my shoes</ul>\r\n");
      out.write("                        <ul>Lost 1 KG in weight</ul>\r\n");
      out.write("\r\n");
      out.write("                        </div>\r\n");
      out.write("\r\n");
      out.write("                        <div class=\"user2\">\r\n");
      out.write("\r\n");
      out.write("                            <div class=\"Totals\">\r\n");
      out.write("                                \r\n");
      out.write("                                <div class=\"Totalsa\">\r\n");
      out.write("                                   \r\n");
      out.write("                                    <div class=\"TotIcon\">\r\n");
      out.write("                                     <img src=\"View/IconImages/IconRun.png\" alt=\"IconRun\" width=\"100\" height=\"100\">   \r\n");
      out.write("                                    </div>   \r\n");
      out.write("                                    \r\n");
      out.write("                                    <ul>Total Running Distance = 10m </ul>\r\n");
      out.write("                                </div>\r\n");
      out.write("                              \r\n");
      out.write("                                <div class=\"Totalsa\">\r\n");
      out.write("                                    \r\n");
      out.write("                                    <div class=\"TotIcon\">\r\n");
      out.write("                                     <img src=\"View/IconImages/IconBike.png\" alt=\"IconRun\" width=\"100\" height=\"100\">   \r\n");
      out.write("                                    </div>  \r\n");
      out.write("                                    \r\n");
      out.write("                                     <ul>Total Biking Distance = 140m</ul>\r\n");
      out.write("                                </div>\r\n");
      out.write("                                \r\n");
      out.write("                                <div class=\"Totalsa\">\r\n");
      out.write("                                    \r\n");
      out.write("                                    <div class=\"TotIcon\">\r\n");
      out.write("                                     <img src=\"View/IconImages/IconScale.png\" alt=\"IconRun\" width=\"100\" height=\"100\">   \r\n");
      out.write("                                    </div>  \r\n");
      out.write("                                    \r\n");
      out.write("                                    <ul>Total Weight loss in 6 months = 2 KG</ul>\r\n");
      out.write("                                </div>\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <!-- @start CHART 1 -->\r\n");
      out.write("                            <div class=\"chart\">\r\n");
      out.write("                                <p> Weight Goal - ");
 if(weightGoals.size() > 0) out.println(weightGoals.get(0).getGoalName()); 
      out.write("</p>\r\n");
      out.write("                                <canvas id=\"canvas\" height=\"150\" width=\"150\"></canvas>\t\t\t\t\t\t\t\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <!-- @end CHART 1 -->\r\n");
      out.write("\r\n");
      out.write("                            <!-- @start CHART 2 -->\r\n");
      out.write("                            <div class=\"chart\">\r\n");
      out.write("                                <p> Distance Goal - ");
 if(distanceGoals.size() > 0) out.println(distanceGoals.get(0).getGoalName()); 
      out.write("</p>\r\n");
      out.write("                                <canvas id=\"canvas2\" height=\"150\" width=\"150\"></canvas>\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <!-- @end CHART 2 -->\r\n");
      out.write("\r\n");
      out.write("                            <!--@start CHART 3 -->\r\n");
      out.write("                            <div class=\"chart\">\r\n");
      out.write("                                <p> Weight Goal</p>\r\n");
      out.write("                                <canvas id=\"canvas3\" height=\"150\" width=\"150\"></canvas>\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <!-- @end CHART 3 -->\r\n");
      out.write("\r\n");
      out.write("                            <!-- @start CHART 4 -->\r\n");
      out.write("                            <div class=\"chart\">\r\n");
      out.write("                                <p> Running Goal 1</p>\r\n");
      out.write("                                <canvas id=\"canvas4\" height=\"150\" width=\"150\"></canvas>\t\t\t\t\t\t\t\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <!-- @end CHART 1 -->\r\n");
      out.write("\r\n");
      out.write("                            <!-- @start CHART 2 -->\r\n");
      out.write("                            <div class=\"chart\">\r\n");
      out.write("                                <p> Bike Riding Goal</p>\r\n");
      out.write("                                <canvas id=\"canvas5\" height=\"150\" width=\"150\"></canvas>\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <!-- @end CHART 2 -->\r\n");
      out.write("\r\n");
      out.write("                            <!--@start CHART 3 -->\r\n");
      out.write("                            <div class=\"chart\">\r\n");
      out.write("                                <p> Weight Goal</p>\r\n");
      out.write("                                <canvas id=\"canvas6\" height=\"150\" width=\"150\"></canvas>\r\n");
      out.write("                            </div>\r\n");
      out.write("                            <!-- @end CHART 3 -->\r\n");
      out.write("                            \r\n");
      out.write("                        </div>\r\n");
      out.write("        \r\n");
      out.write("                        <div class=\"user2\">\r\n");
      out.write("                            <p>Top Trophies<p>\r\n");
      out.write("                                <div class =\"Trophies\">\r\n");
      out.write("                             <h1>Joined Health Tracker</h1>   \r\n");
      out.write("                             <h1>Achieved 1st Group Goal</h1>   \r\n");
      out.write("                             <h1>Beat Running Distance PB</h1>   \r\n");
      out.write("                            \r\n");
      out.write("                        </div>    \r\n");
      out.write("                        </body>\r\n");
      out.write("\r\n");
      out.write("                        <!-- Script for the Pie Charts-->\r\n");
      out.write("\r\n");
      out.write("                        ");

                            if(weightGoals.size() > 0){
                                double progress = weightGoals.get(0).getPercentCompletion(aUser.getWeight());
                                
      out.write("\r\n");
      out.write("                             <script>   \r\n");
      out.write("                                var doughnutData = [\r\n");
      out.write("                                    {\r\n");
      out.write("                                    value: ");
      out.print( 100 - progress );
      out.write(",\r\n");
      out.write("                                        color: \"#F7464A\"\r\n");
      out.write("                                    },\r\n");
      out.write("                                    {\r\n");
      out.write("                                        value: ");
      out.print( progress );
      out.write(",\r\n");
      out.write("                                        color: \"#46BFBD\"\r\n");
      out.write("                                    }\r\n");
      out.write("\r\n");
      out.write("                                ];\r\n");
      out.write("                                \r\n");
      out.write("                                var myDoughnut = new Chart(document.getElementById(\"canvas\").getContext(\"2d\")).Doughnut(doughnutData);\r\n");
      out.write("                              </script>  \r\n");
      out.write("                                ");

                                
                            }
                        
      out.write("\r\n");
      out.write("                        \r\n");
      out.write("                        ");

                            double progress = 0;
                            if(distanceGoals.size() > 0){
                                progress = distanceGoals.get(0).getPercentCompletion();
                                
      out.write("\r\n");
      out.write("                             <script>   \r\n");
      out.write("                                var doughnutData = [\r\n");
      out.write("                                    {\r\n");
      out.write("                                    value: ");
      out.print( 100 - progress );
      out.write(",\r\n");
      out.write("                                        color: \"#F7464A\"\r\n");
      out.write("                                    },\r\n");
      out.write("                                    {\r\n");
      out.write("                                        value: ");
      out.print( progress );
      out.write(",\r\n");
      out.write("                                        color: \"#46BFBD\"\r\n");
      out.write("                                    }\r\n");
      out.write("\r\n");
      out.write("                                ];\r\n");
      out.write("                                \r\n");
      out.write("                                var myDoughnut = new Chart(document.getElementById(\"canvas2\").getContext(\"2d\")).Doughnut(doughnutData);\r\n");
      out.write("                              </script>  \r\n");
      out.write("                                ");

                                
                            }
                        
      out.write("\r\n");
      out.write("                        \r\n");
      out.write("                        <!--<script>\r\n");
      out.write("                                var doughnutData = [\r\n");
      out.write("                                    {\r\n");
      out.write("                                        value: 30,\r\n");
      out.write("                                        color: \"#F7464A\"\r\n");
      out.write("                                    },\r\n");
      out.write("                                    {\r\n");
      out.write("                                        value: 50,\r\n");
      out.write("                                        color: \"#46BFBD\"\r\n");
      out.write("                                    }\r\n");
      out.write("\r\n");
      out.write("                                ];\r\n");
      out.write("                                \r\n");
      out.write("                                var doughnutData2 = [\r\n");
      out.write("                                    {\r\n");
      out.write("                                        value: 30,\r\n");
      out.write("                                        color: \"#F7464A\"\r\n");
      out.write("                                    },\r\n");
      out.write("                                    {\r\n");
      out.write("                                        value: 50,\r\n");
      out.write("                                        color: \"#46BFBD\"\r\n");
      out.write("                                    }\r\n");
      out.write("                                ];\r\n");
      out.write("                                \r\n");
      out.write("                                var myDoughnut = new Chart(document.getElementById(\"canvas\").getContext(\"2d\")).Doughnut(doughnutData);\r\n");
      out.write("                                var myDoughnut = new Chart(document.getElementById(\"canvas2\").getContext(\"2d\")).Doughnut(doughnutData);\r\n");
      out.write("                                var myDoughnut = new Chart(document.getElementById(\"canvas3\").getContext(\"2d\")).Doughnut(doughnutData);\r\n");
      out.write("                                var myDoughnut = new Chart(document.getElementById(\"canvas4\").getContext(\"2d\")).Doughnut(doughnutData);\r\n");
      out.write("                                var myDoughnut = new Chart(document.getElementById(\"canvas5\").getContext(\"2d\")).Doughnut(doughnutData);\r\n");
      out.write("                                var myDoughnut = new Chart(document.getElementById(\"canvas6\").getContext(\"2d\")).Doughnut(doughnutData);\r\n");
      out.write("                        </script>\r\n");
      out.write("                        </div>\r\n");
      out.write("                        -->\r\n");
      out.write("\r\n");
      out.write("                        <div class=\"wrap\">\r\n");
      out.write("\r\n");
      out.write("                            <div class=\"clear\">\r\n");
      out.write("                                <a style=\"margin-left: 70%\" href=\"/HealthTracker/LoginController?type=logout\">Log out</a>\r\n");
      out.write("                                <form style=\"margin-left: 70%\" action=\"/HealthTracker/SearchController\" method=\"POST\">\r\n");
      out.write("                                    <input type=\"text\" name=\"query\" placeholder=\"Search for...\"><br />\r\n");
      out.write("                                    On Site\r\n");
      out.write("                                    <input type=\"radio\" onclick=\"javascript:searchWhere();\" id=\"searchSite\" />Google\r\n");
      out.write("                                    <input type=\"radio\" onclick=\"javascript:searchWhere();\" id=\"searchGoogle\"/>\r\n");
      out.write("                                    <input type = \"hidden\" value = \"site\" name = \"placeToSearch\" id=\"decision\" /> \r\n");
      out.write("                                    <input class=\"sub\" type=\"submit\" value=\"Submit\" />\r\n");
      out.write("                                </form>\r\n");
      out.write("\r\n");
      out.write("                            </div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("                            <!--start-content-->\r\n");
      out.write("                            <div class=\"content\">\r\n");
      out.write("                                <div class=\"wrap\">\r\n");
      out.write("                                    <div class=\"footer-grids\">\r\n");
      out.write("                                        <div class=\"wrap\">\r\n");
      out.write("                                            <div class=\"footer-grid\">\r\n");
      out.write("                                                <h3>Quick Links</h3>\r\n");
      out.write("                                                <ul>\r\n");
      out.write("                                                    <li><a href=\"#\">Home</a></li>\r\n");
      out.write("                                                    <li><a href=\"#\">About Features</a></li>\r\n");
      out.write("                                                    <li><a href=\"#\">Login</a></li>\r\n");
      out.write("                                                    <li><a href=\"#\">Sign Up</a></li>\r\n");
      out.write("                                                </ul>\r\n");
      out.write("                                            </div>\r\n");
      out.write("                                            <div class=\"footer-grid\">\r\n");
      out.write("                                                <h3>More</h3>\r\n");
      out.write("                                                <ul>\r\n");
      out.write("                                                    <li><a href=\"#\">FAQ</a></li>\r\n");
      out.write("                                                    <li><a href=\"#\">Support</a></li>\r\n");
      out.write("                                                    <li><a href=\"#\">Privacy Policy</a></li>\r\n");
      out.write("                                                    <li><a href=\"#\">Terms and Conditions</a></li>\r\n");
      out.write("                                                </ul>\r\n");
      out.write("                                            </div>\r\n");
      out.write("                                            <div class=\"footer-grid\">\r\n");
      out.write("                                                <h3>Connect With Us</h3>\r\n");
      out.write("                                                <ul class=\"social-icons\">\r\n");
      out.write("                                                    <li><a class=\"facebook\" href=\"#\"> </a></li>\r\n");
      out.write("                                                    <li><a class=\"twitter\" href=\"#\"> </a></li>\r\n");
      out.write("                                                    <li><a class=\"youtube\" href=\"#\"> </a></li>\r\n");
      out.write("                                                </ul>\r\n");
      out.write("                                            </div>\r\n");
      out.write("\r\n");
      out.write("                                            <div class=\"clear\"> </div>\r\n");
      out.write("                                        </div>\r\n");
      out.write("                                    </div>\r\n");
      out.write("\r\n");
      out.write("                                    <p class=\"copy-right\"><it><b>Disclaimer:</b> This application is not a commercial application and does not provide insurance. \r\n");
      out.write("                                        This is a study project that is part of a Computing Science module taught at the University of East Anglia,\r\n");
      out.write("                                        Norwich, UK. If you have any questions, please contact the module coordinator,\r\n");
      out.write("                                        Joost Noppen, at j.noppen@uea.ac.uk</it></p>\r\n");
      out.write("                                    <!---//End-bottom-footer-grids---->\r\n");
      out.write("                                </div>  \r\n");
      out.write("                                </body>\r\n");
      out.write("                                </html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
