/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 100024721
 */
public class DBAccess {

    private static Connection con;
    private static PreparedStatement ps;

    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        String jdbcDriver = "org.postgresql.Driver";
        Class.forName(jdbcDriver);
        String jdbcUrl = "jdbc:postgresql:studentdb";
        String username = "dbuser";
        String password = "dbpassword";
        return (DriverManager.getConnection(jdbcUrl, username, password));


    }

    public static ResultSet selectStatement(String s) throws ClassNotFoundException, SQLException {
       try { con = DBAccess.getConnection();
        ps = con.prepareStatement(s);
        return ps.executeQuery();
        } catch (Exception e) {
            return null;
        } finally {
            con.close();
        }
    }

    public static boolean insertStatement(String s) throws SQLException {
        try {
            con = DBAccess.getConnection();
            ps = con.prepareStatement(s);
            ps.executeUpdate();
        } catch (Exception e) {
            return false;
        } finally {
            con.close();
        }
        return true;
    }
}
