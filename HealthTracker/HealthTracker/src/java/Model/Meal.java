package Model;

import Utilities.DBAccess;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;

public class Meal {

    public String type; // food or drink
    public String name;
    public int calories;

    public Meal() {

    }

    public Meal(String type, String name, int calories) {

        this.type = type;
        this.name = name;
        this.calories = calories;
    }

    public Meal(String type, String name) {

        this.type = type;
        this.name = name;
    }
    
    public Meal(ResultSet rs) throws SQLException {
        this.name = rs.getString("mealName");
        this.type = rs.getString("type");
        this.calories = rs.getInt("calories");
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    

    public boolean persist(String username) {
        try {
            Connection con = DBAccess.getConnection();

            Statement statement = con.createStatement();

            String sql = "INSERT INTO Meal VALUES('" + username + "', '"
                    + name + "', '" + type + "', " + calories + ")";

            statement.executeUpdate(sql);

            con.close();

        } catch (Exception e) {
            return false;
        }

        return true;
    }
    
    
        public boolean deleteMeal(String username, String mealName) {
        Connection con;
        try {
            con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("DELETE FROM meal "
                    + "WHERE username = ? AND mealName = ?;");
            ps.setString(1, username);
            ps.setString(2, mealName);
            int affected = ps.executeUpdate();
            if(affected < 1) {
                return false;
            } 
        } catch (Exception ex) {
            System.out.println("Error getting connection for delete goal.");
            Logger.getLogger(Goal.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

        PreparedStatement ps;
        try {
            ps = con.prepareStatement("DELETE FROM meal WHERE "
                    + "username = ? AND mealName = ?;");
            ps.setString(1, username);
            ps.setString(2, mealName);
            String temp = ps.toString();
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error in sql delete goal.");
            Logger.getLogger(Goal.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;

    }
        
            public ArrayList mealReturnAll(String username) throws ServletException {
        ArrayList mealList = new ArrayList();
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM meal WHERE username = ?;");
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Meal aMeal = new Meal(rs);
                mealList.add(aMeal);
            }
        } catch (Exception e) {
            throw new ServletException("Meal returnAll Problem", e);
        }
        return mealList;
    }

    @Override
    public String toString() {
        return "Meal [type=" + type + ", name=" + name + ", calories=" + calories
                + "]";
    }

}
