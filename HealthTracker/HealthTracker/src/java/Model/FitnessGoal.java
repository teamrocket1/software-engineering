package Model;

import Utilities.DBAccess;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class FitnessGoal extends Goal {
    private int targetHeartRate;
    private Exercise exercise;
        private String activityType;


    @Override
    public boolean isCompleted() {
        // TODO Auto-generated method stub
        return false;
    }
    
    public FitnessGoal(String owner, String gName){
        super(owner,gName);
    }
    
    public FitnessGoal(String gName, Date start, Date due, int tHR){
            super("",gName);
            startDate = start;
            dueDate = due;
            targetHeartRate = tHR;
            status = false;         
        }

    public int getTargetHeartRate() {
        return targetHeartRate;
    }

    public void setTargetHeartRate(int targetHeartRate) {
        this.targetHeartRate = targetHeartRate;
    }

    public Exercise getExercise() {
        return exercise;
    }

    public void setExercise(Exercise exercise) {
        this.exercise = exercise;
    }

    public int getIsGroup() {
        return isGroup;
    }

    public void setIsGroup(int isGroup) {
        this.isGroup = isGroup;
    }

    public boolean persist(String username) {
        try {
            java.util.Date utilDate = new java.util.Date();
            java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());    
                
            Connection con = DBAccess.getConnection();

            Statement statement2 = con.createStatement();
            PreparedStatement ps = con.prepareStatement("INSERT INTO Goal "
                    + "VALUES(?, ?, ?, ?, 'F', 0, 0, ?, 0 , 'NA', 0, ?)");
                    ps.setString(1, username);
                    ps.setString(2, goalName);
                    ps.setDate(3, startDate);
                    ps.setDate(4, dueDate);
                    ps.setInt(5, targetHeartRate);
                    ps.setInt(6, isGroup);
            
            String sql2 = "INSERT INTO History VALUES('" + username + "', 'Created Fitness Goal " + goalName + "', '" +
                            currentDate + "')";
            
            ps.executeUpdate();
            statement2.executeUpdate(sql2);

            con.close();

        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public double getActivityDistanceForUser(String username, String activity) {

        java.util.Date utilDate = new java.util.Date();
        java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

        try {

            String sql = "SELECT distance FROM activity WHERE"
                    + " username = '" + username + "' AND activityType = '"
                    + activity + "'";

            ResultSet rs = DBAccess.selectStatement(sql);

            double distance = 0;
            while (rs.next()) {
                distance = rs.getDouble(1);
            }
            return distance;

        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
    }

    public static ArrayList<FitnessGoal> getFitnessGoalsForUser(String username){

            ArrayList<FitnessGoal> fitnessGoals = new ArrayList<FitnessGoal>();
            
            java.util.Date utilDate = new java.util.Date();
            java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());
            
        try {
            Connection con = DBAccess.getConnection();
            Statement statement = con.createStatement();
            Statement statement2 = con.createStatement();
            
//            String sql = "SELECT * FROM Goal WHERE goalOwner = '" + username + "' AND "
//                    + "goalType = 'W' AND dueDate >= '" + currentDate + "'";
            
            String sql = "SELECT * FROM Goal WHERE goalOwner = '" + username + "' AND "
                    + "goalType = 'F'";
            
            
            ResultSet rs = statement.executeQuery(sql);


            while (rs.next()) {
                String goalName = rs.getString("goalName");
                Date startDate = rs.getDate("startDate");
                Date dueDate = rs.getDate("dueDate");
                int targetHeartRate = Integer.parseInt(rs.getString("targetHeartRate"));
                FitnessGoal goal = new FitnessGoal(goalName, startDate, dueDate, targetHeartRate);

                fitnessGoals.add(goal);
            }

            con.close();

        } catch (Exception e) {
            System.out.println(e);;
        }

        return fitnessGoals;
        }
    
     public static ArrayList<FitnessGoal> getFitnessGoalsForGroup(String username){

            ArrayList<FitnessGoal> fitnessGoals = new ArrayList<FitnessGoal>();
            
            java.util.Date utilDate = new java.util.Date();
            java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());
            
        try {
            Connection con = DBAccess.getConnection();
            Statement statement2 = con.createStatement();
            
            PreparedStatement ps = con.prepareStatement("SELECT * FROM Goal "
                    + "WHERE goalOwner = ? AND " + "goalType = 'F'");
            ps.setString(1, username);
            
            
            ResultSet rs = ps.executeQuery();


            while (rs.next()) {
                String goalName = rs.getString("goalName");
                Date startDate = rs.getDate("startDate");
                Date dueDate = rs.getDate("dueDate");
                int targetHeartRate = Integer.parseInt(rs.getString("targetHeartRate"));
                FitnessGoal goal = new FitnessGoal(goalName, startDate, dueDate, targetHeartRate);

                fitnessGoals.add(goal);
            }

            con.close();

        } catch (Exception e) {
            System.out.println(e);;
        }

        return fitnessGoals;
        }

    /**
     * This method is used to get fill a Goal object with its details from 
     * the database. 
     * 
     * It is used in displaying goal pages when we know only their
     * usernames and need to display more data.
     * 
     * @return true iff successful
     */
    @Override
    public boolean getGoalDetails() {
        
        boolean valid = false;
        try {
            Connection con = DBAccess.getConnection();
            Statement statement = con.createStatement();

            // I havent bothered using prepared statements here because theres no user input
            String sql = "SELECT * FROM Goal WHERE goalName = '" + goalName + "' AND goalOwner = '" + goalOwner
                    + "' AND goalType = 'F' AND isGroup = 0";

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                valid = true;
                startDate = rs.getDate("startDate");
                dueDate = rs.getDate("dueDate");
                targetHeartRate = Integer.parseInt(rs.getString("targetHeartRate"));
            }

            con.close();

        } catch (Exception e) {
            System.out.println(e);
            return false;
        }

        return valid;

    }
    
        /**
     * 
     *
     * @return true iff successful
     */
    public boolean getGoalDetailsForGroup() {

        boolean valid = false;
        try {
            Connection con = DBAccess.getConnection();
            Statement statement = con.createStatement();

            // I havent bothered using prepared statements here because theres no user input
            String sql = "SELECT * FROM Goal WHERE goalName = '" + goalName + "' AND goalOwner = '" + goalOwner
                    + "' AND goalType = 'F' AND isGroup = 1";

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                valid = true;
                startDate = rs.getDate("startDate");
                dueDate = rs.getDate("dueDate");
                targetHeartRate = Integer.parseInt(rs.getString("targetHeartRate"));
            }

            con.close();

        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
        
        return valid;
        
    }
    
        public boolean updateGoal(String username) {
        try {

            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("UPDATE Goal "
                    + "SET dueDate = ?, targetHeartRate = ?, "
                    + "activityType = ? WHERE goalOwner = ? AND "
                    + "goalName = ? AND isGroup = ?;");

            ps.setDate(1, dueDate);
            ps.setInt(2, targetHeartRate);
            ps.setString(3, activityType);
            ps.setString(4, username);
            ps.setString(5, goalName);
            ps.setInt(6, isGroup);
            ps.executeUpdate();

        } catch (Exception e) {
            return false;
        }

        return true;
    }
    
}