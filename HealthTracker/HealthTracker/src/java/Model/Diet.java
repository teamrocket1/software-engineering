package Model;

import Utilities.DBAccess;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

public class Diet implements Iterable {

    ArrayList<Meal> meals;

    public Diet(ArrayList<Meal> meals) {
        this.meals = meals;
    }

    public Diet() {
        meals = new ArrayList<Meal>();
    }

    public Meal get(int i) {
        return meals.get(i);
    }

    public boolean getMealsForUser(String username) {
        boolean valid = false;

        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement statement = con.prepareStatement("SELECT * FROM Meal WHERE username = 'DEFAULT' OR "
                    + "username = '" + username + "'");
            ResultSet rs = statement.executeQuery();

            /* 
             * Checks the database to produce a list of foods and drinks for the 
             * user to select from (including their customly added meals)
             */
            while (rs.next()) {
                valid = true;
                String type = rs.getString("type");
                String name = rs.getString("mealName");
                int cal = rs.getInt("calories");
                
                Meal meal = new Meal(type, name, cal);
                meals.add(meal);
            }

            con.close();

        } catch (Exception e) {
            return false;
        }

        return valid;
    }

    public void remove(int i) {
        meals.remove(i);
    }

    public int size() {
        return meals.size();
    }

    @Override
    public Iterator<Meal> iterator() {
        Iterator<Meal> it = meals.iterator();
        return it;
    }

}
