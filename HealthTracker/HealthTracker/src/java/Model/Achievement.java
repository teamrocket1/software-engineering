package Model;

import Utilities.DBAccess;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Achievement {

	private String username;
        private String goalName;
        private String achievmentDate;
        private String goalType;
        private int targetWeight;
        private int targetDistance;
        private int targetHeartRate;
        private int targetBMI;
        private String activityType;

    public Achievement(String username, String goalName, String achievmentDate, String goalType, int targetWeight, int targetDistance, int targetHeartRate, int targetBMI, String activityType) {
        this.username = username;
        this.goalName = goalName;
        this.achievmentDate = achievmentDate;
        this.goalType = goalType;
        this.targetWeight = targetWeight;
        this.targetDistance = targetDistance;
        this.targetHeartRate = targetHeartRate;
        this.targetBMI = targetBMI;
        this.activityType = activityType;
    }
    
    public Achievement(){
        
    }

    public static ArrayList<Achievement> getUsersAchievements(String username) throws ClassNotFoundException, SQLException {
             
        ArrayList<Achievement> achievments = new ArrayList<Achievement>();
        
        try{
        Connection con = DBAccess.getConnection();
        Statement statement = con.createStatement();
        
        String sql = "SELECT * FROM Achievments WHERE goalOwner = '" + username + "'";
        
        ResultSet rs = statement.executeQuery(sql);
        
        while(rs.next()){
            String aGoalName = rs.getString(2);
            String anAchievmentDate = rs.getString(3);
            String aGoalType = rs.getString(4);
            int aTargetWeight = rs.getInt(5);
            int aTargetDistance = rs.getInt(6); 
            int aTargetHeartRate = rs.getInt(7);
            int aTargetBMI = rs.getInt(8);
            String anActivityType = rs.getString(9);
            
            Achievement anAchievement = new Achievement(username, aGoalName, anAchievmentDate, aGoalType, aTargetWeight, aTargetDistance, aTargetHeartRate, aTargetBMI, anActivityType);
            
            achievments.add(anAchievement);
            
        }
        
        }
        catch(Exception e){
            return null;
        }
            return achievments;
        
        
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGoalName() {
        return goalName;
    }

    public void setGoalName(String goalName) {
        this.goalName = goalName;
    }

    public String getAchievmentDate() {
        return achievmentDate;
    }

    public void setAchievmentDate(String achievmentDate) {
        this.achievmentDate = achievmentDate;
    }

    public String getGoalType() {
        return goalType;
    }

    public void setGoalType(String goalType) {
        this.goalType = goalType;
    }

    public int getTargetWeight() {
        return targetWeight;
    }

    public void setTargetWeight(int targetWeight) {
        this.targetWeight = targetWeight;
    }

    public int getTargetDistance() {
        return targetDistance;
    }

    public void setTargetDistance(int targetDistance) {
        this.targetDistance = targetDistance;
    }

    public int getTargetHeartRate() {
        return targetHeartRate;
    }

    public void setTargetHeartRate(int targetHeartRate) {
        this.targetHeartRate = targetHeartRate;
    }

    public int getTargetBMI() {
        return targetBMI;
    }

    public void setTargetBMI(int targetBMI) {
        this.targetBMI = targetBMI;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

	
	
        
	
	
}
