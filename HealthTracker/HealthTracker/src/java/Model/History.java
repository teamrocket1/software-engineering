/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;

import Utilities.DBAccess;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Odie
 */
public class History {
    
    private String username;
    private String text;
    private Date date;
    
    
    History(){
        
    }
    
    
    History(String u, String t, Date d){
        username = u;
        text = t;
        date = d;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    public static ArrayList<History> getUserHistory(String username) {

        ArrayList<History> list = new ArrayList<History>();

        try {

            Connection con = DBAccess.getConnection();

            String sql = "SELECT * FROM History WHERE username = '" + username + "'";

            Statement statement = con.createStatement();

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                String text = rs.getString("text");
                Date date = rs.getDate("date");

                History h = new History(username, text, date);
                list.add(h);
            }



        } catch (Exception ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;


    }
    
    
}
