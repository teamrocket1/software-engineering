package Model;

import Utilities.DBAccess;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Membership {

    public enum Role {

        U, A
    }; // R for regular, A for Admin.

    private String username;
    private String groupName;
    private Role role;
    private int permissionsLevel;

    public Membership(String username) {
        // Interacts with db to get users membership details
    }

    public Membership(String user, String group, Role r, int p) {
        username = user;
        groupName = group;
        role = r;
        permissionsLevel = p;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public int getPermissionsLevel() {
        return permissionsLevel;
    }

    public void setPermissionsLevel(int permissionsLevel) {
        this.permissionsLevel = permissionsLevel;
    }

    

    public boolean persist() {
        try {
            Connection con = DBAccess.getConnection();

            Statement statement = con.createStatement();

            String sql = "INSERT INTO Membership VALUES('" + username + "', '"
                    + groupName + "', '" + role + "', " + permissionsLevel + ")";

            statement.executeUpdate(sql);

            con.close();

        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public static ArrayList<Membership> getUsersMemberships(String username) {
        ArrayList<Membership> memberships = new ArrayList<Membership>();

        try {
            Connection con = DBAccess.getConnection();
            Statement statement = con.createStatement();
            String sql = "SELECT * FROM Membership WHERE username = '" + username + "'";

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                String groupName = rs.getString("groupName");
                String r = rs.getString("role");

                Role role = Role.U;
                if (r.equalsIgnoreCase("A")) {
                    role = Role.A;
                }

                int p = Integer.parseInt(rs.getString("permissionsLevel"));

                Membership m = new Membership(username, groupName, role, p);
                memberships.add(m);
            }

            con.close();

        } catch (Exception e) {
            System.out.println(e);;
        }

        return memberships;

    }
    
    public static ArrayList<Membership> getGroupsMemberships(String groupName) {
        ArrayList<Membership> memberships = new ArrayList<Membership>();

        try {
            Connection con = DBAccess.getConnection();
            Statement statement = con.createStatement();
            String sql = "SELECT * FROM Membership WHERE groupName = '" + groupName + "'";

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                String username = rs.getString("username");
                String r = rs.getString("role");

                Role role = Role.U;
                if (r.equalsIgnoreCase("A")) {
                    role = Role.A;
                }

                int p = Integer.parseInt(rs.getString("permissionsLevel"));

                Membership m = new Membership(username, groupName, role, p);
                memberships.add(m);
            }

            con.close();

        } catch (Exception e) {
            System.out.println(e);;
        }

        return memberships;

    }
    
    public static Membership getUserMembershipForGroup(String username, String  groupName) {
        
        Membership mem = new Membership(username);
        
        

        try {
            Connection con = DBAccess.getConnection();
            Statement statement = con.createStatement();
            String sql = "SELECT * FROM Membership WHERE username = '" + username + "' AND groupName = '" + groupName + "'";

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                String gName = rs.getString("groupName");
                String r = rs.getString("role");

                Role role = Role.U;
                if (r.equalsIgnoreCase("A")) {
                    role = Role.A;
                }

                int p = Integer.parseInt(rs.getString("permissionsLevel"));

                mem = new Membership(username,  gName, role, p);
            }

            con.close();

        } catch (Exception e) {
            System.out.println(e);;
        }

        return mem;
        
    }
}
