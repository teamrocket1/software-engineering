package Model;

import Utilities.DBAccess;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class Goal {

    protected String goalOwner;
    protected String goalName;
    protected Date startDate;
    protected Date dueDate;
    protected boolean status;
    protected int isGroup = 0;
    //protected Diet diet;

    abstract boolean isCompleted();

    public Goal(String owner, String gName){
        goalOwner = owner;
        goalName = gName;
    }
    
    public String getGoalName() {
        return goalName;
    }

    public void setGoalName(String goalName) {
        this.goalName = goalName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
    /**
     * This method is used to get fill a Goal object with its details from 
     * the database. 
     * 
     * It is used in displaying goal pages when we know only their
     * usernames and need to display more data.
     * 
     * @return true iff succesful
     */
    abstract boolean getGoalDetails();

    public static boolean deleteGoal(String username, String goalName,
            int isGroup) {
        Connection con;
        try {
            con = DBAccess.getConnection();
        } catch (Exception ex) {
            System.out.println("Error getting connection for delete goal.");
            Logger.getLogger(Goal.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

        PreparedStatement ps;
        try {
            ps = con.prepareStatement("DELETE FROM goal WHERE "
                    + "goalOwner = ? AND goalName = ? AND isGroup = ?;");
            ps.setString(1, username);
            ps.setString(2, goalName);
            ps.setInt(3, isGroup);
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error in sql delete goal.");
            Logger.getLogger(Goal.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;

    }
}
