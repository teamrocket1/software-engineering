/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Utilities.DBAccess;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Takomborerwa
 */
public class Run extends Activity {
 
    private int heartRate;
    
     public Run (int distance, int duration, String username, String sex, float weight, int age, int heartRate){
        this.distance = distance;
        this.duration = duration;
        this.username = username;
        this.sex = sex;
        this.weight = weight;
        this.age = age;
        this.heartRate = heartRate;
    } 

     
     public Run(int distance, int duration, String username, Date d) {
        this.distance = distance;
        this.duration = duration;
        this.username = username;
        this.date = d;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

     
     


    /**
     * A method to calculate the amount of calories burnt during a Run.
     * 
     * @return Amount of calories burnt during a Run rounded 
     * down to a whole number.
     */
    @Override
    public int getCaloriesBurnt() {

        // Currently using the same formula as that of a Walk.
        // If we're being technical, a run cant use the same formula as a walk
        // because this imploes that the intensity of a workout has no effect
        // on the outcome. Essentially this is saying a 5 min walk burns the same
        // calories as a 5 min run.
        // Might look into changing that if we can.
        
        double calories = 0;

        if (sex.equalsIgnoreCase("m")) {
            double a = (double) (age * 0.20175);
            double w = (double) (weight * 0.09036);
            double h = (double) (heartRate * 0.6309);
            calories = ((a + w + h) - 55.0969) + (duration / 4.184);    
        }

        else if (sex.equalsIgnoreCase("f")) {
            double a = (double) (age * 0.074);
            double w = (double) (weight * 0.05741);
            double h = (double) (heartRate * 0.4472);
            calories = ((a + w + h) - 55.0969) + (duration / 20.4022);
        }
        
        return (int) calories;

    }
    
    /**
     * Stores the details of a Run object in the database; Also checks to see 
     * if a distance goal has been completed and takes appropriate action.
     * 
     * @return true iff successful.
     */
    public boolean persist(){
        try {
            Connection con = DBAccess.getConnection();

            Statement statement = con.createStatement();
            Statement statement2 = con.createStatement();
            Statement statement3 = con.createStatement();
            Statement statement4 = con.createStatement();
            
            java.util.Date utilDate = new java.util.Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

           
            
            String sql = "INSERT INTO Activity VALUES('" + username + "', "
                    + distance + ", " + duration + ", " + getCaloriesBurnt()
                    + ", '" + sqlDate + "', 'R' )";
            
            String moreSQL = "INSERT INTO History VALUES('" + username + "', 'Took a " + distance + "km run', '"
                    + sqlDate + "')";
            
            DBAccess.insertStatement(moreSQL);

            statement.executeUpdate(sql);
            
            // Calling sql function to update goal status
            String sqlAgain = "SELECT * FROM updateAchievedDistance('" + username + "'," + distance +", 'R')";
            statement4.executeQuery(sqlAgain);
            
            ArrayList<DistanceGoal> distanceGoals = DistanceGoal.getDistanceGoalsForUser(username);
            ArrayList<CustomGoal> customGoals = CustomGoal.getCustomGoalsForUser(username);
            
            for(DistanceGoal g : distanceGoals){
                
                /* Check if goal achieved*/
                if(g.getAchievedDistance() >= g.getTargetDistance()){
                    
                    /* If achieved delete from db and do some other stuff */
                    /* Other stuff(notifications) to be added later*/
                    g.status = true;
                    
                    // Insert into achievments
                    sql = "INSERT INTO Achievments VALUES('" + username + "', '" + g.getGoalName() + 
                            "', '" + sqlDate + "', 'D'," + 0 + "," + g.getTargetDistance() + "," +
                            0 + "," + 0 + ",'" + g.getActivityType() + "'"
                            + ")";
                    
                    String sql2 = "DELETE FROM Goal WHERE goalName = '" + g.getGoalName() + "'" +
                            "AND goalOwner = '" + username + "'";
                    
                    String moreSQLAgain = "INSERT INTO History VALUES('" + username + "', 'Completed Goal " + g.getGoalName() + "', '"
                    + sqlDate + "')";
                    
                    DBAccess.insertStatement(moreSQLAgain);
                    
                    statement2.executeUpdate(sql);
                    
                    statement3.executeUpdate(sql2);
                    
                    
                }
            }

            con.close();

        } catch (Exception e) {
            return false;
        }

        return true;
    }
    
    public static ArrayList<Run> getRunsForUser(String username) {

        ArrayList<Run> runs = new ArrayList<Run>();

        java.util.Date utilDate = new java.util.Date();
        java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

        try {
            Connection con = DBAccess.getConnection();
            Statement statement = con.createStatement();

     // I havent bothered using prepared statements here because theres no user input
            
            // Gets walks from the start of the year.
            String sql = "SELECT * FROM Activity WHERE username = '" + username + "' AND "
                    + "activityType = 'R' AND date >= '2014/01/01'";

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                int distance = Integer.parseInt(rs.getString("distance"));
                int duration = Integer.parseInt(rs.getString("duration"));
                Date date = rs.getDate("date");
                Run r = new Run(distance, duration, username, date);
                runs.add(r);
            }

            con.close();

        } catch (Exception e) {
            System.out.println(e);
        }

        return runs;
    }
     
}
