/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Model;

import Utilities.DBAccess;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Odie
 */
public class Comment {
    
    private String username;
    private String text;
    private String groupName;
    private Date date;
    
    public Comment(String u, String t, String g){
        username = u;
        text = t;
        groupName = g;
        
        java.util.Date utilDate = new java.util.Date();
        java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());
        date = currentDate;
    }
    
    public Comment(String u, String t, String g, Date d){
        username = u;
        text = t;
        groupName = g;
        date = d;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    
    public boolean persist(){
        try {   
                
            Connection con = DBAccess.getConnection();

            Statement statement = con.createStatement();
            Statement statement2 = con.createStatement();

            String sql = "INSERT INTO GroupComments VALUES('" + username + "', '"
                    + groupName + "', '" + text + "', '" + date + "')";
            
            // Not sure if comments should show up in a users history
            /*
            String sql2 = "INSERT INTO History VALUES('" + username + "', 'Created Weight Goal " + goalName + "', '" +
                            currentDate + "')";
            */
            
            statement.executeUpdate(sql);

            con.close();

        } catch (Exception e) {
            return false;
        }

        return true;
    }
    
    
    public static ArrayList<Comment> getGroupComments(String groupName){
        ArrayList<Comment> comments = new ArrayList<Comment>();
        
        try {
            Connection con = DBAccess.getConnection();
            Statement statement = con.createStatement();
            String sql = "SELECT * FROM GroupComments WHERE groupName = '" + groupName + "'";

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                String username = rs.getString("username");
                String text = rs.getString("comment");
                Date date = rs.getDate("date");

                Comment c = new Comment(username, text, groupName, date);
                comments.add(c);
            }

            con.close();

        } catch (Exception e) {
            System.out.println(e);;
        }

        return comments;
        
    }
    
    
    
}
