/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Model.DistanceGoal;
import Model.FitnessGoal;
import Model.History;
import Model.User;
import Model.WeightGoal;
import Utilities.DBAccess;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Used to display dedicated goal pages on search or on click.
 *
 * @author Odie
 */
public class ProfilePageController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();
        
        User user= (User) session.getAttribute("aUser");
        
        // Get the details to be displayed on the persons profile page
        String ownerUsername = (String) session.getAttribute("usernameFromLogin");
        String ownerUsernameFromNavURL = request.getParameter("pageOwner");
        User pageOwner = new User(ownerUsername);
        pageOwner.getUserDetails();
        session.setAttribute("pageOwner", pageOwner);

        // get the persons history
       // ArrayList<History> historyList = pageOwner.getUserHistory();
        //session.setAttribute("historyList", historyList);

        if(request.getParameter("pageOwner") != null){
            if(request.getParameter("pageOwner").equalsIgnoreCase(user.getUsername())){
                
                String username = user.getUsername();
            
            ArrayList<WeightGoal> weightGoals = new ArrayList<WeightGoal>();
            ArrayList<DistanceGoal> distanceGoals = new ArrayList<DistanceGoal>();
            ArrayList<FitnessGoal> fitnessGoals = new ArrayList<FitnessGoal>();
            
            try {

                Connection con = DBAccess.getConnection();
                Statement stmt = con.createStatement();

                String sql = "SELECT * FROM Goal WHERE goalOwner = '" + username + "' AND isGroup = 0";
                ResultSet rs = stmt.executeQuery(sql);

                while (rs.next()) {

                    if (rs.getString("goalType").equalsIgnoreCase("W")) {
                        String goalName = rs.getString("goalName");
                        Date startDate = rs.getDate("startDate");
                        Date dueDate = rs.getDate("dueDate");
                        int targetWeight = Integer.parseInt(rs.getString("targetWeight"));
                        WeightGoal goal = new WeightGoal(goalName, startDate, dueDate, targetWeight);

                        weightGoals.add(goal);
                    } 
                    
                    else if (rs.getString("goalType").equalsIgnoreCase("D")) {
                        String goalName = rs.getString("goalName");
                        Date startDate = rs.getDate("startDate");
                        Date dueDate = rs.getDate("dueDate");
                        int targetDistance = Integer.parseInt(rs.getString("targetDistance"));
                        String activityType = rs.getString("activityType");
                        DistanceGoal goal = new DistanceGoal(targetDistance, startDate, dueDate, goalName, activityType);
                        goal.setAchievedDistance(rs.getInt("achievedDistance"));
                        distanceGoals.add(goal);
                    }
                    
                    else if (rs.getString("goalType").equalsIgnoreCase("F")) {
                        String goalName = rs.getString("goalName");
                        Date startDate = rs.getDate("startDate");
                        Date dueDate = rs.getDate("dueDate");
                        int targetHeartRate = Integer.parseInt(rs.getString("targetHeartRate"));
                        FitnessGoal goal = new FitnessGoal(goalName, startDate, dueDate, targetHeartRate);
                        fitnessGoals.add(goal);
                    }

                }
                
                session.setAttribute("weightGoals", weightGoals);
                session.setAttribute("distanceGoals", distanceGoals);
                session.setAttribute("fitnessGoals", fitnessGoals);
                
                Statement stmt2 = con.createStatement();

                String sql2 = "SELECT SUM(distance) FROM Activity WHERE activityType = 'R' AND username = '" + username + "'";
                ResultSet rs2 = stmt.executeQuery(sql2);
                int totalRun = 0;
                while(rs2.next()){
                    totalRun = rs2.getInt(1);
                }
                
                
                session.setAttribute("totalRun", totalRun);
                request.getRequestDispatcher("View/UserPage.jsp").forward(request, response);

            } catch (Exception e) {
                    String errorMessage = "Profile Page error, sorry please try again.";
                    session.setAttribute("errorMessage", errorMessage);
                    request.getRequestDispatcher("View/Error.jsp").forward(request, response);
            }
                
            }
            
            
        }
        
        /* Get more stuff if u want and add to session object here */
       // if (request.getParameter("type").equalsIgnoreCase("showPage")) {

            ArrayList<WeightGoal> weightGoals = new ArrayList<WeightGoal>();
            ArrayList<DistanceGoal> distanceGoals = new ArrayList<DistanceGoal>();
            ArrayList<FitnessGoal> fitnessGoals = new ArrayList<FitnessGoal>();
            
            try {

                Connection con = DBAccess.getConnection();
                Statement stmt = con.createStatement();

                String sql = "SELECT * FROM Goal WHERE goalOwner = '" + ownerUsername + "' AND isGroup = 0";
                ResultSet rs = stmt.executeQuery(sql);

                while (rs.next()) {

                    if (rs.getString("goalType").equalsIgnoreCase("W")) {
                        String goalName = rs.getString("goalName");
                        Date startDate = rs.getDate("startDate");
                        Date dueDate = rs.getDate("dueDate");
                        int targetWeight = Integer.parseInt(rs.getString("targetWeight"));
                        WeightGoal goal = new WeightGoal(goalName, startDate, dueDate, targetWeight);

                        weightGoals.add(goal);
                    } 
                    
                    else if (rs.getString("goalType").equalsIgnoreCase("D")) {
                        String goalName = rs.getString("goalName");
                        Date startDate = rs.getDate("startDate");
                        Date dueDate = rs.getDate("dueDate");
                        int targetDistance = Integer.parseInt(rs.getString("targetDistance"));
                        String activityType = rs.getString("activityType");
                        DistanceGoal goal = new DistanceGoal(targetDistance, startDate, dueDate, goalName, activityType);
                        goal.setAchievedDistance(rs.getInt("achievedDistance"));
                        distanceGoals.add(goal);
                    }
                    
                    else if (rs.getString("goalType").equalsIgnoreCase("F")) {
                        String goalName = rs.getString("goalName");
                        Date startDate = rs.getDate("startDate");
                        Date dueDate = rs.getDate("dueDate");
                        int targetHeartRate = Integer.parseInt(rs.getString("targetHeartRate"));
                        FitnessGoal goal = new FitnessGoal(goalName, startDate, dueDate, targetHeartRate);
                        fitnessGoals.add(goal);
                    }

                }
                
                session.setAttribute("weightGoals", weightGoals);
                session.setAttribute("distanceGoals", distanceGoals);
                session.setAttribute("fitnessGoals", fitnessGoals);
                
                Statement stmt2 = con.createStatement();

                String sql2 = "SELECT SUM(distance) FROM Activity WHERE activityType = 'R' AND username = '" + pageOwner.getUsername() + "'";
                ResultSet rs2 = stmt.executeQuery(sql2);
                int totalRun = 0;
                while(rs2.next()){
                    totalRun = rs2.getInt(1);
                }
                
                
                session.setAttribute("totalRun", totalRun);
                request.getRequestDispatcher("View/UserPage.jsp").forward(request, response);

            } catch (Exception e) {
                    String errorMessage = "Profile Page error, sorry please try again.";
                    session.setAttribute("errorMessage", errorMessage);
                    request.getRequestDispatcher("View/Error.jsp").forward(request, response);
            }

        //}

       // request.getRequestDispatcher("View/ProfilePage.jsp").forward(request, response);

        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ProfilePageController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ProfilePageController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
