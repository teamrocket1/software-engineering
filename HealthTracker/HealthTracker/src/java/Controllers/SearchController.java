/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controllers;

import Model.User;
import Utilities.DBAccess;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Odie
 */
public class SearchController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("aUser");
        
        if(request.getParameter("placeToSearch").equalsIgnoreCase("google")){
            String url = "http://www.google.com/search?q=" + request.getParameter("query");
            response.sendRedirect(url);
        }
        
        else if(request.getParameter("placeToSearch").equalsIgnoreCase("site")){
            String query = request.getParameter("query");
            
            try{
                Connection con = DBAccess.getConnection();
                
                // Try to get users
                PreparedStatement ps = con.prepareStatement("SELECT username FROM User_Data WHERE username ILIKE ?");
                ps.setString(1, query);
                ResultSet rs = ps.executeQuery();
                
                // just get the users' names to display in results. Theyll be able to click on it
                // to show more stuff.
                ArrayList<String> usernames = new ArrayList<String>();         
                while(rs.next()){
                    usernames.add(rs.getString("username"));
                }
                
                // Try to get Goal
                PreparedStatement ps2 = con.prepareStatement("SELECT goalname FROM Goal WHERE goalName ILIKE ? AND goalOwner = '" + user.getUsername() + "'");
                ps2.setString(1, query);
                ResultSet rs2 = ps2.executeQuery();
                
                // just get the users' goal names to display in results. Theyll be able to click on it
                // to show more stuff.
                ArrayList<String> goalnames = new ArrayList<String>();         
                while(rs2.next()){
                    goalnames.add(rs2.getString("goalName"));
                }
                
                // try to get glossary terms
                PreparedStatement ps3 = con.prepareStatement("SELECT * FROM Glossary WHERE definitionName ILIKE ?");
                ps3.setString(1, query);
                ResultSet rs3 = ps3.executeQuery();
                
                // just get the users' goal names to display in results. Theyll be able to click on it
                // to show more stuff.
                ArrayList<String> glossaryNames = new ArrayList<String>();
                ArrayList<String> glossaryDefinitions = new ArrayList<String>();
                while(rs3.next()){
                    glossaryNames.add(rs3.getString("definitionName"));
                    glossaryDefinitions.add(rs3.getString("definition"));
                }
                
                session.setAttribute("usernames", usernames);
                session.setAttribute("goalnames", goalnames);
                session.setAttribute("glossaryNames", glossaryNames);
                session.setAttribute("glossaryDefinitions", glossaryDefinitions);
                request.getRequestDispatcher("View/SearchResults.jsp").forward(request, response);
   
            }
            catch(Exception e){
                String errorMessage = "Search error please try again.";
                    session.setAttribute("errorMessage", errorMessage);
                    request.getRequestDispatcher("View/Error.jsp").forward(request, response);
            }
            
           
        }
        
        
        
        
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SearchController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SearchController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
