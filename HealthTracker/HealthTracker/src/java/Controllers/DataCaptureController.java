/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Model.Bike;
import Model.Diet;
import Model.Meal;
import Model.Run;
import Model.User;
import Model.Walk;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Odie
 */
public class DataCaptureController extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String msg = "";

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("aUser");


        /* Called if user clicks on link to update diet/fitness*/
        if (request.getParameter("type").equalsIgnoreCase("clicked")) {
            Diet diet = new Diet();
            diet.getMealsForUser(user.getUsername());
            session.setAttribute("meals", diet);
            request.getRequestDispatcher("View/UpdateInfo.jsp").forward(request, response);
        } /* If user selected Fitness then walk */ else if (request.getParameter("type1").equalsIgnoreCase("set")) {
            int duration = Integer.parseInt(request.getParameter("walkDuration"));
            int distance = Integer.parseInt(request.getParameter("walkDistance"));
            String unit = request.getParameter("walkDistanceUnit");

            // db stores distance in km. If user enters miles convert.
            if (unit.equalsIgnoreCase("mi")) {
                double d = distance;
                distance = (int) (d * 1.6);
            }

            Walk walk = new Walk(distance, duration, user.getUsername(), user.getSex(),
                    user.getWeight(), user.getAge(), user.getHeartRate());

            if (walk.persist()) {
                msg = "successful walk";
                // redirect to appropriate feedback page here
                request.getRequestDispatcher("View/UserPage.jsp").forward(request, response);
            } else {
                String errorMessage = "Unsucceful data entry, make sure they are valid.";
                session.setAttribute("errorMessage", errorMessage);
                request.getRequestDispatcher("View/Error.jsp").forward(request, response);
            }
        } /* If user selected Fitness then run */ else if (request.getParameter("type2").equalsIgnoreCase("set")) {

            int duration = Integer.parseInt(request.getParameter("runDuration"));
            int distance = Integer.parseInt(request.getParameter("runDistance"));
            String unit = request.getParameter("runDistanceUnit");

            // db stores distance in km. If user enters miles convert.
            if (unit.equalsIgnoreCase("mi")) {
                double d = distance;
                distance = (int) (d * 1.6);
            }

            Run run = new Run(distance, duration, user.getUsername(), user.getSex(),
                    user.getWeight(), user.getAge(), user.getHeartRate());

            if (run.persist()) {
                msg = "successful walk";
                // redirect to appropriate feedback page here
                request.getRequestDispatcher("View/UserPage.jsp").forward(request, response);
            } else {
                String errorMessage = "Unsucceful data entry, make sure they are valid.";
                session.setAttribute("errorMessage", errorMessage);
                request.getRequestDispatcher("View/Error.jsp").forward(request, response);
            }

        } /* If user selected Fitness then bike */ else if (request.getParameter("type3").equalsIgnoreCase("set")) {

            int duration = Integer.parseInt(request.getParameter("bikeDuration"));
            int distance = Integer.parseInt(request.getParameter("bikeDistance"));
            String unit = request.getParameter("bikeDistanceUnit");

            // db stores distance in km. If user enters miles convert.
            if (unit.equalsIgnoreCase("mi")) {
                double d = distance;
                distance = (int) (d * 1.6);
            }

            Bike bike = new Bike(distance, duration, user.getUsername(), user.getSex(),
                    user.getWeight(), user.getAge(), user.getHeartRate());

            if (bike.persist()) {
                msg = "successful walk";
                // redirect to appropriate feedback page here
                request.getRequestDispatcher("View/UserPage.jsp").forward(request, response);
            } else {
                String errorMessage = "Unsucceful data entry, make sure they are valid.";
                session.setAttribute("errorMessage", errorMessage);
                request.getRequestDispatcher("View/Error.jsp").forward(request, response);
            }

        } /* If user selects diet */ /* If user selects from dropdown list */ else if (request.getParameter("type4").equalsIgnoreCase("set")) {
        } /* If user enters custom food/drink */ else if (request.getParameter("type5").equalsIgnoreCase("set")) {
            String food = request.getParameter("customFood");
            int fCal = Integer.parseInt(request.getParameter("foodCalories"));
            String drink = request.getParameter("customDrink");
            int dCal = Integer.parseInt(request.getParameter("drinkCalories"));

            if (!food.equalsIgnoreCase("")) {
                String type = "food";
                Meal meal = new Meal(type, food, fCal);
                meal.persist(user.getUsername());
            }
            if (!drink.equalsIgnoreCase("")) {
                String type = "drink";
                Meal meal = new Meal(type, drink, dCal);
                meal.persist(user.getUsername());
            }

            // redirect to appropriate feedback page here
            request.getRequestDispatcher("View/UserPage.jsp").forward(request, response);
        }








        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DataCaptureController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DataCaptureController at " + request.getContextPath() + msg + "</h1>");
            out.println("<h1>" + user.getFirstName() + " " + user.getLastName());
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
