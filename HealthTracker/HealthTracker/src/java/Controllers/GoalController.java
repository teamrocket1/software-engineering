/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Model.CustomGoal;
import Model.DistanceGoal;
import Model.User;
import Model.WeightGoal;
import Model.FitnessGoal;
import Model.Goal;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Odie
 */
public class GoalController extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("aUser");
        String msg = "";


        /* Called if user clicks on Goals link*/
        if (request.getParameter("type").equalsIgnoreCase("clicked")) {

            /* Gets users weight goal information to be displayed on the page */
            ArrayList<WeightGoal> weightGoals = WeightGoal.getWeightGoalsForUser(user.getUsername());
            session.setAttribute("weightGoals", weightGoals);

            /* Gets users distance goal information to be displayed on the page */
            ArrayList<DistanceGoal> distanceGoals = DistanceGoal.getDistanceGoalsForUser(user.getUsername());
            session.setAttribute("distanceGoals", distanceGoals);

            /* Gets users fitness goal information to be displayed on the page */
            ArrayList<FitnessGoal> fitnessGoals = FitnessGoal.getFitnessGoalsForUser(user.getUsername());
            session.setAttribute("fitnessGoals", fitnessGoals);

            /* Gets users fitness goal information to be displayed on the page */
            ArrayList<CustomGoal> customGoals = CustomGoal.getCustomGoalsForUser(user.getUsername());
            session.setAttribute("customGoals", customGoals);

            request.getRequestDispatcher("View/Goals.jsp").forward(request, response);

        } else if (request.getParameter("type").equalsIgnoreCase("delete")) {
            String[] goalNames = request.getParameterValues("delete");
            String username = user.getUsername();
            for (int i = 0; i < goalNames.length; i++) {
                Goal.deleteGoal(username, goalNames[i], 0);
            }
            request.getRequestDispatcher("/GoalController?type=clicked").forward(request, response);
        } else if (request.getParameter("type").equalsIgnoreCase("modifyWeightGoal")) {
            String goalName = request.getParameter("goalName");
            int targetWeight = Integer.parseInt(request.getParameter("targetWeight"));
            int day = Integer.parseInt(request.getParameter("day_weight"));
            int month = Integer.parseInt(request.getParameter("month_weight"));
            int year = Integer.parseInt(request.getParameter("year_weight"));
            Date dueDate = new Date(year - 1900, (month - 1), day);
            Date startDate = Date.valueOf(request.getParameter("startDate"));

            WeightGoal goal = new WeightGoal(goalName, startDate, dueDate, targetWeight);
            goal.setIsGroup(0);
            if (goal.updateGoal(user.getUsername())) {
                request.getRequestDispatcher("/GoalController?type=clicked").forward(request, response);
            } else {
                // redirect to appropriate error page here
                String errorMessage = "Weight goals fields error, make sure they are valid.";
                session.setAttribute("errorMessage", errorMessage);
                request.getRequestDispatcher("View/Error.jsp").forward(request, response);
            }
        } /* If user creates weight goal */ else if (request.getParameter("type1").equalsIgnoreCase("set")) {
            String goalName = request.getParameter("goalName");
            int targetWeight = Integer.parseInt(request.getParameter("targetWeight"));
            int day = Integer.parseInt(request.getParameter("day_weight"));
            int month = Integer.parseInt(request.getParameter("month_weight"));
            int year = Integer.parseInt(request.getParameter("year_weight"));
            Date dueDate = new Date(year - 1900, (month - 1), day);

            java.util.Date utilDate = new java.util.Date();
            java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

            WeightGoal goal = new WeightGoal(goalName, currentDate, dueDate, targetWeight);

            if (goal.persist(user.getUsername())) {
                // redirect to appropriate feedback page here
                request.getRequestDispatcher("View/UserPage.jsp").forward(request, response);
            } else {
                String errorMessage = "Weight goals fields error, make sure they are valid.";
                session.setAttribute("errorMessage", errorMessage);
                request.getRequestDispatcher("View/Error.jsp").forward(request, response);
            }



        } /* If user creates fitness goal */ else if (request.getParameter("type2").equalsIgnoreCase("set")) {
            String goalName = request.getParameter("goalName_fitness");
            int targetHeartRate = Integer.parseInt(request.getParameter("targetHeartRate"));
            int day = Integer.parseInt(request.getParameter("day_fitness"));
            int month = Integer.parseInt(request.getParameter("month_fitness"));
            int year = Integer.parseInt(request.getParameter("year_fitness"));
            Date dueDate = new Date(year - 1900, (month - 1), day);

            java.util.Date utilDate = new java.util.Date();
            java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

            FitnessGoal goal = new FitnessGoal(goalName, currentDate, dueDate, targetHeartRate);

            if (goal.persist(user.getUsername())) {
                // redirect to appropriate feedback page here
                request.getRequestDispatcher("View/UserPage.jsp").forward(request, response);
            } else {
                // redirect to appropriate error page here
                String errorMessage = "Fitness goals fields error, make sure they are valid.";
                session.setAttribute("errorMessage", errorMessage);
                request.getRequestDispatcher("View/Error.jsp").forward(request, response);
            }

        } /* If user creates custom goal */ else if (request.getParameter("type3").equalsIgnoreCase("set")) {
            String goalName = request.getParameter("goalName_custom");
            int targetWeight = Integer.parseInt(request.getParameter("targetWeight_custom"));
            int targetHeartRate = Integer.parseInt(request.getParameter("targetHeartRate_custom"));
            int targetDistance = Integer.parseInt(request.getParameter("targetDistance_custom"));
            int day = Integer.parseInt(request.getParameter("day_custom"));
            int month = Integer.parseInt(request.getParameter("month_custom"));
            int year = Integer.parseInt(request.getParameter("year_custom"));
            Date dueDate = new Date(year - 1900, (month - 1), day);
            String activityType = request.getParameter("activityType_custom");
            String unit = request.getParameter("customDistanceUnit");

            int targetBMI = 0; // not supported for now

            // db stores distance in km. If user enters miles convert.
            if (unit.equalsIgnoreCase("mi")) {
                double d = targetDistance;
                targetDistance = (int) (d * 1.6);
            }

            java.util.Date utilDate = new java.util.Date();
            java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

            CustomGoal goal = new CustomGoal(goalName, currentDate, dueDate, targetWeight, targetDistance, targetHeartRate, targetBMI, activityType);
            if (goal.persist(user.getUsername())) {
                // redirect to appropriate feedback page here
                request.getRequestDispatcher("View/UserPage.jsp").forward(request, response);
            } else {
                // redirect to appropriate error page here
                String errorMessage = "Custom goals fields error, make sure they are valid.";
                session.setAttribute("errorMessage", errorMessage);
                request.getRequestDispatcher("View/Error.jsp").forward(request, response);
            }


        } /* If user creates distance goal */ else if (request.getParameter("type4").equalsIgnoreCase("set")) {
            int targetDistance = Integer.parseInt(request.getParameter("targetDistance_distance"));
            int day = Integer.parseInt(request.getParameter("day_distance"));
            int month = Integer.parseInt(request.getParameter("month_distance"));
            int year = Integer.parseInt(request.getParameter("year_distance"));
            Date dueDate = new Date(year - 1900, (month - 1), day);
            String goalName = request.getParameter("goalName_distance");
            String activityType = request.getParameter("activityType");

            String unit = request.getParameter("distanceDistanceUnit");

            // db stores distance in km. If user enters miles convert.
            if (unit.equalsIgnoreCase("mi")) {
                double d = targetDistance;
                targetDistance = (int) (d * 1.6);
            }

            java.util.Date utilDate = new java.util.Date();
            java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

            DistanceGoal goal = new DistanceGoal(targetDistance, currentDate, dueDate, goalName, activityType);
            goal.persist(user.getUsername());

            if (goal.persist(user.getUsername())) {
                // redirect to appropriate feedback page here
                request.getRequestDispatcher("View/UserPage.jsp").forward(request, response);
            } else {
                String errorMessage = "Distance goals fields error, make sure they are valid.";
                session.setAttribute("errorMessage", errorMessage);
                request.getRequestDispatcher("View/Error.jsp").forward(request, response);
            }


        }

        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet GoalController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet GoalController at " + request.getContextPath() + msg + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }

    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
