/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Model.Bike;
import Model.History;
import Model.Meal;
import Model.Run;
import Model.User;
import Model.Walk;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Odie
 */
public class HistoryController extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("aUser");
        String msg = "";
        
        
        if (request.getParameter("type").equalsIgnoreCase("history")) { //Getting data for the history page
            ArrayList<History> historyList = user.getUserHistory();
            session.setAttribute("historyList", historyList);
            request.getRequestDispatcher("View/History.jsp").forward(request, response);
        } 
        
        
        
        else if(request.getParameter("type").equalsIgnoreCase("activityStats")) {
            String username = user.getUsername();
            Meal meal = new Meal();
            ArrayList mealList = meal.mealReturnAll(username);
            //session.setAttribute("mealList", mealList);
            
            // Getting walks to display on graph
            ArrayList<Walk> walks = Walk.getWalksForUser(username);
            
            // Getting runs to display on graph
            ArrayList<Run> runs = Run.getRunsForUser(username);
            
            // Getting bikes to display on graph
            ArrayList<Bike> bikes = Bike.getRunsForUser(username);
            
            
            session.setAttribute("walks", walks);
            session.setAttribute("runs", runs);
            session.setAttribute("bikes", bikes);
            request.getRequestDispatcher("View/ActivityStats.jsp").forward(request, response);
        } 
        
        
        else if(request.getParameter("type").equalsIgnoreCase("deleteMeal")) {
            String[] mealNames = request.getParameterValues("delete");
            String username = user.getUsername();
            Meal meal = new Meal();
            for (int i = 0; i < mealNames.length; i++) {
                meal.deleteMeal(username, mealNames[i]);
            }
            request.getRequestDispatcher("/HistoryController?type=activityStats").forward(request, response);
        }
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet HistoryController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet HistoryController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
