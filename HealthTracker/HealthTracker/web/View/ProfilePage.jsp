<%-- 
    Document   : ProfilePage
    Created on : Mar 25, 2014, 2:51:19 PM
    Author     : Odie
--%>

<%@page import="java.util.Iterator"%>
<%@page import="Model.History"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:useBean id="aUser" type="Model.User" scope="session" />
        <jsp:useBean id="pageOwner" type="Model.User" scope="session" />
        <jsp:useBean id="historyList" type="ArrayList<History>" scope="session" />
        
        <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400italic' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="css/style2.css" type="text/css" />

        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!---strat-slider---->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/slider-style.css" />
        <script type="text/javascript" src="js/modernizr.custom.28468.js"></script>
        <!---//start-slider---->
        <!---start-login-script-->
        <script src="js/login.js"></script>
        <!---//End-login-script--->
        <!--768px-menu---->
        <link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
        <script type="text/javascript" src="js/jquery.mmenu.js"></script>
        <script type="text/javascript">
            //	The menu on the left
            $(function() {
                $('nav#menu-left').mmenu();
            });
        </script>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${pageOwner.username}</title>
    </head>
    <body>
        
        
        <div class="header">
            <div class="wrap">
                <!--<div class="header-left">
                        <div class="logo">
                                <a href="index.jsp">HealthTracker</a>
                        </div>
                </div>-->
                <div class="header-right2">
                    <div class="top-nav">
                        <ul>
                            <%                                                String username = aUser.getUsername();
                                String firstLetter = username.substring(0, 1);
                                String remainingLetters = username.substring(1, username.length());
                                username = firstLetter.toUpperCase() + remainingLetters;

                            %>

                            <li><a href="/HealthTracker/ProfilePageController?pageOwner=<%=aUser.getUsername()%>"><%= username%></a></li>
                            <li><a href="/HealthTracker/DataCaptureController?type=clicked">Update Activities</a></li>
                            <li><a href="/HealthTracker/GoalController?type=clicked">Goals</a></li>
                            <li><a href="/HealthTracker/GroupController?type=clicked">Groups</a></li>
                            <li><a href="/HealthTracker/HistoryController?type=history">History</a></li>
                            <li><a href="/HealthTracker/AchievmentController?type=clicked">Achievements</a></li>
                            <li><a href="/HealthTracker/HomeController?type=clickedEdit">Edit Profile</a></li>
                            <li><a href="/HealthTracker/HistoryController?type=activityStats">Activity Stats</a></li>
                        </ul>
                    </div>

                    <div class="clear"> </div>
                    </ul>
                </div>
                <div class="clear"> </div>
            </div>
            <div class="clear"> </div>
        </div>
                            
                            <br />
                            
                            <h1>${pageOwner.username}'s Page</h1><br />
                            
                            <ul class="controls">
                                <!--<li class="icon"><a href="NewMessage.jsp" onClick="MyWindow=window.open('/HealthTracker/View/NewMessages.jsp','MyWindow','toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no,width=400,height=300'); return false;" class="priority">D</a></li>-->
                                <a href="/HealthTracker/MessageController?type=viewMessages">Messages</a>
                            </ul>
        
        <a style="margin-left: 70%" href="/HealthTracker/LoginController?type=logout">Log out</a><br />
        
        
        Name: <b>${pageOwner.firstName} ${pageOwner.lastName}</b> <br />
        
        Age: ${pageOwner.age} <br />
        
        Sex: ${pageOwner.sex} <br />
        
        Weight: ${pageOwner.weight} <br />
        
        Height: ${pageOwner.height} <br />
        
        Heart Rate: ${pageOwner.heartRate} <br />
        
        Contact: ${pageOwner.email} <br />
        
        <em>**Ideally the result of the health algorithm should be displayed here instead of some of the above details**</em><br /><br />
        
        <!-- Displaying page owners history-->
        <!-- Graphs and charts and whatever should also be here-->
        <%

            if (session.getAttribute("historyList") != null) {

                ArrayList<History> list = (ArrayList<History>) session.getAttribute("historyList");
                Iterator it = list.iterator();

                while (it.hasNext()) {

                    History historyObject = (History) it.next();
        %>       

            <%=historyObject.getDate()%> ==> <%=historyObject.getText() %> <br /><br />
        
        <%                            }

            }

        %>
        
        <em>**Graphs and charts should be here too**</em>
    </body>
</html>
