<%-- 
    Document   : UserPage
    Created on : Mar 9, 2014, 4:36:35 PM
    Author     : Odie
--%>

<%@page import="Model.DistanceGoal"%>
<%@page import="Model.User"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Model.WeightGoal"%>
<%@page import="Utilities.DBAccess"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:useBean id="aUser" type="Model.User" scope="session" />
        <jsp:useBean id="weightGoals" type="ArrayList<WeightGoal>" scope="session" />
        <jsp:useBean id="distanceGoals" type="ArrayList<DistanceGoal>" scope="session" />
        <jsp:useBean id="totalRun" type="Integer" scope="session" />

    <head>
        <title>Health Tracker</title>
        <script>
            var patt = new RegExp("e");
            var passwordPatt = new RegExp("e");
            var pat = /[0-9A-Za-z]+/;
        </script>
        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    </script>
    <!---strat-slider---->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/slider-style.css" />
    <script type="text/javascript" src="js/modernizr.custom.28468.js"></script>
    <!---//start-slider---->
    <!---start-login-script-->
    <script src="js/login.js"></script>
    <!---//End-login-script--->
    <!--768px-menu---->
    <link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
    <script type="text/javascript" src="js/jquery.mmenu.js"></script>
    <script type="text/javascript">
            //	The menu on the left
            $(function() {
                $('nav#menu-left').mmenu();
            });
    </script>

    <!-- @start of script/hrefs for sign up pop up-->

    <link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="css/demo.css">
    <link rel="stylesheet" href="css/avgrund.css">

    <script>
        function openDialog() {
            Avgrund.show("#default-popup");
        }
        function closeDialog() {
            Avgrund.hide();
        }
    </script>
    <!-- @ end of script for sign up pop up-->            

    <!--//768px-menu-->

    <script type="text/javascript">

        function searchWhere() {
            if (document.getElementById('searchSite').checked) {
                document.getElementById("decision").value = "site";
            } else {
                document.getElementById("decision").value = "google";
            }
        }

    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>User Page (^_^)</title>


    <!--insert Doughnut-->
    <title>Doughnut Chart</title>
    <script src="js/Chart.js"></script>
    <style>
        canvas{
        }
    </style>

    <!--end of chart.js-->

</head>
<body <!--style="padding-top: 70px;-->">

    <!---start-wrap---->
    <!------start-768px-menu---->

    <!------start-768px-menu---->
    <!---start-header---->

    <!--<div class="header">
        <div class="wrap">
            <div class="header-left">
                <div class="logo">
                    <a href="index.jsp">Health Tracker</a>
                </div>                
            </div>
        </div>
    </div>
    -->
    <!---//End-header---->


    <%
        // Checks if time for goal has run out. This had to happen periodically and because we can have a thread for each
        // user, It checks everytime the user goes to their home page.
        try {

            java.util.Date utilDate = new java.util.Date();
            java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

            ArrayList<WeightGoal> weightGoalss = WeightGoal.getWeightGoalsForUser(aUser.getUsername());

            for (WeightGoal wg : weightGoalss) {
                if (wg.getDueDate().before(currentDate)) {
                    String sql = "DELETE FROM Goal WHERE goalOwner = '" + aUser.getUsername()
                            + "' AND goalName = '" + wg.getGoalName() + "'";

                    String sql2 = "INSERT INTO History VALUES('" + aUser.getUsername() + "', 'You failed to achieve your Weight Goal " + wg.getGoalName()
                            + ". But dont give up!', '" + currentDate + "')";

                    DBAccess.insertStatement(sql);
                    DBAccess.insertStatement(sql2);
                }
            }
        } catch (Exception e) {
        } finally {
        }
    %>


    <div class="header">
        <div class="wrap">
            <div class="header-right2">
                <div class="top-nav">
                    <ul>
                        <%                                                String username = aUser.getUsername();
                            String firstLetter = username.substring(0, 1);
                            String remainingLetters = username.substring(1, username.length());
                            username = firstLetter.toUpperCase() + remainingLetters;

                        %>

                        <li><a href="/HealthTracker/ProfilePageController?pageOwner=<%=aUser.getUsername()%>"><%= username%></a></li>
                        <li><a href="/HealthTracker/DataCaptureController?type=clicked">Update Activities</a></li>
                        <li><a href="/HealthTracker/GoalController?type=clicked">Goals</a></li>
                        <li><a href="/HealthTracker/GroupController?type=clicked">Groups</a></li>
                        <li><a href="/HealthTracker/HistoryController?type=history">History</a></li>
                        <li><a href="/HealthTracker/AchievmentController?type=clicked">Achievements</a></li>
                        <li><a href="/HealthTracker/HomeController?type=clickedEdit">Edit Profile</a></li>
                        <li><a href="/HealthTracker/HistoryController?type=activityStats">Activity Stats</a></li>
                    </ul>
                </div>

                <div class="clear"> </div>
                </ul>
            </div>
            <div class="clear"> </div>
        </div>
        <div class="clear"> </div>
    </div>
    <br />
    <br />

    
        <div class="user3">
            <div class="user">
                <div class ="profilepic">
                    <img src="http://www.sportspickle.com/wp-content/uploads/2012/11/169e1e366bf6a0a16ed19e3eb0bdf2ba.jpg" alt="some_text">
                </div>

                <ul>Hello <b>${aUser.firstName} ${aUser.lastName}</b></ul>
                <ul><b>${aUser.username}</b></ul>
                <!--<ul>and ... I wont tell anyone but ..your password. (>_>) (<_<) Its <b>${aUser.password}</b> (o_o) <br /><br /></ul>-->
                <!--<ul>Your email is <b>${aUser.email}</b> and you're a <b>${aUser.age}</b> year old <b>${aUser.sex}</b>. <br /><br /></ul>
                <ul>Your weight is <b>${aUser.weight}</b>, your height is <b>${aUser.height}</b> and your resting heart rate is <b>${aUser.heartRate}</b>.<br /><br/></ul>
                <ul>How'd I know all this? SESSIONS! THATS HOW BITCH! \(^_^)/</ul>-->
                <ul><b>Goals Achieved: 198</b></ul>
                <ul>
                    <%  double w = (double) aUser.getWeight();
                        double h = (double) aUser.getHeight();
                        double BMI = 0;

                        if (w != 0 && h != 0) {
                            BMI = w / Math.pow(h, 2);
                        }
                        String message = null;

                        if (BMI < 18.5) {
                            message = "Underweight";
                        } else if (BMI >= 18.5 && BMI < 25) {
                            message = "Healthy";
                        } else if (BMI >= 25 && BMI < 30) {
                            message = "Overweight";
                        } else {
                            message = "Very overweight";
                        }

                    %>
                    Your BMI is <b><%out.println(BMI);%></b>, this means you are <%out.println(message);%></ul><br>
                <hr>
                <br>

                <ul><li><a href="/HealthTracker/HomeController?type=clickedEdit">Edit Profile</a></li></ul>
                <ul>My Groups</ul>
                <ul><p>Uni Group</p><ul>
                        <ul><p>Family Group</p><ul>        
                                <br/>
                                <hr>
                                <br>
                                <ul><h3>Recent Achievements</h3></ul>
                                <ul>Ran 1000 miles</ul>
                                <ul>Walked a mile in my shoes</ul>
                                <ul>Lost 1 KG in weight</ul>

                                </div>

                                <div class="user2">

                                    <div class="Totals">

                                        <div class="Totalsa">

                                            <div class="TotIcon">
                                                <img src="View/IconImages/IconRun.png" alt="IconRun" width="100" height="100">   
                                            </div>   

                                            <ul>Total Running Distance = <%=totalRun%>m </ul>
                                        </div>

                                        <div class="Totalsa">

                                            <div class="TotIcon">
                                                <img src="View/IconImages/IconBike.png" alt="IconRun" width="100" height="100">   
                                            </div>  

                                            <ul>Total Biking Distance = 140m</ul>
                                        </div>

                                        <div class="Totalsa">

                                            <div class="TotIcon">
                                                <img src="View/IconImages/IconScale.png" alt="IconRun" width="100" height="100">   
                                            </div>  

                                            <ul>Total Weight loss in 6 months = 2 KG</ul>
                                        </div>
                                    </div>
                                    <!-- @start CHART 1 -->
                                    <div class="chart">
                                        <p> Weight Goal - <% if (weightGoals.size() > 0) {
                                        out.println(weightGoals.get(0).getGoalName());
                                    }%></p>
                                        <canvas id="canvas" height="150" width="150"></canvas>							
                                    </div>
                                    <!-- @end CHART 1 -->

                                    <!-- @start CHART 2 -->
                                    <div class="chart">
                                        <p> Distance Goal - <% if (distanceGoals.size() > 0) {
                                        out.println(distanceGoals.get(0).getGoalName());
                                    }%></p>
                                        <canvas id="canvas2" height="150" width="150"></canvas>
                                    </div>
                                    <!-- @end CHART 2 -->

                                    <!--@start CHART 3 -->
                                    <div class="chart">
                                        <p> Weight Goal</p>
                                        <canvas id="canvas3" height="150" width="150"></canvas>
                                    </div>
                                    <!-- @end CHART 3 -->

                                    <!-- @start CHART 4 -->
                                    <div class="chart">
                                        <p> Running Goal 1</p>
                                        <canvas id="canvas4" height="150" width="150"></canvas>							
                                    </div>
                                    <!-- @end CHART 1 -->

                                    <!-- @start CHART 2 -->
                                    <div class="chart">
                                        <p> Bike Riding Goal</p>
                                        <canvas id="canvas5" height="150" width="150"></canvas>
                                    </div>
                                    <!-- @end CHART 2 -->

                                    <!--@start CHART 3 -->
                                    <div class="chart">
                                        <p> Weight Goal</p>
                                        <canvas id="canvas6" height="150" width="150"></canvas>
                                    </div>
                                    <!-- @end CHART 3 -->

                                </div>

                                <div class="user2">
                                    <p>Top Trophies<p>
                                    <div class ="Trophies">
                                        <h1>Joined Health Tracker</h1>   
                                        <h1>Achieved 1st Group Goal</h1>   
                                        <h1>Beat Running Distance PB</h1>   

                                    </div>    
                                    </body>

                                    <!-- Script for the Pie Charts-->

                                    <%
                                        if (weightGoals.size() > 0) {
                                            double progress = weightGoals.get(0).getPercentCompletion(aUser.getWeight());
                                    %>
                                    <script>
        var doughnutData = [
            {
                value: <%= 100 - progress%>,
                color: "#F7464A"
            },
            {
                value: <%= progress%>,
                color: "#46BFBD"
            }

        ];

        var myDoughnut = new Chart(document.getElementById("canvas").getContext("2d")).Doughnut(doughnutData);
                                    </script>  
                                    <%

                                        }
                                    %>

                                    <%
                                        double progress = 0;
                                        if (distanceGoals.size() > 0) {
                                            progress = distanceGoals.get(0).getPercentCompletion();
                                    %>
                                    <script>
                                        var doughnutData = [
                                            {
                                                value: <%= 100 - progress%>,
                                                color: "#F7464A"
                                            },
                                            {
                                                value: <%= progress%>,
                                                color: "#46BFBD"
                                            }

                                        ];

                                        var myDoughnut = new Chart(document.getElementById("canvas2").getContext("2d")).Doughnut(doughnutData);
                                    </script>  
                                    <%

                                        }
                                    %>

                                    <!--<script>
                                            var doughnutData = [
                                                {
                                                    value: 30,
                                                    color: "#F7464A"
                                                },
                                                {
                                                    value: 50,
                                                    color: "#46BFBD"
                                                }
            
                                            ];
                                            
                                            var doughnutData2 = [
                                                {
                                                    value: 30,
                                                    color: "#F7464A"
                                                },
                                                {
                                                    value: 50,
                                                    color: "#46BFBD"
                                                }
                                            ];
                                            
                                            var myDoughnut = new Chart(document.getElementById("canvas").getContext("2d")).Doughnut(doughnutData);
                                            var myDoughnut = new Chart(document.getElementById("canvas2").getContext("2d")).Doughnut(doughnutData);
                                            var myDoughnut = new Chart(document.getElementById("canvas3").getContext("2d")).Doughnut(doughnutData);
                                            var myDoughnut = new Chart(document.getElementById("canvas4").getContext("2d")).Doughnut(doughnutData);
                                            var myDoughnut = new Chart(document.getElementById("canvas5").getContext("2d")).Doughnut(doughnutData);
                                            var myDoughnut = new Chart(document.getElementById("canvas6").getContext("2d")).Doughnut(doughnutData);
                                    </script>
                                    </div>
                                    -->

                                    <div class="wrap">

                                        <div class="clear">
                                            <a style="margin-left: 70%" href="/HealthTracker/LoginController?type=logout">Log out</a>
                                            <form style="margin-left: 70%" action="/HealthTracker/SearchController" method="POST">
                                                <input type="text" name="query" placeholder="Search for..."><br />
                                                On Site
                                                <input type="radio" onclick="javascript:searchWhere();" id="searchSite" />Google
                                                <input type="radio" onclick="javascript:searchWhere();" id="searchGoogle"/>
                                                <input type = "hidden" value = "site" name = "placeToSearch" id="decision" /> 
                                                <input class="sub" type="submit" value="Submit" />
                                            </form>

                                        </div>



                                        <!--start-content-->
                                        <div class="content">
                                            <div class="wrap">
                                                <div class="footer-grids">
                                                    <div class="wrap">
                                                        <div class="footer-grid">
                                                            <h3>Quick Links</h3>
                                                            <ul>
                                                                <li><a href="#">Home</a></li>
                                                                <li><a href="#">About Features</a></li>
                                                                <li><a href="#">Login</a></li>
                                                                <li><a href="#">Sign Up</a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="footer-grid">
                                                            <h3>More</h3>
                                                            <ul>
                                                                <li><a href="#">FAQ</a></li>
                                                                <li><a href="#">Support</a></li>
                                                                <li><a href="#">Privacy Policy</a></li>
                                                                <li><a href="#">Terms and Conditions</a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="footer-grid">
                                                            <h3>Connect With Us</h3>
                                                            <ul class="social-icons">
                                                                <li><a class="facebook" href="#"> </a></li>
                                                                <li><a class="twitter" href="#"> </a></li>
                                                                <li><a class="youtube" href="#"> </a></li>
                                                            </ul>
                                                        </div>

                                                        <div class="clear"> </div>
                                                    </div>
                                                </div>

                                                <p class="copy-right"><it><b>Disclaimer:</b> This application is not a commercial application and does not provide insurance. 
                                                    This is a study project that is part of a Computing Science module taught at the University of East Anglia,
                                                    Norwich, UK. If you have any questions, please contact the module coordinator,
                                                    Joost Noppen, at j.noppen@uea.ac.uk</it></p>
                                                <!---//End-bottom-footer-grids---->
                                            </div> 
                                       
                                        </body>
                                        </html>
