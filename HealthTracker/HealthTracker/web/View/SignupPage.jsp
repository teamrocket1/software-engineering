<%-- 
    Document   : SignupPage
    Created on : Mar 27, 2014, 11:51:03 PM
    Author     : Odie
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h3>SIGN UP</h3>
		
		<!-- Signup form -->
        <form action="/HealthTracker/LoginController" method="POST">

            <input type="text" name="firstname" placeholder="firstname"
                   maxlength ="20" required> <br />
            <input type="text" name="lastname" placeholder="lastname"
                   maxlength ="20" required> <br />
            <input type="text" name="username" placeholder="username" 
                   maxlength ="20" required> <br />
            <input type="password" name="password" placeholder="password"
                   maxlength ="20" title="Your password must conatin at least 
                   eight symbols containing at least one number,
                   one lower, and one upper letter. Thank You."
                   pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" required> <br />
            <input type="email" name="email" placeholder="email"
                   maxlength ="30" required> <br />
            <input title="Please enter 'm' for male, 'f' for femal or 'o' for
                    other. Thank You."
                   type="text" name="sex" placeholder="sex"
                   maxlength ="1" pattern="[mfo]" required> <br />
            <input type="number" name="age" placeholder="age"
                   maxlength ="3" min="1" step="1" required> <br />
            <input type="number" name="weight" placeholder="weight"
                   maxlength ="10" min="1" step="1" required> 
            <select name="weightUnit">
                <option value="Kg">Kg</option>
                <option value="lbs">lbs</option>
            </select><br />
            <input type="number" name="height" placeholder="height"
                   maxlength ="10"  min="1" step="1" required>cm <br />
            <input type="number" name="heartrate" placeholder="heartrate"
                   maxlength ="10" min="1" max="220" step="1" required> <br />
            <input type = "hidden" value = "signUp" name = "type" /> 
            <input class="sub" type="submit" value="Sign Up" />

            <!--<button>L</button>-->

        </form>
    </body>
</html>
