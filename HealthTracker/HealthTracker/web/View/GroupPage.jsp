<%-- 
    Document   : GroupPage
    Created on : Mar 23, 2014, 6:37:53 PM
    Author     : Odie
--%>

<%@page import="Model.WeightGoal"%>
<%@page import="Model.DistanceGoal"%>
<%@page import="Model.FitnessGoal"%>
<%@page import="Model.CustomGoal"%>
<%@page import="Model.Membership.Role"%>
<%@page import="Model.Comment"%>
<%@page import="java.util.Iterator"%>
<%@page import="Model.History"%>
<%@page import="Model.Membership"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:useBean id="aUser" type="Model.User" scope="session" />
        <jsp:useBean id="aGroup" type="Model.Group" scope="session" />
        <jsp:useBean id="memberships" type="ArrayList<Membership>" scope="session" />
        <jsp:useBean id="membership" type="Membership" scope="session" />
        <jsp:useBean id="historyList" type="ArrayList<History>" scope="session" />
        <jsp:useBean id="commentList" type="ArrayList<Comment>" scope="session" />


        <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400italic' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="css/style2.css" type="text/css" />

        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!---strat-slider---->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/slider-style.css" />
        <script type="text/javascript" src="js/modernizr.custom.28468.js"></script>
        <!---//start-slider---->
        <!---start-login-script-->
        <script src="js/login.js"></script>
        <!---//End-login-script--->
        <!--768px-menu---->
        <link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
        <script type="text/javascript" src="js/jquery.mmenu.js"></script>
        <script type="text/javascript">
            //	The menu on the left
            $(function() {
                $('nav#menu-left').mmenu();
            });
        </script>


        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${aGroup.groupName} Page</title>
    </head>
    <body>

        <div class="header">
            <div class="wrap">
                <!--<div class="header-left">
                        <div class="logo">
                                <a href="index.jsp">HealthTracker</a>
                        </div>
                </div>-->
                <div class="header-right2">
                    <div class="top-nav">
                        <ul>
                            <%                                                String username = aUser.getUsername();
                                String firstLetter = username.substring(0, 1);
                                String remainingLetters = username.substring(1, username.length());
                                username = firstLetter.toUpperCase() + remainingLetters;

                            %>

                            <li><a href="/HealthTracker/ProfilePageController?pageOwner=<%=aUser.getUsername()%>"><%= username%></a></li>
                            <li><a href="/HealthTracker/DataCaptureController?type=clicked">Update Activities</a></li>
                            <li><a href="/HealthTracker/GoalController?type=clicked">Goals</a></li>
                            <li><a href="/HealthTracker/GroupController?type=clicked">Groups</a></li>
                            <li><a href="/HealthTracker/HistoryController?type=history">History</a></li>
                            <li><a href="/HealthTracker/AchievmentController?type=clicked">Achievements</a></li>
                            <li><a href="/HealthTracker/HomeController?type=clickedEdit">Edit Profile</a></li>
                            <li><a href="/HealthTracker/HistoryController?type=activityStats">Activity Stats</a></li>
                        </ul>
                    </div>

                    <div class="clear"> </div>

                </div>
                <div class="clear"> </div>
            </div>
            <div class="clear"> </div>
        </div>

        <br />

        <h1>${aGroup.groupName}</h1>
        <!--Displaying goals-->

        <form action="/HealthTracker/GroupGoalController" method="POST">
            <table border="1">                
                <tr>
                    <td>Goal Name</td>
                    <td>Start Date</td>
                    <td>Due Date</td>
                    <td>Goal Type</td>
                    <td>Percent Complete</td>
                </tr>
                <%
                    if (session.getAttribute("weightGoals") != null) {

                        ArrayList<WeightGoal> wg = (ArrayList<WeightGoal>) session.getAttribute("weightGoals");
                        Iterator it = wg.iterator();
                        while (it.hasNext()) {

                            WeightGoal g = (WeightGoal) it.next();

                            double v = (double) g.getTargetWeight();
                            double u = (double) aUser.getWeight();
                            double completion = 0.0;

                            if (u > v) {
                                completion = 100 - (((u - v + 1) / u) * 100);
                            } else {
                                completion = 100 + (((u - v + 1) / u) * 100);
                            }


                %>

                <tr>
                    <td><a href="/HealthTracker/GroupGoalController?type=clickedGroupGoal&goalName=<%=g.getGoalName()%>&goalOwner=${aGroup.groupName}"><%out.println(g.getGoalName());%></a></td>
                    
                    <td><%=g.getStartDate()%></td>
                    <td><%=g.getDueDate()%></td>
                    <td>Weight Goal</td>
                    <td><%=(int) completion%>%</td>
                </tr>


                <%
                    }
                %>

                <%
                    }
                %>

                <%
                    if (session.getAttribute("distanceGoals") != null) {

                        ArrayList<DistanceGoal> wg = (ArrayList<DistanceGoal>) session.getAttribute("distanceGoals");
                        Iterator it = wg.iterator();
                        while (it.hasNext()) {

                            DistanceGoal g = (DistanceGoal) it.next();
                            double v = (double) g.getTargetDistance();
                            double u = (double) g.getAchievedDistance();
                            double completion = 0.0;

                            completion = 100 - ((v - u) / v) * 100;

                            Object o = g;



                %>
                <%=o.getClass()%>
                <tr>
                    <td><a href="/HealthTracker/GroupGoalController?type=clickedGroupGoal&goalName=<%=g.getGoalName()%>&goalOwner=${aGroup.groupName}"><%out.println(g.getGoalName());%></a></td>
                    <td><%=g.getStartDate()%></td>
                    <td><%=g.getDueDate()%></td>
                    <td>Distance Goal</td>
                    <td><%=(int) completion%>%</td>
                </tr>


                <%
                    }
                %>

                <%
                    }
                %>

                <%
                    if (session.getAttribute("fitnessGoals") != null) {

                        ArrayList<FitnessGoal> wg = (ArrayList<FitnessGoal>) session.getAttribute("fitnessGoals");
                        Iterator it = wg.iterator();
                        while (it.hasNext()) {

                            FitnessGoal g = (FitnessGoal) it.next();
                            double v = (double) g.getTargetHeartRate();
                            double u = (double) aUser.getHeartRate();
                            double completion = 0.0;

                            completion = 100 - ((v - u) / v) * 100;

                            Object o = g;



                %>
                <%=o.getClass()%>
                <tr>
                    <td><a href="/HealthTracker/GroupGoalController?type=clickedGroupGoal&goalName=<%=g.getGoalName()%>&goalOwner=${aGroup.groupName}"><%out.println(g.getGoalName());%></a></td>
                    <td><%=g.getStartDate()%></td>
                    <td><%=g.getDueDate()%></td>
                    <td>Fitness Goal</td>
                    <td><%=(int) completion%>%</td>
                </tr>


                <%
                    }
                %>

                <%
                    }
                %>

                <%
                    if (session.getAttribute("customGoals") != null) {

                        ArrayList<CustomGoal> wg = (ArrayList<CustomGoal>) session.getAttribute("customGoals");
                        Iterator it = wg.iterator();
                        while (it.hasNext()) {

                            double completion1 = 0.0;
                            double completion2 = 0.0;
                            double completion3 = 0.0;
                            int n = 0;

                            CustomGoal g = (CustomGoal) it.next();
                            if (g.getTargetDistance() != 0) {
                                double v = (double) g.getTargetDistance();
                                double u = (double) g.getAchievedDistance();
                                completion1 = 0.0;
                                completion1 = 100 - ((v - u) / v) * 100;
                                n++;
                            }

                            if (g.getTargetWeight() != 0) {
                                double v = (double) g.getTargetWeight();
                                double u = (double) aUser.getWeight();
                                completion2 = 0.0;
                                completion2 = 100 - ((v - u) / v) * 100;
                                n++;
                            }

                            if (g.getTargetHeartRate() != 0) {
                                double v = (double) g.getTargetHeartRate();
                                double u = (double) aUser.getHeartRate();
                                completion3 = 0.0;
                                completion3 = 100 - ((v - u) / v) * 100;
                                n++;
                            }

                            double completion = (completion1 + completion2 + completion3) / n;

                            Object o = g;



                %>
                <%=o.getClass()%>
                <tr>
                    <td><a href="/HealthTracker/GroupGoalController?type=clickedGroupGoal&goalName=<%=g.getGoalName()%>&goalOwner=${aGroup.groupName}"><%out.println(g.getGoalName());%></a></td>
                    <td><%=g.getStartDate()%></td>
                    <td><%=g.getDueDate()%></td>
                    <td>Custom Goal</td>
                    <td><%=(int) completion%>%</td>
                </tr>


                <%
                    }
                %>

                <%
                    }
                %>

            </table>
        </form>


        <a href="/HealthTracker/HomeController?type=clicked"><h3>Home</h3></a> 
        <%
            if (membership.getRole() == Role.A) {
        %>
        <a href="/HealthTracker/GroupGoalController?type=clickedGroup&groupName=${aGroup.groupName}"><h3>Create Group Goal</h3></a>
        <%            }
        %>
        <a style="margin-left: 70%" 
           href="/HealthTracker/LoginController?type=logout">Log out</a><br />


        <%

            if (session.getAttribute("historyList") != null) {

                ArrayList<History> list = (ArrayList<History>) session.getAttribute("historyList");
                Iterator it = list.iterator();

                while (it.hasNext()) {

                    History historyObject = (History) it.next();
        %>       

        <%=historyObject.getDate()%> ==> <%=historyObject.getUsername()%> <%=historyObject.getText()%> <br /><br />

        <%                            }

            }

        %>

        <br />
        ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        <h3>Comments</h3>

        <%

            if (session.getAttribute("commentList") != null) {

                ArrayList<Comment> list = (ArrayList<Comment>) session.getAttribute("commentList");
                Iterator it = list.iterator();

                while (it.hasNext()) {

                    Comment commentObject = (Comment) it.next();
        %>       

        <%=commentObject.getDate()%> | <em><%=commentObject.getUsername()%></em> ==> <%=commentObject.getText()%> <br /><br />

        <%                            }

            }

        %>

        <br /><br />

        <form  action="/HealthTracker/GroupController" method="POST">
            <textarea  name="text" rows="7" cols="35"></textarea><br />
            <input type = "hidden"  name = "type" value="comment" /> 
            <input class="sub" type="submit" value="Comment" />
        </form>

    </body>
</html>
