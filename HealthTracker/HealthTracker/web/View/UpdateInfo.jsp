<%-- 
    Document   : UpdateInfo
    Created on : Mar 9, 2014, 8:01:23 PM
    Author     : Odie
--%>

<%@page import="Model.Meal"%>
<%@page import="java.util.Iterator"%>
<%@page import="Model.Diet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:useBean id="aUser" type="Model.User" scope="session" />
        <jsp:useBean id="meals" type="Model.Diet" scope="session" />

        
        <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400italic' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="css/style2.css" type="text/css" />

        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!---strat-slider---->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/slider-style.css" />
        <script type="text/javascript" src="js/modernizr.custom.28468.js"></script>
        <!---//start-slider---->
        <!---start-login-script-->
        <script src="js/login.js"></script>
        <!---//End-login-script--->
        <!--768px-menu---->
        <link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
        <script type="text/javascript" src="js/jquery.mmenu.js"></script>
        <script type="text/javascript">
            //	The menu on the left
            $(function() {
                $('nav#menu-left').mmenu();
            });
        </script>
        
        <script type="text/javascript">

            function yesnoCheckFitness() {
                if (document.getElementById('yesCheckFitness').checked) {
                    document.getElementById('ifYesFitness').style.display = 'block';
                    document.getElementById('ifYesDiet').style.display = 'none';
                } else {
                    document.getElementById('ifYesFitness').style.display = 'none';
                }
            }

            function yesnoCheckDiet() {
                if (document.getElementById('yesCheckDiet').checked) {
                    document.getElementById('ifYesDiet').style.display = 'block';
                    document.getElementById('ifYesFitness').style.display = 'none';
                } else {
                    document.getElementById('ifYesDiet').style.display = 'none';
                }
            }

            function yesnoCheckCustomDiet() {
                if (document.getElementById('yesCheckCustomDiet').checked) {
                    document.getElementById('selectMeal').style.display = 'none';
                    document.getElementById('customMeal').style.display = 'block';

                    document.getElementById("selectedMeal").value = "!set";
                    document.getElementById("customisedMeal").value = "set";
                } else {
                    document.getElementById('selectMeal').style.display = 'block';
                    document.getElementById('customMeal').style.display = 'none';

                    document.getElementById("selectedMeal").value = "set";
                    document.getElementById("customisedMeal").value = "!set";
                }
            }

            function activityCheck() {
                var dropdown = document.getElementById("activitySelect");
                var selected = dropdown.options[dropdown.selectedIndex].value;

                if (selected == 0) {
                    document.getElementById('walkForm').style.display = 'block';
                    document.getElementById('runForm').style.display = 'none';
                    document.getElementById('bikeForm').style.display = 'none';

                    document.getElementById("walkType").value = "set";
                    document.getElementById("runType").value = "!set";
                    document.getElementById("bikeType").value = "!set";

                }

                if (selected == 1) {
                    document.getElementById('walkForm').style.display = 'none';
                    document.getElementById('runForm').style.display = 'block';
                    document.getElementById('bikeForm').style.display = 'none';

                    document.getElementById("walkType").value = "!set";
                    document.getElementById("runType").value = "set";
                    document.getElementById("bikeType").value = "!set";
                }

                if (selected == 2) {
                    document.getElementById('walkForm').style.display = 'none';
                    document.getElementById('runForm').style.display = 'none';
                    document.getElementById('bikeForm').style.display = 'block';

                    document.getElementById("walkType").value = "!set";
                    document.getElementById("runType").value = "!set";
                    document.getElementById("bikeType").value = "set";
                }

            }

        </script>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Update Fitness/Diet (>_<)</title>
    </head>
    <body>
        
        <div class="header">
            <div class="wrap">
                <!--<div class="header-left">
                        <div class="logo">
                                <a href="index.jsp">HealthTracker</a>
                        </div>
                </div>-->
                <div class="header-right2">
                    <div class="top-nav">
                        <ul>
                            <%                                                String username = aUser.getUsername();
                                String firstLetter = username.substring(0, 1);
                                String remainingLetters = username.substring(1, username.length());
                                username = firstLetter.toUpperCase() + remainingLetters;

                            %>

                            <li><a href="/HealthTracker/ProfilePageController?pageOwner=<%=aUser.getUsername()%>"><%= username%></a></li>
                            <li><a href="/HealthTracker/DataCaptureController?type=clicked">Update Activities</a></li>
                            <li><a href="/HealthTracker/GoalController?type=clicked">Goals</a></li>
                            <li><a href="/HealthTracker/GroupController?type=clicked">Groups</a></li>
                            <li><a href="/HealthTracker/HistoryController?type=history">History</a></li>
                            <li><a href="/HealthTracker/AchievmentController?type=clicked">Achievements</a></li>
                            <li><a href="/HealthTracker/HomeController?type=clickedEdit">Edit Profile</a></li>
                            <li><a href="/HealthTracker/HistoryController?type=activityStats">Activity Stats</a></li>
                        </ul>
                    </div>

                    <div class="clear"> </div>
                    </ul>
                </div>
                <div class="clear"> </div>
            </div>
            <div class="clear"> </div>
        </div>
        
        <br />
                            
        <h1>Update Fitness/Diet</h1>

        <a style="margin-left: 70%" href="/HealthTracker/LoginController?type=logout">Log out</a>

        <form action="/HealthTracker/DataCaptureController" method="POST">

            Update Fitness Information? Yes
            <input type="radio" onclick="javascript:yesnoCheckFitness();" name="yesno" id="yesCheckFitness"/>No
            <input type="radio" onclick="javascript:yesnoCheckFitness();" name="yesno" id="noCheckFitness"/>
            <br>	


            <div id="ifYesFitness" style="display:none">

                Select Activity
                <select id="activitySelect" onclick="javascript:activityCheck();" name = "activity">
                    <option value="0">Walk</option>
                    <option value="1">Run</option>
                    <option value="2">Bike</option>
                    <option value="unselected" selected="selected">Select Activity</option>
                </select>
                <br />

                <div id="walkForm" style="display:none">
                    <label for="duration">Duration:</label>
                    <input type="text" name="walkDuration" placeholder="duration" /> <br />
                    <label for="Distance">Distance:</label>
                    <input type="text" name="walkDistance" placeholder="distance" /> 
                    <select name="walkDistanceUnit">
                        <option value="Km">Km</option>
                        <option value="mi">mi</option>
                    </select><br /><br />
                    <input id = "walkType" type = "hidden" value = "fitness1" name = "type1" />
                    <input class="sub" type="submit" value="Submit" />
                </div>

                <div id="runForm" style="display:none">
                    <input type="text" name="runDuration" placeholder="duration" /> <br />
                    <input type="text" name="runDistance" placeholder="distance" /> 
                    <select name="runDistanceUnit">
                        <option value="Km">Km</option>
                        <option value="mi">mi</option>
                    </select><br />
                    <input id = "runType" type = "hidden" value = "fitness2" name = "type2" />
                    <input class="sub" type="submit" value="Submit" />
                </div>

                <div id="bikeForm" style="display:none">
                    <input type="text" name="bikeDuration" placeholder="duration" /> <br />
                    <input type="text" name="bikeDistance" placeholder="distance" />
                    <select name="bikeDistanceUnit">
                        <option value="Km">Km</option>
                        <option value="mi">mi</option>
                    </select><br />
                    <input type="text" name="bikeSpeed" placeholder="speed" /> <br />
                    <input id = "bikeType" type = "hidden" value = "fitness3" name = "type3" />
                    <input class="sub" type="submit" value="Submit" />
                </div>


            </div>

            Update Diet Information? Yes
            <input type="radio" onclick="javascript:yesnoCheckDiet();" name="yesno" id="yesCheckDiet"/>No
            <input type="radio" onclick="javascript:yesnoCheckDiet();" name="yesno" id="noCheckDiet"/>
            <br>

            <div id="ifYesDiet" style="display:none">


                <div id="selectMeal" style="display:block">

                    Food: <select name="food">

                        <%
                            Iterator it = meals.iterator();

                            while (it.hasNext()) {

                                Meal m = (Meal) it.next();
                                if (m.getType().equalsIgnoreCase("food")) {
                        %>        
                        <option value="<%=m.getName()%>"><%=m.getName()%></option>
                        <%
                                }
                            }
                        %>   
                    </select><br />


                    Drink: <select name ="drink">

                        <%
                            Iterator it2 = meals.iterator();

                            while (it2.hasNext()) {

                                Meal m = (Meal) it2.next();
                                if (m.getType().equalsIgnoreCase("drink")) {
                        %>        
                        <option value="<%=m.getName()%>"><%=m.getName()%></option>
                        <%
                                }
                            }
                        %>   
                    </select>
                    <input id="selectedMeal" type = "hidden" value = "diet" name = "type4" /> 
                    <input class="sub" type="submit" value="Submit" />
                    <br />

                </div>


                Enter Custom Meal: Yes
                <input type="radio" onclick="javascript:yesnoCheckCustomDiet();" name="yesno" id="yesCheckCustomDiet"/>No
                <input type="radio" onclick="javascript:yesnoCheckCustomDiet();" name="yesno" id="noCheckCustomDiet"/>
                <br>
                <div id="customMeal" style="display:none">
                    <input type="text" name="customFood" placeholder="food">   <input type="text" name="foodCalories" placeholder="Calories"> <br />
                    <input type="text" name="customDrink" placeholder="drink">  <input type="text" name="drinkCalories" placeholder="Calories"><br />
                    <input id="customisedMeal" type = "hidden" value = "diet" name = "type5" /> 
                    <input class="sub" type="submit" value="Submit" />
                </div>
   
            </div>


            <!-- This hidden field is used to check whether entering a controller from a form or link -->
            <input type = "hidden" value = "..." name = "type" /> 


        </form>

<!-- 

Delete Meal Stuff

    <h1>Meal(s) List!</h1>
        <form action="HistoryController" method="POST">
            <table border ="1">
                <tr>
                    <td>Meal Name</td>
                    <td>Calories</td>
                    <td>Type</td>
                    <td>Delete?</td>
                </tr>
                <%
                   /* List mealList = (List) session.getAttribute("mealList");
                    Iterator it = mealList.iterator();
                    while (it.hasNext()) {
                        Meal meal = (Meal) it.next();*/
                %>
                <tr>
                    <td><%//meal.getName()%></td>
                    <td><%//meal.getCalories()%></td>
                    <td><%//meal.getType()%></td>
                    <td><input value="<%//meal.getName()%>" type="checkbox" name="delete"> </td>
                </tr>
                <%//}%>
            </table>
            <input type="submit" value="deleteMeal" name ="type">
        </form>

-->
                    
                    
        <!--<img src="/HealthTracker/Images/bill.gif" />-->

    </body>
</html>
