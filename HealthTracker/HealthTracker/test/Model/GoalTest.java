/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 ** Test Case
 * @author Takomborerwa
 */
public class GoalTest {
    
    public GoalTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of isCompleted method, of class Goal.
     */
    @Test
    public void testIsCompleted() {
        System.out.println("isCompleted");
        Goal instance = new GoalImpl();
        boolean expResult = false;
        boolean result = instance.isCompleted();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getGoalName method, of class Goal.
     */
    @Test
    public void testGetGoalName() {
        System.out.println("getGoalName");
        Goal instance = new GoalImpl();
        String expResult = "";
        String result = instance.getGoalName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setGoalName method, of class Goal.
     */
    @Test
    public void testSetGoalName() {
        System.out.println("setGoalName");
        String goalName = "";
        Goal instance = new GoalImpl();
        instance.setGoalName(goalName);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getStartDate method, of class Goal.
     */
    @Test
    public void testGetStartDate() {
        System.out.println("getStartDate");
        Goal instance = new GoalImpl();
        Date expResult = null;
        Date result = instance.getStartDate();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setStartDate method, of class Goal.
     */
    @Test
    public void testSetStartDate() {
        System.out.println("setStartDate");
        Date startDate = null;
        Goal instance = new GoalImpl();
        instance.setStartDate(startDate);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDueDate method, of class Goal.
     */
    @Test
    public void testGetDueDate() {
        System.out.println("getDueDate");
        Goal instance = new GoalImpl();
        Date expResult = null;
        Date result = instance.getDueDate();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDueDate method, of class Goal.
     */
    @Test
    public void testSetDueDate() {
        System.out.println("setDueDate");
        Date dueDate = null;
        Goal instance = new GoalImpl();
        instance.setDueDate(dueDate);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of isStatus method, of class Goal.
     */
    @Test
    public void testIsStatus() {
        System.out.println("isStatus");
        Goal instance = new GoalImpl();
        boolean expResult = false;
        boolean result = instance.isStatus();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setStatus method, of class Goal.
     */
    @Test
    public void testSetStatus() {
        System.out.println("setStatus");
        boolean status = false;
        Goal instance = new GoalImpl();
        instance.setStatus(status);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    public class GoalImpl extends Goal {

        public boolean isCompleted() {
            return false;
        }
    }
}
