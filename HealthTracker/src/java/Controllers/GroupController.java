/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controllers;

import Model.Comment;
import Model.CustomGoal;
import Model.DistanceGoal;
import Model.FitnessGoal;
import Model.Group;
import Model.History;
import Model.Membership;
import Model.Membership.Role;
import Model.User;
import Model.WeightGoal;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * Used to display dedicated goal pages on search or on click.
 * 
 * @author Odie
 */
public class GroupController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("aUser");
        
        
        Group aGroup = null;
        if(session.getAttribute("aGroup") != null){
            aGroup = (Group) session.getAttribute("aGroup");
        }
        
        String msg = "";
        
        if(request.getParameter("type").equalsIgnoreCase("createGroup")){
            String groupName = request.getParameter("groupName");
            
            Group newGroup = new Group(groupName);
            
            if(newGroup.createGroup(user.getUsername())){
                // redirect to appropriate feedback page here
                request.getRequestDispatcher("View/UserPage.jsp").forward(request, response);
            }
            else{
                // redirect to appropriate error page here
                    String errorMessage = "Error creating group, make sure you have enter a unqiue group name.";
                    session.setAttribute("errorMessage", errorMessage);
                    request.getRequestDispatcher("View/Error.jsp").forward(request, response); 
            }
            
            
        }
        else if(request.getParameter("type").equalsIgnoreCase("clicked")){
            ArrayList<Membership> memberships = Membership.getUsersMemberships(user.getUsername());
            session.setAttribute("memberships", memberships);
            request.getRequestDispatcher("View/Groups.jsp").forward(request, response);
        }
        
        else if(request.getParameter("type").equalsIgnoreCase("groupPage")){
             String groupName = request.getParameter("groupName");
            
            Group group = new Group(groupName);
            /* Gets users weight goal information to be displayed on the page */
            ArrayList<WeightGoal> weightGoals = 
                    WeightGoal.getWeightGoalsForGroup(groupName);
            session.setAttribute("weightGoals", weightGoals);

            /* Gets users distance goal information to be displayed on the page */
            ArrayList<DistanceGoal> distanceGoals =
                    DistanceGoal.getDistanceGoalsForGroup( groupName);
            session.setAttribute("distanceGoals", distanceGoals);

            /* Gets users fitness goal information to be displayed on the page */
            ArrayList<FitnessGoal> fitnessGoals =
                    FitnessGoal.getFitnessGoalsForGroup(groupName);
            session.setAttribute("fitnessGoals", fitnessGoals);

            /* Gets users custom goal information to be displayed on the page */
            ArrayList<CustomGoal> customGoals =
                    CustomGoal.getCustomGoalsForGroup(groupName);
            session.setAttribute("customGoals", customGoals);
            
            ArrayList<History> historyList = group.getMembersHistory();
            ArrayList<Comment> commentList = Comment.getGroupComments(groupName);
            
            /* Get the users membership details */
            Membership mem = Membership.getUserMembershipForGroup(user.getUsername(), groupName);
            session.setAttribute("membership", mem);
            
            session.setAttribute("historyList", historyList);
            session.setAttribute("commentList", commentList);
            session.setAttribute("aGroup", group);
            request.getRequestDispatcher("View/GroupPage.jsp").forward(request, response);    
        }
        
        else if(request.getParameter("type").equalsIgnoreCase("comment")){
             
            String text = request.getParameter("text");
            
            /* if a user presses the comment button without typing anything */
            if(text.equalsIgnoreCase("")){
                request.getRequestDispatcher("View/GroupPage.jsp").forward(request, response);
            }
            
            Comment comment = new Comment(user.getUsername(), text, aGroup.getGroupName()); 
            
            if(comment.persist()){
                String url = "/GroupController?type=groupPage&groupName="+aGroup.getGroupName();
                request.getRequestDispatcher(url).forward(request, response);
            }
            else{
                String errorMessage = "Commenting error please make sure you have entered a valid comment.";
                    session.setAttribute("errorMessage", errorMessage);
                    request.getRequestDispatcher("View/Error.jsp").forward(request, response);     
            }
            
            
        }
        
        /* New member clicking on accept invitation link */
        else{
            String groupName = request.getParameter("group");
            String r = request.getParameter("role");
            int p = Integer.parseInt(request.getParameter("permissions"));
            
            Role role = Role.U;
            if(r.equalsIgnoreCase("A")){
                role = Role.A;
            }
                   
            Group group = new Group(groupName);
            
            if(group.joinGroup(user.getUsername(), role, p)){
                // redirect to appropriate feedback page here
                request.getRequestDispatcher("View/UserPage.jsp").forward(request, response);
            }
            else{
                // error msg
                String errorMessage = "Group acceptance error, please try again or maybe you've already joined the group.";
                    session.setAttribute("errorMessage", errorMessage);
                    request.getRequestDispatcher("View/Error.jsp").forward(request, response); 
            }
            
            
            
        }
        
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet GroupController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet GroupController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(GroupController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(GroupController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
