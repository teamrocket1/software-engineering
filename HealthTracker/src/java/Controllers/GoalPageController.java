/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controllers;

import Model.CustomGoal;
import Model.DistanceGoal;
import Model.FitnessGoal;
import Model.Goal;
import Model.History;
import Model.Membership;
import Model.User;
import Model.WeightGoal;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Odie
 */
public class GoalPageController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        HttpSession session = request.getSession();
        //User u = (User) session.getAttribute("aUser");
        
        // Get the goal name and goal owner of the goal to be displayed
        String goalOwner = request.getParameter("goalOwner");
        String goalName = request.getParameter("goalName");
        //String goalType = request.getParameter("goalType");
        FitnessGoal fg = new FitnessGoal(goalOwner, goalName);
        WeightGoal wg = new WeightGoal(goalOwner, goalName);
        DistanceGoal dg = new DistanceGoal(goalOwner, goalName);
        CustomGoal cg = new CustomGoal(goalOwner, goalName);
        
       /*
        * getGoalDetails() will only execute succesfully and return true if
        * the goal is of the correct type. ie only one correct answer so only
        * one kind of goal will be made (which makes sense since we're trying to
         get info for that one goal.)
        *
        * WHich type it is, we dont know. ALl we know is one will definitely
        * be made and we will refer to it as "goal" in the jsp view.
        */
        
        // if goal being requested is fitness type
        if(fg.getGoalDetails()){
            session.setAttribute("goal", fg);
        }
        
        // if goal being requested is weight type
        else if(wg.getGoalDetails()){
            session.setAttribute("goal", wg);
        }
        
        // if goal being requested is distance type
        else if(dg.getGoalDetails()){
            session.setAttribute("goal", dg);
        }
        
        // if goal being requested is custom type
        else if(cg.getGoalDetails()){
            session.setAttribute("goal", cg);
        }
        
        
        
        /* Get more stuff if u want and add to session object here */
        
        request.getRequestDispatcher("View/GoalPage.jsp").forward(request, response);
        
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet GoalPageController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet GoalPageController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
