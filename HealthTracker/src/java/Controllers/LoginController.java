/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Model.User;
import Security.PasswordHash;
import Utilities.DBAccess;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Odie
 */
public class LoginController extends HttpServlet {

    private Integer loginAttempts = 0;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        /* Creating a session object and setting the time before it is invalid  */
        HttpSession session = request.getSession(true);
        session.setMaxInactiveInterval(3600000);
        //A session to keep track of log in attempts
        HttpSession loginSession = request.getSession(true);
        loginSession.setMaxInactiveInterval(300);

        String msg = "";

        if (request.getParameter("type").equalsIgnoreCase("login")) {
            //This keeps track of the number of inccorect login attemps
            if (loginSession.getAttribute("loginAttempts") != null) {
                loginAttempts = (Integer) loginSession.getAttribute("loginAttempts");
            }
//                if(loginAttempts == null) {
//                    loginAttempts = 0;
//                }
            if (loginAttempts < 4) {
                String username = request.getParameter("username");
                String password = request.getParameter("password");

                User user = new User();

                if (user.login(username, password)) {
                    session.setAttribute("aUser", user);
                    session.setAttribute("usernameFromLogin", user.getUsername());
                    request.getRequestDispatcher("/ProfilePageController").forward(request, response);
                } else {
                    loginAttempts += 1;
                    loginSession.setAttribute("loginAttemps", loginAttempts);
                    //Redirect to error page stating incorrect login details
                    String errorMessage = "Wrong login credentials.";
                    session.setAttribute("errorMessage", errorMessage);
                    request.getRequestDispatcher("View/Error.jsp").forward(request, response);

                    // incorect credentials. error page
                }
            } else {
                //redirect to a page that does not let them log in and 
                //advices to try again in 5 minutes
                request.getRequestDispatcher("View/HoldPage.jsp").forward(request, response);
            }

            /*
             * "aUser" is the identifier we will use to reference the object "user"
             * created above along with its member variables and functions 
             * in subsequent web pages and controllers.
             */
        } else if (request.getParameter("type").equalsIgnoreCase("signUp")) {
            String fName = request.getParameter("firstname");
            String lName = request.getParameter("lastname");
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String email = request.getParameter("email");
            String sex = request.getParameter("sex");
            String weightUnit = request.getParameter("weightUnit");
            int a = Integer.parseInt(request.getParameter("age"));
            int w = Integer.parseInt(request.getParameter("weight"));
            int h = Integer.parseInt(request.getParameter("height"));
            int heartrate = Integer.parseInt(request.getParameter("heartrate"));

            if (weightUnit.equalsIgnoreCase("lbs")) {
                double wInKg = (double) w / 2.2;
                w = (int) wInKg;
            }

            String hashPassword = "";
            try {
                hashPassword = PasswordHash.createHash(password);
            } catch (Exception ex) {
                System.out.println("Error hashing password on sign up.");
                Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            }

            User user = new User(fName, lName, username, hashPassword, email, sex, a, w, h, heartrate);

            if (user.signUp()) {
                msg = "Hello " + user.getUsername();
                /*
                 * "aUser" is the identifier we will use to reference the object "user"
                 * created above along with its member variables and functions 
                 * in subsequent web pages and controllers.
                 */
                session.setAttribute("aUser", user);
                request.getRequestDispatcher("/ProfilePageController").forward(request, response);

            } else {
                // user already exists. error page
                 String errorMessage = "This username is already taken please try another name please.";
                    session.setAttribute("errorMessage", errorMessage);
                    request.getRequestDispatcher("View/Error.jsp").forward(request, response);
            }


        } else if (request.getParameter("type").equalsIgnoreCase("logout")) {
            HttpSession existingSession = request.getSession();
            session.invalidate();
            response.sendRedirect("index.jsp");
        }

        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LoginController at " + request.getContextPath() + "</h1>");
            out.println(msg);
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
