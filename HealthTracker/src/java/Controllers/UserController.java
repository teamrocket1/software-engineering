/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Model.DistanceGoal;
import Model.User;
import Model.WeightGoal;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author James
 */
public class UserController extends HttpServlet {
    
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("aUser");
        String msg = "";
        
        
        /* Called if user clicks on Update info link*/
        if(request.getParameter("type").equalsIgnoreCase("UpdateProfile")){
            
            User backupUser = user;            
            try {
        String fName = request.getParameter("firstname");
        user.setFirstName(fName);
            String lName = request.getParameter("lastname");
            user.setLastName(lName);
            String password = request.getParameter("password");
            user.setPassword(password);
            String email = request.getParameter("email");
            user.setEmail(email);
            String sex = request.getParameter("sex");
            user.setSex(sex);
            int w = Integer.parseInt(request.getParameter("weight"));
            String weightUnit = request.getParameter("weightUnit");
             if (weightUnit.equalsIgnoreCase("lbs")) {
                double wInKg = (double) w / 2.2;
                w = (int) wInKg;
            }
             user.setWeight(w);
            int a = Integer.parseInt(request.getParameter("age"));
            user.setAge(a);
            int h = Integer.parseInt(request.getParameter("height"));
            user.setHeight(h);
            int heartrate = Integer.parseInt(request.getParameter("heartrate"));
            user.setHeartRate(heartrate);
            
            user.updateInfo();

            } catch (Exception e) {
                user = backupUser;
                 String errorMessage = "Edit Profile error please make sure your details are valid.";
                    session.setAttribute("errorMessage", errorMessage);
                    request.getRequestDispatcher("View/Error.jsp").forward(request, response);
            }

           // User user = new User(fName, lName, username, password, email, sex, a, w, h, heartrate);


            /*
             * "aUser" is the identifier we will use to reference the object "user"
             * created above along with its member variables and functions 
             * in subsequent web pages and controllers.
             */
            session.setAttribute("aUser", user);
            request.getRequestDispatcher("View/UserPage.jsp").forward(request, response);
        } else{
                    request.getRequestDispatcher("View/EditProfile.jsp").forward(request, response);            
        }
        
        
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet GoalController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet GoalController at " + request.getContextPath() + msg + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }
    
        // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
