/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Model.CustomGoal;
import Model.DistanceGoal;
import Model.User;
import Model.WeightGoal;
import Model.FitnessGoal;
import Model.Goal;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Odie
 */
public class GroupGoalController extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("aUser");
        String msg = "";


        /* Called if user clicks on Goals link*/
        if (request.getParameter("type").equalsIgnoreCase("clicked")) {

            /* Gets users weight goal information to be displayed on the page */
            ArrayList<WeightGoal> weightGoals = WeightGoal.getWeightGoalsForUser(user.getUsername());
            session.setAttribute("weightGoals", weightGoals);

            /* Gets users distance goal information to be displayed on the page */
            ArrayList<DistanceGoal> distanceGoals = DistanceGoal.getDistanceGoalsForUser(user.getUsername());
            session.setAttribute("distanceGoals", distanceGoals);

            /* Gets users fitness goal information to be displayed on the page */
            ArrayList<FitnessGoal> fitnessGoals = FitnessGoal.getFitnessGoalsForUser(user.getUsername());
            session.setAttribute("fitnessGoals", fitnessGoals);

            /* Gets users fitness goal information to be displayed on the page */
            ArrayList<CustomGoal> customGoals = CustomGoal.getCustomGoalsForUser(user.getUsername());
            session.setAttribute("customGoals", customGoals);

            request.getRequestDispatcher("View/Goals.jsp").forward(request, response);

        } else if (request.getParameter("type").equalsIgnoreCase("clickedGroup")) {

            String groupName = request.getParameter("groupName");

            /* Gets users weight goal information to be displayed on the page */
            ArrayList<WeightGoal> weightGoals =
                    WeightGoal.getWeightGoalsForGroup(groupName);
            session.setAttribute("weightGoals", weightGoals);

            /* Gets users distance goal information to be displayed on the page */
            ArrayList<DistanceGoal> distanceGoals =
                    DistanceGoal.getDistanceGoalsForGroup(groupName);
            session.setAttribute("distanceGoals", distanceGoals);

            /* Gets users fitness goal information to be displayed on the page */
            ArrayList<FitnessGoal> fitnessGoals =
                    FitnessGoal.getFitnessGoalsForGroup(groupName);
            session.setAttribute("fitnessGoals", fitnessGoals);

            /* Gets users custom goal information to be displayed on the page */
            ArrayList<CustomGoal> customGoals =
                    CustomGoal.getCustomGoalsForGroup(groupName);
            session.setAttribute("customGoals", customGoals);

            request.getRequestDispatcher("View/GroupGoals.jsp?groupName=" + groupName).forward(request, response);
            //session.setAttribute("groupName", groupName);

        } else if (request.getParameter("type").equalsIgnoreCase("clickedGroupGoal")) {
            String goalOwner = request.getParameter("goalOwner");
            String goalName = request.getParameter("goalName");
            //String goalType = request.getParameter("goalType");
            FitnessGoal fg = new FitnessGoal(goalOwner, goalName);
            fg.setIsGroup(1);
            WeightGoal wg = new WeightGoal(goalOwner, goalName);
            wg.setIsGroup(1);
            DistanceGoal dg = new DistanceGoal(goalOwner, goalName);
            dg.setIsGroup(1);
            CustomGoal cg = new CustomGoal(goalOwner, goalName);
            cg.setIsGroup(1);
            /*
             * getGoalDetails() will only execute succesfully and return true if
             * the goal is of the correct type. ie only one correct answer so only
             * one kind of goal will be made (which makes sense since we're trying to
             get info for that one goal.)
             *
             * WHich type it is, we dont know. ALl we know is one will definitely
             * be made and we will refer to it as "goal" in the jsp view.
             */

            // if goal being requested is fitness type
            if (fg.getGoalDetailsForGroup()) {
                session.setAttribute("goal", fg);
            } // if goal being requested is weight type
            else if (wg.getGoalDetailsForGroup()) {
                session.setAttribute("goal", wg);
            } // if goal being requested is distance type
            else if (dg.getGoalDetailsForGroup()) {
                session.setAttribute("goal", dg);
            } // if goal being requested is custom type
            else if (cg.getGoalDetailsForGroup()) {
                session.setAttribute("goal", cg);
            } else {
                String errorMessage = "Undefined goal type please try again.";
                session.setAttribute("errorMessage", errorMessage);
                request.getRequestDispatcher("View/Error.jsp").forward(request, response);
            }



            /* Get more stuff if u want and add to session object here */

            request.getRequestDispatcher("View/GroupGoalPage.jsp").forward(request, response);

        } else if (request.getParameter("type").equalsIgnoreCase("delete")) {
            String groupName = request.getParameter("groupName");
            String[] goalNames = request.getParameterValues("delete");
            for (int i = 0; i < goalNames.length; i++) {
                Goal.deleteGoal(groupName, goalNames[i], 1);
            }
            request.getRequestDispatcher("/GroupGoalController?type=clickedGroup").forward(request, response);
        } /**
         * @ START of modifying goals*
         */
        else if (request.getParameter("type").equalsIgnoreCase("modifyWeightGoal")) {
            String goalName = request.getParameter("goalName");
            String goalOwner = request.getParameter("goalOwner");
            int targetWeight = Integer.parseInt(request.getParameter("targetWeight"));
            int day = Integer.parseInt(request.getParameter("day_weight"));
            int month = Integer.parseInt(request.getParameter("month_weight"));
            int year = Integer.parseInt(request.getParameter("year_weight"));
            Date dueDate = new Date(year - 1900, (month - 1), day);
            Date startDate = Date.valueOf(request.getParameter("startDate"));

            WeightGoal goal = new WeightGoal(goalName, startDate, dueDate, targetWeight);
            goal.setIsGroup(1);
            if (goal.updateGoal(goalOwner)) {
                request.getRequestDispatcher("/GroupGoalController?type=clickedGroup&groupName=" + goalOwner).forward(request, response);
            } else {
                // redirect to appropriate error page here
                String errorMessage = "Weight goals fields error, make sure they are valid.";
                session.setAttribute("errorMessage", errorMessage);
                request.getRequestDispatcher("View/Error.jsp").forward(request, response);
            }

        } else if (request.getParameter("type").equalsIgnoreCase("modifyDistanceGoal")) {
            int targetDistance = Integer.parseInt(request.getParameter("targetDistance_distance"));
            int day = Integer.parseInt(request.getParameter("day"));
            int month = Integer.parseInt(request.getParameter("month"));
            int year = Integer.parseInt(request.getParameter("year"));
            Date dueDate = new Date(year - 1900, (month - 1), day);
            String goalName = request.getParameter("goalName");
            String goalOwner = request.getParameter("goalOwner");
            String activityType = request.getParameter("activityType");

            String unit = request.getParameter("distanceDistanceUnit");
            Date startDate = Date.valueOf(request.getParameter("startDate"));

            // db stores distance in km. If user enters miles convert.
            if (unit.equalsIgnoreCase("mi")) {
                double d = targetDistance;
                targetDistance = (int) (d * 1.6);
            }

            DistanceGoal goal = new DistanceGoal(targetDistance, startDate, dueDate, goalName, activityType);
            goal.setIsGroup(1);
            if (goal.updateGoal(goalOwner)) {
                request.getRequestDispatcher("/GroupGoalController?type=clickedGroup&groupName=" + goalOwner).forward(request, response);
            } else {
                // redirect to appropriate error page here
                String errorMessage = "Distance goals fields error, make sure they are valid.";
                session.setAttribute("errorMessage", errorMessage);
                request.getRequestDispatcher("View/Error.jsp").forward(request, response);
            }
        } else if (request.getParameter("type").equalsIgnoreCase("modifyFitnessGoal")) {
            String goalName = request.getParameter("goalName");
            String goalOwner = request.getParameter("goalOwner");
            int targetHeartRate = Integer.parseInt(request.getParameter("targetHeartRate"));
            int day = Integer.parseInt(request.getParameter("day"));
            int month = Integer.parseInt(request.getParameter("month"));
            int year = Integer.parseInt(request.getParameter("year"));
            Date dueDate = new Date(year - 1900, (month - 1), day);

            Date startDate = Date.valueOf(request.getParameter("startDate"));
            FitnessGoal goal = new FitnessGoal(goalName, startDate, dueDate, targetHeartRate);
            goal.setIsGroup(1);
            if (goal.updateGoal(goalOwner)) {
                // redirect to appropriate feedback page here
                request.getRequestDispatcher("/GroupGoalController?type=clickedGroup&groupName=" + goalOwner).forward(request, response);
            } else {
                // redirect to appropriate error page here
                String errorMessage = "Fitness goals fields error, make sure they are valid.";
                session.setAttribute("errorMessage", errorMessage);
                request.getRequestDispatcher("View/Error.jsp").forward(request, response);
            }

        } else if (request.getParameter("type").equalsIgnoreCase("modifyCustomGoal")) {
            String goalName = request.getParameter("goalName");
            String goalOwner = request.getParameter("goalOwner");
            int targetWeight = Integer.parseInt(request.getParameter("targetWeight"));
            int targetHeartRate = Integer.parseInt(request.getParameter("targetHeartRate"));
            int targetDistance = Integer.parseInt(request.getParameter("targetDistance"));
            int day = Integer.parseInt(request.getParameter("day"));
            int month = Integer.parseInt(request.getParameter("month"));
            int year = Integer.parseInt(request.getParameter("year"));
            Date dueDate = new Date(year - 1900, (month - 1), day);
            String activityType = request.getParameter("activityType");
            String unit = request.getParameter("customDistanceUnit");

            int targetBMI = 0; // not supported for now

            // db stores distance in km. If user enters miles convert.
            if (unit.equalsIgnoreCase("mi")) {
                double d = targetDistance;
                targetDistance = (int) (d * 1.6);
            }

            Date startDate = Date.valueOf(request.getParameter("startDate"));

            CustomGoal goal = new CustomGoal(goalName, startDate, dueDate, targetWeight, targetDistance, targetHeartRate, targetBMI, activityType);
            goal.setIsGroup(1);
            if (goal.updateGoal(goalOwner)) {
                // redirect to appropriate feedback page here
                request.getRequestDispatcher("/GroupGoalController?type=clickedGroup&groupName=" + goalOwner).forward(request, response);
            } else {
                // redirect to appropriate error page here
                String errorMessage = "Custom goals fields error, make sure they are valid.";
                session.setAttribute("errorMessage", errorMessage);
                request.getRequestDispatcher("View/Error.jsp").forward(request, response);
            }

        } /**
         * @ end of modifying goals*
         */
        /* If user creates weight goal */ else if (request.getParameter("type1").equalsIgnoreCase("set")) {
            String groupName = request.getParameter("groupName");
            String goalName = request.getParameter("goalName");
            int targetWeight = Integer.parseInt(request.getParameter("targetWeight"));
            int day = Integer.parseInt(request.getParameter("day_weight"));
            int month = Integer.parseInt(request.getParameter("month_weight"));
            int year = Integer.parseInt(request.getParameter("year_weight"));
            Date dueDate = new Date(year - 1900, (month - 1), day);

            java.util.Date utilDate = new java.util.Date();
            java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

            WeightGoal goal = new WeightGoal(goalName, currentDate, dueDate, targetWeight);
            goal.setIsGroup(1);

            if (goal.persist(groupName)) {
                // redirect to appropriate feedback page here
                request.getRequestDispatcher("GroupGoalController?type=clickedGroup").forward(request, response);
            } else {
                // redirect to appropriate error page here
                String errorMessage = "Weight goals fields error, make sure they are valid.";
                session.setAttribute("errorMessage", errorMessage);
                request.getRequestDispatcher("View/Error.jsp").forward(request, response);
            }



        } /* If user creates fitness goal */ else if (request.getParameter("type2").equalsIgnoreCase("set")) {
            String groupName = request.getParameter("groupName");
            String goalName = request.getParameter("goalName_fitness");
            int targetHeartRate = Integer.parseInt(request.getParameter("targetHeartRate"));
            int day = Integer.parseInt(request.getParameter("day_fitness"));
            int month = Integer.parseInt(request.getParameter("month_fitness"));
            int year = Integer.parseInt(request.getParameter("year_fitness"));
            Date dueDate = new Date(year - 1900, (month - 1), day);

            java.util.Date utilDate = new java.util.Date();
            java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

            FitnessGoal goal = new FitnessGoal(goalName, currentDate, dueDate, targetHeartRate);
            goal.setIsGroup(1);

            if (goal.persist(groupName)) {
                // redirect to appropriate feedback page here
                request.getRequestDispatcher("GroupGoalController?type=clickedGroup").forward(request, response);
            } else {
                // redirect to appropriate error page here
                String errorMessage = "Fitness goals fields error, make sure they are valid.";
                session.setAttribute("errorMessage", errorMessage);
                request.getRequestDispatcher("View/Error.jsp").forward(request, response);
            }

        } /* If user creates custom goal */ else if (request.getParameter("type3").equalsIgnoreCase("set")) {
            String groupName = request.getParameter("groupName");
            String goalName = request.getParameter("goalName_custom");
            int targetWeight = Integer.parseInt(request.getParameter("targetWeight_custom"));
            int targetHeartRate = Integer.parseInt(request.getParameter("targetHeartRate_custom"));
            int targetDistance = Integer.parseInt(request.getParameter("targetDistance_custom"));
            int day = Integer.parseInt(request.getParameter("day_custom"));
            int month = Integer.parseInt(request.getParameter("month_custom"));
            int year = Integer.parseInt(request.getParameter("year_custom"));
            Date dueDate = new Date(year - 1900, (month - 1), day);
            String activityType = request.getParameter("activityType_custom");
            String unit = request.getParameter("customDistanceUnit");

            int targetBMI = 0; // not supported for now

            // db stores distance in km. If user enters miles convert.
            if (unit.equalsIgnoreCase("mi")) {
                double d = targetDistance;
                targetDistance = (int) (d * 1.6);
            }

            java.util.Date utilDate = new java.util.Date();
            java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

            CustomGoal goal = new CustomGoal(goalName, currentDate, dueDate, targetWeight, targetDistance, targetHeartRate, targetBMI, activityType);
            goal.setIsGroup(1);

            if (goal.persist(groupName)) {
                // redirect to appropriate feedback page here
                request.getRequestDispatcher("GroupGoalController?type=clickedGroup").forward(request, response);
            } else {
                // redirect to appropriate error page here
                String errorMessage = "Custom goals fields error, make sure they are valid.";
                session.setAttribute("errorMessage", errorMessage);
                request.getRequestDispatcher("View/Error.jsp").forward(request, response);
            }


        } /* If user creates distance goal */ else if (request.getParameter("type4").equalsIgnoreCase("set")) {
            String groupName = request.getParameter("groupName");
            int targetDistance = Integer.parseInt(request.getParameter("targetDistance_distance"));
            int day = Integer.parseInt(request.getParameter("day_distance"));
            int month = Integer.parseInt(request.getParameter("month_distance"));
            int year = Integer.parseInt(request.getParameter("year_distance"));
            Date dueDate = new Date(year - 1900, (month - 1), day);
            String goalName = request.getParameter("goalName_distance");
            String activityType = request.getParameter("activityType");

            String unit = request.getParameter("distanceDistanceUnit");

            // db stores distance in km. If user enters miles convert.
            if (unit.equalsIgnoreCase("mi")) {
                double d = targetDistance;
                targetDistance = (int) (d * 1.6);
            }

            java.util.Date utilDate = new java.util.Date();
            java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

            DistanceGoal goal = new DistanceGoal(targetDistance, currentDate, dueDate, goalName, activityType);
            goal.setIsGroup(1);


            if (goal.persist(groupName)) {
                // redirect to appropriate feedback page here
                request.getRequestDispatcher("GroupGoalController?type=clickedGroup").forward(request, response);
            } else {
                // redirect to appropriate error page here
                String errorMessage = "Distance goals fields error, make sure they are valid.";
                session.setAttribute("errorMessage", errorMessage);
                request.getRequestDispatcher("View/Error.jsp").forward(request, response);
            }




            try {
                /* TODO output your page here. You may use following sample code. */
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Servlet GoalController</title>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>Servlet GoalController at " + request.getContextPath() + msg + "</h1>");
                out.println("</body>");
                out.println("</html>");
            } finally {
                out.close();
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
