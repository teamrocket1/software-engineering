/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Model.User;
import Utilities.DBAccess;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Odie
 */
public class MessageController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("aUser");

        String type = (String) request.getParameter("type");

        /* Messaging function. This part handles the very first messsge sent between two users. */
        if (type.equalsIgnoreCase("firstMessage")) {
            String recipient = request.getParameter("recipient");
            String msg = request.getParameter("msg");
            String sender = request.getParameter("sender");
            String firstSender = request.getParameter("firstSender");
            String party = sender + " " + recipient;

            /* Initializing the "stack" to zero.
             * 
             * The "stack" is a number assigned serially to each message concerning
             * a party(a party is a sender and a recepient).
             * 
             * The stack is used to arrange messages and display them in the right order
             */
            int stack = 0;

            /* The message is added to the database along with other details concerning it.
             * 
             * An int variable "sent" is created and if the message is sent the value
             * of sent is 1 and if an error occurs, its value is 2.
             * 
             * This makes it possible to carry out some error handling depeding on the
             * value of "sent".
             */
            try {
                Connection con = DBAccess.getConnection();
                Statement statement = con.createStatement();
                statement.executeUpdate("INSERT INTO HealthTrackerMessages VALUES('" + sender + "', '" + recipient + "', '" + msg + "', '" + party + "', " + 1 + ", 0, 1 )");

                Integer sent = 1;
                session.setAttribute("sent", sent);

                request.getRequestDispatcher("View/NewMessages.jsp").forward(request, response);
            } catch (Exception e) {
                Integer sent = 2;
                session.setAttribute("sent", sent);
                request.getRequestDispatcher("View/NewMessages.jsp").forward(request, response);
            }
        }

        /* Messaging function. This part handles the actual sending of messages
         * back and forth between users.
         */
        if (type.equalsIgnoreCase("postMessage")) {
            String recipient = request.getParameter("recipient");
            String msg = request.getParameter("msg");
            String sender = request.getParameter("sender");
            String firstSender = request.getParameter("firstSender");
            String party = request.getParameter("party");

            /* Initializing the "stack" to zero.
             * 
             * The "stack" is a number assigned serially to each message concerning
             * a party(a party is a sender and a recepient).
             * 
             * The stack is used to arrange messages and display them in the right order
             */
            int stack = 0;

            /*
             * First the tag field of all the existing messages between this party is set to 0.
             * This signifies that the messages have been read.
             * 
             * Next the stack value to be assigned to this message is obtained by looping through
             * the existing messages and adding 1 each time.
             * 
             * The message is then added to the database along with  the other details concerning it.
             */
            try {
                Connection con = DBAccess.getConnection();
                Statement statement = con.createStatement();
                statement.executeUpdate("UPDATE HealthTrackerMessages SET tag = 0 WHERE party = '" + party + "' AND tag = 1");

                ResultSet r = statement.executeQuery("SELECT * from HealthTrackerMessages where party = '" + party + "'");
                while (r.next()) {
                    int x = r.getInt("stack");
                    if (stack < x) {
                        stack = x;
                    }
                }
                statement.executeUpdate("INSERT INTO HealthTrackerMessages VALUES('" + sender + "', '" + recipient + "', '" + msg + "', '" + party + "', " + (stack + 1) + ", 0, 1 )");

                request.getRequestDispatcher("MessageController?type=viewConvo&sender="+recipient).forward(request, response);
                //response.sendRedirect("conversation.jsp?sender=" + firstSender + "");
            } catch (Exception e) {
                throw new ServletException("message problem", e);
            }
        }

        if (type.equalsIgnoreCase("viewMessages")) {

            ArrayList<String> senders = new ArrayList<String>();
            ArrayList<String> messages = new ArrayList<String>();

            try {
                Connection con = DBAccess.getConnection();
//                Statement statementX = con.createStatement();
//                ResultSet rsX = statementX.executeQuery("SELECT * FROM HealthTrackerMessages WHERE read = 0 AND recepient = '" + user.getUsername() + "'");
//                Integer readCount = 0;
//                while (rsX.next()) {
//                    readCount++;
//                }

                int x = 0;
                String appeared = "";

                Statement statement = con.createStatement();
                Statement statement2 = con.createStatement();
                ResultSet rs1 = statement.executeQuery("SELECT username from user_data");

                while (rs1.next()) {
                    String sender = rs1.getString("username");

                    ResultSet rs2 = statement2.executeQuery("SELECT * from HealthTrackerMessages WHERE sender = '" + sender + "' AND recepient = '" + user.getUsername() + "'  AND tag = 1");
                    boolean senderAdded = false;
                    while (rs2.next()) {
                        x++;
                        appeared = sender;
                        if (!senderAdded) {
                            senders.add(sender);
                            senderAdded = true;
                            messages.add(rs2.getString("message"));
                        }

                    }
                    rs2.close();

                    Statement statement3 = con.createStatement();
                    ResultSet rs3 = statement3.executeQuery("SELECT * from HealthTrackerMessages WHERE sender = '" + user.getUsername() + "' AND recepient = '" + sender + "'  AND tag = 1");
                    while (rs3.next()) {
                        //if (!appeared.equalsIgnoreCase(sender)) {
                            senders.add(sender);
                            messages.add(rs3.getString("message"));
                       // }
                    }
                }
                rs1.close();
                session.setAttribute("messages", messages);
                session.setAttribute("senders", senders);
                request.getRequestDispatcher("View/Messages.jsp").forward(request, response);
            } catch (Exception e) {
                    String errorMessage = "View Message error please try again.";
                    session.setAttribute("errorMessage", errorMessage);
                    request.getRequestDispatcher("View/Error.jsp").forward(request, response);            }

        }

        if (type.equalsIgnoreCase("viewConvo")) {

            ArrayList<String> sentMessages = new ArrayList<String>();
            ArrayList<String> receivedMessages = new ArrayList<String>();
            ArrayList<String> ordering = new ArrayList<String>();
            ArrayList<String> messages = new ArrayList<String>();
            
            try {

                String sender = request.getParameter("sender");

                Connection con = DBAccess.getConnection();

                String party = null;
                /* String r = newUser.getUsername();
                 int x = r.compareTo(sender);
                 if(x < 1) {
                 party1 = sender + " " + newUser.getUsername();
                 }
                 r.
                    
                 String party2 = newUser.getUsername() + " " + sender; */
                String sql = "SELECT party from HealthTrackerMessages WHERE sender = '" + sender + "' AND recepient = '" + user.getUsername() + "'";
                //Connection con = DBAccess.getConnection();
                Statement statementX = con.createStatement();

                Statement statement = con.createStatement();
                Statement statement2 = con.createStatement();
                ResultSet rs2 = statement2.executeQuery(sql);
                while (rs2.next()) {
                    party = rs2.getString("party");
                }

                statementX.executeUpdate("UPDATE HealthTrackerMessages SET read = 1 WHERE party = '" + party + "' AND recepient ='" + user.getUsername() + "'");

                ResultSet rs1 = statement.executeQuery("SELECT * from HealthTrackerMessages WHERE party = '" + party + "' ORDER BY stack ASC");

                while (rs1.next()) {

                    messages.add(rs1.getString("message"));
                    if (rs1.getString("sender").equalsIgnoreCase(user.getUsername())){
                        //sentMessages.add(rs1.getString("message"));
                        ordering.add("sent");
                    }
                    
                    else if (rs1.getString("recepient").equalsIgnoreCase(user.getUsername())){
                        //receivedMessages.add(rs1.getString("message"));
                        ordering.add("received");
                    }
                    
                }
                
                
                session.setAttribute("messages", messages);
                session.setAttribute("ordering", ordering);
                
                //session.setAttribute("sent", sentMessages);
                //session.setAttribute("received", receivedMessages);
                request.getRequestDispatcher("View/Conversation.jsp?sender="+sender+"&party="+party).forward(request, response);
            } catch (Exception e) {
                    String errorMessage = "Viewing of conversation error please try again.";
                    session.setAttribute("errorMessage", errorMessage);
                    request.getRequestDispatcher("View/Error.jsp").forward(request, response);            }

        }
        
        if(type.equalsIgnoreCase("refreshAfterPost")){
            
            ArrayList<String> sentMessages = new ArrayList<String>();
            ArrayList<String> receivedMessages = new ArrayList<String>();
            ArrayList<String> ordering = new ArrayList<String>();
            ArrayList<String> messages = new ArrayList<String>();
            
            try {

                String sender = request.getParameter("sender");

                Connection con = DBAccess.getConnection();

                String party = null;
                /* String r = newUser.getUsername();
                 int x = r.compareTo(sender);
                 if(x < 1) {
                 party1 = sender + " " + newUser.getUsername();
                 }
                 r.
                    
                 String party2 = newUser.getUsername() + " " + sender; */
                String sql = "SELECT party from HealthTrackerMessages WHERE sender = '" + user.getUsername() + "' AND recepient = '" + sender + "'";
                //Connection con = DBAccess.getConnection();
                Statement statementX = con.createStatement();

                Statement statement = con.createStatement();
                Statement statement2 = con.createStatement();
                ResultSet rs2 = statement2.executeQuery(sql);
                while (rs2.next()) {
                    party = rs2.getString("party");
                }

                statementX.executeUpdate("UPDATE HealthTrackerMessages SET read = 1 WHERE party = '" + party + "' AND recepient ='" + user.getUsername() + "'");

                ResultSet rs1 = statement.executeQuery("SELECT * from HealthTrackerMessages WHERE party = '" + party + "' ORDER BY stack ASC");

                while (rs1.next()) {

                    messages.add(rs1.getString("message"));
                    if (rs1.getString("sender").equalsIgnoreCase(user.getUsername())){
                        //sentMessages.add(rs1.getString("message"));
                        ordering.add("sent");
                    }
                    
                    else if (rs1.getString("recepient").equalsIgnoreCase(user.getUsername())){
                        //receivedMessages.add(rs1.getString("message"));
                        ordering.add("received");
                    }
                    
                }
                
                
                session.setAttribute("messages", messages);
                session.setAttribute("ordering", ordering);
                
                //session.setAttribute("sent", sentMessages);
                //session.setAttribute("received", receivedMessages);
                request.getRequestDispatcher("View/Conversation.jsp?sender="+sender+"&party="+party).forward(request, response);
            } catch (Exception e) {
                    String errorMessage = "Refresh error. Please try again";
                    session.setAttribute("errorMessage", errorMessage);
                    request.getRequestDispatcher("View/Error.jsp").forward(request, response);            }
            
        }

        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MessageController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MessageController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
