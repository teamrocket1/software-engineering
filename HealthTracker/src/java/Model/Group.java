package Model;

import Model.Membership.Role;
import Utilities.DBAccess;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Group {

    private String groupName;
    private ArrayList<User> members;
    private Goal groupGoal;

    // Creates a group using details from the db for a given group name.
    public Group(String name) {
        groupName = name;
        members = new ArrayList<User>();
        // Fill arraylist with db info
        // Create goal from db
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public ArrayList<User> getMembers() {
        return members;
    }

    public void setMembers(ArrayList<User> members) {
        this.members = members;
    }

    public Goal getGroupGoal() {
        return groupGoal;
    }

    public void setGroupGoal(Goal groupGoal) {
        this.groupGoal = groupGoal;
    }

    

    public boolean createGroup(String username) {

        try {
            Connection con = DBAccess.getConnection();

            Statement statement = con.createStatement();

            String sql = "INSERT INTO Groups VALUES('" + groupName + "', " + 1 + ")";

            statement.executeUpdate(sql);

            con.close();

            Membership membership = new Membership(username, groupName, Role.A, 5);
            membership.persist();

        } catch (Exception e) {
            return false;
        }

        return true;

    }

    public boolean joinGroup(String username, Role r, int permissions) {

        try {

            /* Cretaing new membership if user not already a member */
            Membership membership = new Membership(username, groupName, r, permissions);

            if (membership.persist()) {
                Connection con = DBAccess.getConnection();

                Statement statement = con.createStatement();

                String sql = "UPDATE Groups SET numberOfMembers = numberOfMembers + 1 WHERE groupName = '" + groupName + "'";

                statement.executeUpdate(sql);

                con.close();
            }

        } catch (Exception e) {
            return false;
        }

        return true;

    }
    
    
    public ArrayList<History> getMembersHistory() throws SQLException {

        ArrayList<Membership> memberships = Membership.getGroupsMemberships(groupName);
        ArrayList<History> list = new ArrayList<History>();

        
        Connection con = null;
        try {
            con = DBAccess.getConnection();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Group.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Group.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        for(Membership m : memberships){
         
            try {

            String sql = "SELECT * FROM History WHERE username = '" + m.getUsername() + "'";

            Statement statement = con.createStatement();

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                String text = rs.getString("text");
                Date date = rs.getDate("date");

                History h = new History(m.getUsername(), text, date);
                list.add(h);
            }



        } catch (Exception ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
            
        }
        
        con.close();

        return list;


    }
    
    
    

}
