package Model;

import Controllers.LoginController;
import Security.PasswordHash;
import Utilities.DBAccess;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;

public class User {

    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private String email;
    private String sex;
    private int age;
    private int weight;
    private int height;
    private int heartRate;
    private ArrayList<Goal> goals;

    // Default constructor sets all fields to null
    public User() {
    }

    public User(String f, String l, String u, String p, String e, String s, int a, int w, int h, int wh) {
        this.username = u;
        this.password = p;
        this.email = e;
        this.sex = s;
        this.age = a;
        this.firstName = f;
        this.lastName = l;
        this.height = h;
        this.weight = w;
        this.heartRate = wh;
    }
    
    public User(String u){
        username = u;
    }

    public boolean login(String u, String p) throws ServletException {

        boolean valid = false;
        String hashPassword;
        try {

            hashPassword = getHashPassword(u);
            if (!PasswordHash.validatePassword(p, hashPassword)) {
                return false;
            } else {

                Connection con = DBAccess.getConnection();
                PreparedStatement ps = con.prepareStatement("SELECT * FROM user_data "
                        + "WHERE username = ?");
                ps.setString(1, u);
                ResultSet rs = ps.executeQuery();

                /* 
                 * Checks the database to find a match with the credentials entered
                 * When a match is found, a User bean is made and attched to the session
                 */
                while (rs.next()) {
                    valid = true;
                    this.username = rs.getString("username");
                    this.password = rs.getString("password");
                    this.firstName = rs.getString("firstName");
                    this.lastName = rs.getString("lastName");
                    this.email = rs.getString("email");
                    this.sex = rs.getString("sex");
                    this.age = rs.getInt("age");
                    this.height = rs.getInt("height");
                    this.weight = rs.getInt("weight");
                    this.heartRate = rs.getInt("heartrate");

                }

                con.close();
            }
        } catch (Exception e) {
            return false;
        }

        return valid;
    }

    public String getHashPassword(String username) {
        String hash = "";
        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT password"
                    + " FROM user_data WHERE username = ?;");
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                hash = rs.getString(1);
            }
        } catch (Exception ex) {
            System.out.println("Error getting hashed password.");
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hash;
    }

    public boolean signUp() {

        try {
            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("INSERT INTO user_data"
                    + " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
            ps.setString(1, username);
            ps.setString(2, password);
            ps.setString(3, firstName);
            ps.setString(4, lastName);
            ps.setString(5, email);
            ps.setString(6, sex);
            ps.setInt(7, age);
            ps.setInt(8, weight);
            ps.setInt(9, height);
            ps.setInt(10, heartRate);

            ps.executeUpdate();

            con.close();

        } catch (Exception ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

        return true;

    }

    public boolean updateInfo() {

        java.util.Date utilDate = new java.util.Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
        
        try {
            String sql = "UPDATE User_Data SET password = '" + password
                    + "', firstname = '" + firstName
                    + "', lastname = '" + lastName + "', email = '" + email
                    + "', sex = '" + sex + "', age = " + age + ",weight = "
                    + weight + ", height = " + height + ", heartrate = "
                    + heartRate + " WHERE username = "
                    + "'" + username + "'";

            DBAccess.insertStatement(sql);
            
//            Connection con = DBAccess.getConnection();
//            Statement s = con.createStatement();
//            
//            ResultSet rs = s.executeQuery("SELECT * FROM Goal WHERE goalOwner = '" + username + "' AND isGroup = 0");
//            while(rs.next()){
//                if(rs.getInt("targetWeight") == weight){
//                    DBAccess.insertStatement("INSERT INTO Achievements VALUES")
//                }
//            }
            /* Updating user goal/achievemnt */
            ArrayList<WeightGoal> wgUser = WeightGoal.getWeightGoalsForUser(username);
            for(WeightGoal g : wgUser){
                if(g.getTargetWeight() == weight){
                    DBAccess.insertStatement("INSERT INTO Achievments VALUES('" + username + "', '" + g.getGoalName() + 
                            "', '" + sqlDate + "', 'D'," + 0 + "," + 0 + "," +
                            0 + "," + 0 + ",'NA'"
                            + ")");
                    DBAccess.insertStatement("DELETE FROM Goal WHERE goalName = '" + g.getGoalName() + "'" +
                            "AND goalOwner = '" + username + "'");
                    DBAccess.insertStatement("INSERT INTO History VALUES('" + username + "', 'Completed Goal " + g.getGoalName() + "', '"
                    + sqlDate + "')");
                }
            }
            
            /* Updating group goal/achievemnt */
            
            ArrayList<Membership> mems = Membership.getUsersMemberships(username);
            
            for (Membership m : mems) {
                
                ArrayList<WeightGoal> wgGroup = WeightGoal.getWeightGoalsForGroup(m.getGroupName());
                
                for (WeightGoal g : wgGroup) {
                    
                    ArrayList<Membership> mems2 = Membership.getGroupsMemberships(m.getGroupName());
                    int completionCount = 0;
                    for (Membership m2 : mems2) {

                        User user = new User(m2.getUsername());
                        user.getUserDetails();
                        if (user.weight == g.getTargetWeight()) {
                            completionCount++;
                        }

                    }
                    
                    
                    if(completionCount == mems2.size()){
                        DBAccess.insertStatement("INSERT INTO Achievments VALUES('" + m.getGroupName() + "', '" + g.getGoalName()
                                + "', '" + sqlDate + "', 'D'," + 0 + "," + 0 + ","
                                + 0 + "," + 0 + ",'NA'"
                                + ")");
                        DBAccess.insertStatement("DELETE FROM Goal WHERE goalName = '" + g.getGoalName() + "'"
                                + "AND goalOwner = '" + m.getGroupName() + "'");
                        DBAccess.insertStatement("INSERT INTO History VALUES('" + m.getGroupName() + "', 'Completed Goal " + g.getGoalName() + "', '"
                                + sqlDate + "')");
                    }
                        
                    
                    
                }
                
            }

        } catch (Exception e) {
            return false;
        }

        return true;

    }

    /**
     * This method is used to get fill a User object with its details from 
     * the database. 
     * 
     * It is used in displaying users' profile pages when we know only their
     * usernames and need to display more data.
     * 
     * @return 
     */
    public boolean getUserDetails() {

        boolean valid = false;
        String hashPassword;
        try {

            String firstName;
            String lastName;
            String password;
            String email;
            String sex;
            int age;
            int weight;
            int height;
            int heartRate;

            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("SELECT * FROM user_data "
                    + "WHERE username = ?");
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();

            /* 
             * Checks the database to find a match with the credentials entered
             * When a match is found, a User bean is made and attched to the session
             */
            while (rs.next()) {
                valid = true;
                this.password = rs.getString("password");
                this.firstName = rs.getString("firstName");
                this.lastName = rs.getString("lastName");
                this.email = rs.getString("email");
                this.sex = rs.getString("sex");
                this.age = rs.getInt("age");
                this.height = rs.getInt("height");
                this.weight = rs.getInt("weight");
                this.heartRate = rs.getInt("heartrate");

            }
            
            

            con.close();
        }
        
    catch (Exception ex) {
            return false;
    }

    return valid ;
}

public ArrayList<History> getUserHistory() {

        ArrayList<History> list = new ArrayList<History>();

        try {

            Connection con = DBAccess.getConnection();

            String sql = "SELECT * FROM History WHERE username = '" + username + "'";

            Statement statement = con.createStatement();

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                String text = rs.getString("text");
                Date date = rs.getDate("date");

                History h = new History(username, text, date);
                list.add(h);

            }
            
            ArrayList<Membership> memberships = Membership.getUsersMemberships(username);
           
            for(Membership m : memberships) {
                ResultSet rs2 = DBAccess.selectStatement("SELECT * FROM History WHERE username = '" + m.getGroupName() + "'");
                while(rs2.next()){
                    String text = rs2.getString("text");
                    Date date = rs2.getDate("date");
                
                    History h = new History(m.getGroupName(), text, date);
                    list.add(h);
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(User.class  

.getName()).log(Level.SEVERE, null, ex);
        }

        return list;


    }

    public double getBMI() {
        if (weight != 0 && height != 0) {
            return (weight / (height/100 * height/100));
        }
        return 0;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public ArrayList<Goal> getGoals() {
        return goals;
    }

    public void setGoals(ArrayList<Goal> goals) {
        this.goals = goals;
    }

    public void addGoal(Goal g) {
        goals.add(g);
    }

    public void removeGoal(Goal g) {
        goals.remove(g);
    }
}
