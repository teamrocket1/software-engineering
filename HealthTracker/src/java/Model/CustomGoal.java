package Model;

import Utilities.DBAccess;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class CustomGoal extends Goal {

    private int targetWeight;
    private int targetBMI;
    private int targetHeartRate;
    private int targetDistance;
    private int achievedDistance;
    private String activityType;
    private Exercise exersise;

    public CustomGoal(String goalName, Date currentDate, Date dueDate, int targetWeight, int targetDistance, int targetHeartRate, int targetBMI, String activityType) {
        super("", goalName);
        this.targetDistance = targetDistance;
        this.targetWeight = targetWeight;
        this.targetHeartRate = targetHeartRate;
        this.targetBMI = targetBMI;
        this.startDate = currentDate;
        this.dueDate = dueDate;
        status = false;
        this.activityType = activityType;
        achievedDistance = 0;
    }

    public CustomGoal(String owner, String gName) {
        super(owner, gName);
    }

    @Override
    boolean isCompleted() {
        // TODO Auto-generated method stub
        return false;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public int getTargetWeight() {
        return targetWeight;
    }

    public void setTargetWeight(int targetWeight) {
        this.targetWeight = targetWeight;
    }

    public int getTargetBMI() {
        return targetBMI;
    }

    public void setTargetBMI(int targetBMI) {
        this.targetBMI = targetBMI;
    }

    public int getTargetHeartRate() {
        return targetHeartRate;
    }

    public void setTargetHeartRate(int targetHeartRate) {
        this.targetHeartRate = targetHeartRate;
    }

    public int getTargetDistance() {
        return targetDistance;
    }

    public void setTargetDistance(int targetDistance) {
        this.targetDistance = targetDistance;
    }

    public int getAchievedDistance() {
        return achievedDistance;
    }

    public void setAchievedDistance(int achievedDistance) {
        this.achievedDistance = achievedDistance;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public Exercise getExersise() {
        return exersise;
    }

    public void setExersise(Exercise exersise) {
        this.exersise = exersise;
    }

    public int getIsGroup() {
        return isGroup;
    }

    public void setIsGroup(int isGroup) {
        this.isGroup = isGroup;
    }

    public boolean persist(String username) {
        try {
            java.util.Date utilDate = new java.util.Date();
            java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());
            Connection con = DBAccess.getConnection();

            PreparedStatement ps = con.prepareStatement("INSERT INTO Goal "
                    + "VALUES(?, ?, ?, ?, 'C', ?, ?, ?, ?, ?, 0, ?);");
            ps.setString(1, username);
            ps.setString(2, goalName);
            ps.setDate(3, startDate);
            ps.setDate(4, dueDate);
            ps.setInt(5, targetWeight);
            ps.setInt(6, targetDistance);
            ps.setInt(7, targetHeartRate);
            ps.setInt(8, targetBMI);
            ps.setString(9, activityType);
            ps.setInt(10, isGroup);
            String sql2 = "INSERT INTO History VALUES('" + username + "', 'Created Custom Goal " + goalName + "', '"
                    + currentDate + "')";
            ps.executeUpdate();
            DBAccess.insertStatement(sql2);

        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public boolean updateGoal(String username) {
        try {
            Connection con = DBAccess.getConnection();

            PreparedStatement ps = con.prepareStatement("UPDATE Goal SET "
                    + "dueDate = ?, targetWeight = ?, targetDistance = ?, "
                    + "targetHeartRate = ?, targetBMI = ?, activityType = ?"
                    + " WHERE goalOwner = ? AND goalName = ? AND isGroup = ?;");

            ps.setDate(1, dueDate);
            ps.setInt(2, targetWeight);
            ps.setInt(3, targetDistance);
            ps.setInt(4, targetHeartRate);
            ps.setInt(5, targetBMI);
            ps.setString(6, activityType);
            ps.setString(7, username);
            ps.setString(8, goalName);
            ps.setInt(9, isGroup);
            ps.executeUpdate();

        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public double getActivityCustomForUser(String username, String activity) {

        java.util.Date utilDate = new java.util.Date();
        java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

        try {

            String sql = "SELECT custom FROM activity WHERE"
                    + " username = '" + username + "' AND activityType = '"
                    + activity + "'";

            ResultSet rs = DBAccess.selectStatement(sql);

            double custom = 0;
            while (rs.next()) {
                custom = rs.getDouble(1);
            }
            return custom;

        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
    }

    public static ArrayList<CustomGoal> getCustomGoalsForUser(String username) {

        ArrayList<CustomGoal> customGoals = new ArrayList<CustomGoal>();

        java.util.Date utilDate = new java.util.Date();
        java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

        try {
            Connection con = DBAccess.getConnection();
            Statement statement = con.createStatement();

            // I havent bothered using prepared statements here because theres no user input
            String sql = "SELECT * FROM Goal WHERE goalOwner = '" + username + "' AND "
                    + "goalType = 'C' AND dueDate >= '" + currentDate + "' "
                    + "AND isGroup = 0;";

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                String goalName = rs.getString("goalName");
                Date startDate = rs.getDate("startDate");
                Date dueDate = rs.getDate("dueDate");
                int targetDistance = Integer.parseInt(rs.getString("targetDistance"));
                int targetHeartRate = Integer.parseInt(rs.getString("targetHeartRate"));
                int targetBMI = Integer.parseInt(rs.getString("targetBMI"));
                int targetWeight = Integer.parseInt(rs.getString("targetWeight"));
                String activityType = rs.getString("activityType");
                CustomGoal goal = new CustomGoal(goalName, startDate, dueDate, targetWeight, targetDistance, targetHeartRate, targetBMI, activityType);
                goal.setAchievedDistance(rs.getInt("achievedDistance"));
                customGoals.add(goal);
            }

            con.close();

        } catch (Exception e) {
            System.out.println(e);
        }

        return customGoals;
    }

    public static ArrayList<CustomGoal> getCustomGoalsForGroup(String username) {

        ArrayList<CustomGoal> customGoals = new ArrayList<CustomGoal>();

        java.util.Date utilDate = new java.util.Date();
        java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

        try {
            Connection con = DBAccess.getConnection();
            Statement statement = con.createStatement();

            // I havent bothered using prepared statements here because theres no user input
            String sql = "SELECT * FROM Goal WHERE goalOwner = '" + username + "' AND "
                    + "goalType = 'C' AND dueDate >= '" + currentDate + "' "
                    + "AND isGroup = 1;";

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                String goalName = rs.getString("goalName");
                Date startDate = rs.getDate("startDate");
                Date dueDate = rs.getDate("dueDate");
                int targetDistance = Integer.parseInt(rs.getString("targetDistance"));
                int targetHeartRate = Integer.parseInt(rs.getString("targetHeartRate"));
                int targetBMI = Integer.parseInt(rs.getString("targetBMI"));
                int targetWeight = Integer.parseInt(rs.getString("targetWeight"));
                String activityType = rs.getString("activityType");
                CustomGoal goal = new CustomGoal(goalName, startDate, dueDate, targetWeight, targetDistance, targetHeartRate, targetBMI, activityType);
                goal.setAchievedDistance(rs.getInt("achievedDistance"));
                customGoals.add(goal);
            }

            con.close();

        } catch (Exception e) {
            System.out.println(e);
        }

        return customGoals;
    }

    /**
     * This method is used to get fill a Goal object with its details from the
     * database.
     *
     * It is used in displaying goal pages when we know only their usernames and
     * need to display more data.
     *
     * @return true iff successful
     */
    @Override
    public boolean getGoalDetails() {

        boolean valid = false;

        try {
            Connection con = DBAccess.getConnection();
            Statement statement = con.createStatement();

            // I havent bothered using prepared statements here because theres no user input
            String sql = "SELECT * FROM Goal WHERE goalName = '" + goalName + "' AND goalOwner = '" + goalOwner
                    + "' AND goalType = 'C' AND isGroup = 0";

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                valid = true;
                startDate = rs.getDate("startDate");
                dueDate = rs.getDate("dueDate");
                targetDistance = Integer.parseInt(rs.getString("targetDistance"));
                targetHeartRate = Integer.parseInt(rs.getString("targetHeartRate"));
                targetBMI = Integer.parseInt(rs.getString("targetBMI"));
                targetWeight = Integer.parseInt(rs.getString("targetWeight"));
                activityType = rs.getString("activityType");
                setAchievedDistance(rs.getInt("achievedDistance"));
            }

            con.close();

        } catch (Exception e) {
            System.out.println(e);
            return false;
        }

        return valid;

    }

    /**
     *
     *
     * @return true iff successful
     */
    public boolean getGoalDetailsForGroup() {

        boolean valid = false;
        try {
            Connection con = DBAccess.getConnection();
            Statement statement = con.createStatement();

            // I havent bothered using prepared statements here because theres no user input
            String sql = "SELECT * FROM Goal WHERE goalName = '" + goalName + "' AND goalOwner = '" + goalOwner
                    + "' AND goalType = 'C' AND isGroup = 1";

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                valid = true;
                startDate = rs.getDate("startDate");
                dueDate = rs.getDate("dueDate");
                targetDistance = Integer.parseInt(rs.getString("targetDistance"));
                targetHeartRate = Integer.parseInt(rs.getString("targetHeartRate"));
                targetBMI = Integer.parseInt(rs.getString("targetBMI"));
                targetWeight = Integer.parseInt(rs.getString("targetWeight"));
                activityType = rs.getString("activityType");
                setAchievedDistance(rs.getInt("achievedDistance"));
            }

            con.close();

        } catch (Exception e) {
            System.out.println(e);
            return false;
        }

        return valid;

    }
}
