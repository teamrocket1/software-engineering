/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Utilities.DBAccess;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author 100015558
 */
public class DistanceGoal extends Goal {

    private int targetDistance;
    private int achievedDistance;
    private String activityType;

    public DistanceGoal(int targetDistance, Date currentDate, Date dueDate, String gName, String aType) {
        super("", gName);
        this.targetDistance = targetDistance;
        this.startDate = currentDate;
        this.dueDate = dueDate;
        status = false;
        activityType = aType;
        achievedDistance = 0;
    }

    public DistanceGoal(String owner, String gName) {
        super(owner, gName);
    }

    @Override
    boolean isCompleted() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getTargetDistance() {
        return targetDistance;
    }

    public void setTargetDistance(int targetDistance) {
        this.targetDistance = targetDistance;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public int getAchievedDistance() {
        return achievedDistance;
    }

    public void setAchievedDistance(int achievedDistance) {
        this.achievedDistance = achievedDistance;
    }

    public String getGoalOwner() {
        return goalOwner;
    }

    public void setGoalOwner(String goalOwner) {
        this.goalOwner = goalOwner;
    }

    public int getIsGroup() {
        return isGroup;
    }

    public void setIsGroup(int isGroup) {
        this.isGroup = isGroup;
    }

    
    public double getPercentCompletion(){
        
        // u is current, v is final
        double u = (double) achievedDistance;
        double v = (double) targetDistance;
        
        if(v < u){
            return 100.0;
        }
        else{
            return (u/v) * 100;
        }
        
    }
    

    public boolean persist(String username) {
        try {
            java.util.Date utilDate = new java.util.Date();
            java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("INSERT INTO Goal "
                    + "VALUES(?, ?, ?, ?, 'D', 0, ?, 0, 0, ?, 0, ?);");
            ps.setString(1, username);
            ps.setString(2, goalName);
            ps.setDate(3, startDate);
            ps.setDate(4, dueDate);
            ps.setInt(5, targetDistance);
            ps.setString(6, activityType);
            ps.setInt(7, isGroup);


            String sql2 = "INSERT INTO History VALUES('" + username + "', 'Created Distance Goal " + goalName + "', '"
                    + currentDate + "')";
            /*PreparedStatement ps2 = con.prepareStatement("INSERT INTO History "
             + "VALUES(?, 'Created Distance Goal ?', ?)");
             ps2.setString(1, username);
             ps2.setString(2, goalName);
             ps2.setDate(3, currentDate);
             */
            ps.executeUpdate();
            DBAccess.insertStatement(sql2);

        } catch (Exception e) {
            return false;
        }

        return true;
    }


    public boolean updateGoal(String username) {
        try {

            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("UPDATE Goal "
                    + "SET dueDate = ?, targetDistance = ?, "
                    + "activityType = ? WHERE goalOwner = ? AND "
                    + "goalName = ? AND isGroup = ?;");

            ps.setDate(1, dueDate);
            ps.setInt(2, targetDistance);
            ps.setString(3, activityType);
            ps.setString(4, username);
            ps.setString(5, goalName);
            ps.setInt(6, isGroup);
            ps.executeUpdate();

        } catch (Exception e) {
            return false;
        }

        return true;
    }
    
        public void addToGroupGoal(String goalOwner) {
                try {

            Connection con = DBAccess.getConnection();
            PreparedStatement ps = con.prepareStatement("UPDATE Goal SET "
                    + "achievedDistance = ? WHERE goalOwner = ? AND "
                    + "goalName = ? AND isGroup = ?;");

            ps.setInt(1, achievedDistance);
            ps.setString(2, goalOwner);
            ps.setString(3, goalName);
            ps.setInt(4, isGroup);
            ps.executeUpdate();

        } catch (Exception e) {
        }
    }

    public double getActivityDistanceForUser(String username, String activity) {

        java.util.Date utilDate = new java.util.Date();
        java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

        try {

            String sql = "SELECT distance FROM activity WHERE"
                    + " username = '" + username + "' AND activityType = '"
                    + activity + "'";

            ResultSet rs = DBAccess.selectStatement(sql);

            double distance = 0;
            while (rs.next()) {
                distance = rs.getDouble(1);
            }
            return distance;

        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
    }

    public static ArrayList<DistanceGoal> getDistanceGoalsForUser(String username) {

        ArrayList<DistanceGoal> distanceGoals = new ArrayList<DistanceGoal>();

        java.util.Date utilDate = new java.util.Date();
        java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

        try {
            Connection con = DBAccess.getConnection();
            Statement statement = con.createStatement();

            // I havent bothered using prepared statements here because theres no user input
            String sql = "SELECT * FROM Goal WHERE goalOwner = '" + username + "' AND "
                    + "goalType = 'D' AND dueDate >= '" + currentDate + "' "
                    + "AND isGroup = 0;";

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                String goalName = rs.getString("goalName");
                Date startDate = rs.getDate("startDate");
                Date dueDate = rs.getDate("dueDate");
                int targetDistance = Integer.parseInt(rs.getString("targetDistance"));
                String activityType = rs.getString("activityType");
                DistanceGoal goal = new DistanceGoal(targetDistance, startDate, dueDate, goalName, activityType);
                goal.setAchievedDistance(rs.getInt("achievedDistance"));
                distanceGoals.add(goal);
            }

            con.close();

        } catch (Exception e) {
            System.out.println(e);
        }

        return distanceGoals;
    }

    public static ArrayList<DistanceGoal> getDistanceGoalsForGroup(String groupName) {

        ArrayList<DistanceGoal> distanceGoals = new ArrayList<DistanceGoal>();

        java.util.Date utilDate = new java.util.Date();
        java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

        try {
            Connection con = DBAccess.getConnection();

            PreparedStatement ps = con.prepareCall("SELECT * FROM Goal WHERE "
                    + "goalOwner = ? AND "
                    + "goalType = 'D' AND isGroup = 1;");
            ps.setString(1, groupName);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                String goalName = rs.getString("goalName");
                Date startDate = rs.getDate("startDate");
                Date dueDate = rs.getDate("dueDate");
                int targetDistance = Integer.parseInt(rs.getString("targetDistance"));
                String a = rs.getString("activityType");
                DistanceGoal goal = new DistanceGoal(targetDistance, startDate, dueDate, goalName, a);

                distanceGoals.add(goal);
            }

            con.close();

        } catch (Exception e) {
            System.out.println(e);;
        }

        return distanceGoals;
    }

    /**
     * This method is used to get fill a Goal object with its details from the
     * database.
     *
     * It is used in displaying goal pages when we know only their usernames and
     * need to display more data.
     *
     * @return true iff successful
     */
    @Override
    public boolean getGoalDetails() {

        boolean valid = false;

        try {
            Connection con = DBAccess.getConnection();
            Statement statement = con.createStatement();

            // I havent bothered using prepared statements here because theres no user input
            String sql = "SELECT * FROM Goal WHERE goalName = '" + goalName + "' AND goalOwner = '" + goalOwner
                    + "' AND goalType = 'D' AND isGroup = 0";

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                valid = true;
                startDate = rs.getDate("startDate");
                dueDate = rs.getDate("dueDate");
                targetDistance = Integer.parseInt(rs.getString("targetDistance"));
                activityType = rs.getString("activityType");
                setAchievedDistance(rs.getInt("achievedDistance"));
            }

            con.close();

        } catch (Exception e) {
            System.out.println(e);
            return false;
        }

        return valid;

    }

    /**
     *
     *
     * @return true iff successful
     */
    public boolean getGoalDetailsForGroup() {

        boolean valid = false;
        try {
            Connection con = DBAccess.getConnection();
            Statement statement = con.createStatement();

            // I havent bothered using prepared statements here because theres no user input
            String sql = "SELECT * FROM Goal WHERE goalName = '" + goalName + "' AND goalOwner = '" + goalOwner
                    + "' AND goalType = 'D' AND isGroup = 1";

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                valid = true;
                startDate = rs.getDate("startDate");
                dueDate = rs.getDate("dueDate");
                targetDistance = Integer.parseInt(rs.getString("targetDistance"));
                activityType = rs.getString("activityType");
                setAchievedDistance(rs.getInt("achievedDistance"));
            }

            con.close();

        } catch (Exception e) {
            System.out.println(e);
            return false;
        }

        return valid;

    }
}
