package Model;

import Utilities.DBAccess;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WeightGoal extends Goal {

    private int targetWeight;

    public WeightGoal(String gName, Date start, Date due, int tWeight) {
        super("", gName);
        startDate = start;
        dueDate = due;
        targetWeight = tWeight;
        status = false;
    }

    public WeightGoal(String owner, String gName) {
        super(owner, gName);
    }

    @Override
    boolean isCompleted() {
        // TODO Auto-generated method stub
        return false;
    }

    public int getTargetWeight() {
        return targetWeight;
    }

    public void setTargetWeight(int targetWeight) {
        this.targetWeight = targetWeight;
    }

    public void setIsGroup(int isGroup) {
        this.isGroup = isGroup;
    }

    public int getIsGroup() {
        return isGroup;
    }
    
    public double getPercentCompletion(int currentWeight){
        
        // u is current, v is final
        double u = (double) currentWeight;
        double v = (double) targetWeight;
        
        if(v < u){
            return ((u - v)/v) * 100;
        }
        else{
            return ((v - u)/v) * 100;
        }
        
    }

    public boolean persist(String username) {
        try {
            java.util.Date utilDate = new java.util.Date();
            java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

            Connection con = DBAccess.getConnection();

            Statement statement2 = con.createStatement();

            PreparedStatement ps = con.prepareStatement("INSERT INTO Goal "
                    + "VALUES(?, ?, ?, ?, 'W', ?, 0 , 0, 0, 'NA', 0, ?)");
            ps.setString(1, username);
            ps.setString(2, goalName);
            ps.setDate(3, startDate);
            ps.setDate(4, dueDate);
            ps.setInt(5, targetWeight);
            ps.setInt(6, isGroup);

            String sql2 = "INSERT INTO History VALUES('" + username + "', 'Created Weight Goal " + goalName + "', '"
                    + currentDate + "')";

            ps.executeUpdate();
            statement2.executeUpdate(sql2);
            con.close();

        } catch (Exception e) {
            Logger.getLogger(WeightGoal.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }



        return true;
    }

    public boolean updateGoal(String username) {
        try {
            java.util.Date utilDate = new java.util.Date();
            java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

            Connection con = DBAccess.getConnection();

            PreparedStatement ps = con.prepareStatement("UPDATE Goal SET"
                    + " dueDate = ?, targetWeight = ? WHERE goalOwner = ? AND "
                    + "goalName = ? AND isGroup = ?");
            ps.setDate(1, dueDate);
            ps.setInt(2, targetWeight);
            ps.setString(3, username);
            ps.setString(4, goalName);
            ps.setInt(5, isGroup);

            ps.executeUpdate();
            con.close();

        } catch (Exception e) {
            Logger.getLogger(WeightGoal.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }



        return true;
    }

    public static ArrayList<WeightGoal> getWeightGoalsForUser(String username) {

        ArrayList<WeightGoal> weightGoals = new ArrayList<WeightGoal>();

        java.util.Date utilDate = new java.util.Date();
        java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

        try {
            Connection con = DBAccess.getConnection();
            Statement statement = con.createStatement();
            Statement statement2 = con.createStatement();

//            String sql = "SELECT * FROM Goal WHERE goalOwner = '" + username + "' AND "
//                    + "goalType = 'W' AND dueDate >= '" + currentDate + "'";
            String sql = "SELECT * FROM Goal WHERE goalOwner = '" + username + "' AND "
                    + "goalType = 'W'";

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                String goalName = rs.getString("goalName");
                Date startDate = rs.getDate("startDate");
                Date dueDate = rs.getDate("dueDate");
                int targetWeight = Integer.parseInt(rs.getString("targetWeight"));
                WeightGoal goal = new WeightGoal(goalName, startDate, dueDate, targetWeight);

                weightGoals.add(goal);
            }

            con.close();

        } catch (Exception e) {
            System.out.println(e);;
        }

        return weightGoals;
    }

    public static ArrayList<WeightGoal> getWeightGoalsForGroup(String groupName) {

        ArrayList<WeightGoal> weightGoals = new ArrayList<WeightGoal>();

        java.util.Date utilDate = new java.util.Date();
        java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

        try {
            Connection con = DBAccess.getConnection();

            PreparedStatement ps = con.prepareCall("SELECT * FROM Goal WHERE "
                    + "goalOwner = ? AND "
                    + "goalType = 'W' AND isGroup = 1;");
            ps.setString(1, groupName);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                String goalName = rs.getString("goalName");
                Date startDate = rs.getDate("startDate");
                Date dueDate = rs.getDate("dueDate");
                int targetWeight = Integer.parseInt(rs.getString("targetWeight"));
                WeightGoal goal = new WeightGoal(goalName, startDate, dueDate, targetWeight);

                weightGoals.add(goal);
            }

            con.close();

        } catch (Exception e) {
            System.out.println(e);;
        }

        return weightGoals;
    }

    /**
     * This method is used to get fill a Goal object with its details from the
     * database.
     *
     * It is used in displaying goal pages when we know only their usernames and
     * need to display more data.
     *
     * @return true iff successful
     */
    @Override
    public boolean getGoalDetails() {
        boolean valid = false;
        try {
            Connection con = DBAccess.getConnection();
            Statement statement = con.createStatement();

            // I havent bothered using prepared statements here because theres no user input
            String sql = "SELECT * FROM Goal WHERE goalName = '" + goalName + "' AND goalOwner = '" + goalOwner
                    + "' AND goalType = 'W' AND isGroup = 0";

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                valid = true;
                startDate = rs.getDate("startDate");
                dueDate = rs.getDate("dueDate");
                targetWeight = Integer.parseInt(rs.getString("targetWeight"));
            }

            con.close();

        } catch (Exception e) {
            System.out.println(e);
            return false;
        }

        return valid;

    }

    /**
     *
     *
     * @return true iff successful
     */
    public boolean getGoalDetailsForGroup() {

        boolean valid = false;
        try {
            Connection con = DBAccess.getConnection();
            Statement statement = con.createStatement();

            // I havent bothered using prepared statements here because theres no user input
            String sql = "SELECT * FROM Goal WHERE goalName = '" + goalName + "' AND goalOwner = '" + goalOwner
                    + "' AND goalType = 'W' AND isGroup = 1";

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                valid = true;
                startDate = rs.getDate("startDate");
                dueDate = rs.getDate("dueDate");
                targetWeight = Integer.parseInt(rs.getString("targetWeight"));
            }

            con.close();

        } catch (Exception e) {
            System.out.println(e);
            return false;
        }

        return valid;

    }
}
