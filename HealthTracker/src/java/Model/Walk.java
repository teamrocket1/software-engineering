package Model;

import Utilities.DBAccess;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
//import java.util.Date;

/**
 *
 * @author Takomborerwa
 */
public class Walk extends Activity {

    private int heartRate;

    public Walk(int distance, int duration, String username, String sex, float weight, int age, int heartRate) {
        this.distance = distance;
        this.duration = duration;
        this.username = username;
        this.sex = sex;
        this.weight = weight;
        this.age = age;
        this.heartRate = heartRate;
    }

    public Walk(int distance, int duration, String username, Date d) {
        this.distance = distance;
        this.duration = duration;
        this.username = username;
        this.date = d;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * A method to calculate the amount of calories burnt during a Walk.
     *
     * @return Amount of calories burnt during a Walk rounded down to a whole
     * number.
     */
    @Override
    public int getCaloriesBurnt() {

        double calories = 0;

        if (sex.equalsIgnoreCase("m")) {
            double a = (double) (age * 0.20175);
            double w = (double) (weight * 0.09036);
            double h = (double) (heartRate * 0.6309);
            calories = ((a + w + h) - 55.0969) + (duration / 4.184);
        } else if (sex.equalsIgnoreCase("f")) {
            double a = (double) (age * 0.074);
            double w = (double) (weight * 0.05741);
            double h = (double) (heartRate * 0.4472);
            calories = ((a + w + h) - 55.0969) + (duration / 20.4022);
        }

        return (int) calories;

    }

    public boolean persist() {
        try {
            Connection con = DBAccess.getConnection();

            Statement statement = con.createStatement();
            Statement statement2 = con.createStatement();
            Statement statement3 = con.createStatement();
            Statement statement4 = con.createStatement();

            java.util.Date utilDate = new java.util.Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());



            String sql = "INSERT INTO Activity VALUES('" + username + "', "
                    + distance + ", " + duration + ", " + getCaloriesBurnt()
                    + ", '" + sqlDate + "', 'W' )";

            String moreSQL = "INSERT INTO History VALUES('" + username + "', 'Took a " + distance + "km walk', '"
                    + sqlDate + "')";

            DBAccess.insertStatement(moreSQL);

            statement.executeUpdate(sql);

            // Calling sql function to update goal status
            String sqlAgain = "SELECT * FROM updateAchievedDistance('" + username + "'," + distance + ", 'W')";
            statement4.executeQuery(sqlAgain);

            ArrayList<DistanceGoal> distanceGoals = DistanceGoal.getDistanceGoalsForUser(username);
            ArrayList<CustomGoal> customGoals = CustomGoal.getCustomGoalsForUser(username);

            for (DistanceGoal g : distanceGoals) {

                /* Check if goal achieved*/
                if (g.getAchievedDistance() >= g.getTargetDistance()) {

                    /* If achieved delete from db and do some other stuff */
                    /* Other stuff(notifications) to be added later*/
                    g.status = true;

                    // Insert into achievments
                    sql = "INSERT INTO Achievments VALUES('" + username + "', '" + g.getGoalName()
                            + "', '" + sqlDate + "', 'D'," + 0 + "," + g.getTargetDistance() + ","
                            + 0 + "," + 0 + ",'" + g.getActivityType() + "'"
                            + ")";

                    String sql2 = "DELETE FROM Goal WHERE goalName = '" + g.getGoalName() + "'"
                            + "AND goalOwner = '" + username + "'";

                    String moreSQLAgain = "INSERT INTO History VALUES('" + username + "', 'Completed Goal " + g.getGoalName() + "', '"
                            + sqlDate + "')";

                    DBAccess.insertStatement(moreSQLAgain);

                    statement2.executeUpdate(sql);

                    statement3.executeUpdate(sql2);


                }
            }

            con.close();

        } catch (Exception e) {
            return false;
        }
        try {
            groupPersist(username, distance);
        } catch (Exception e) {
        }

        return true;
    }

    public static void groupPersist(String username, int distance) {
        //The groups the member is in
        ArrayList<Membership> groups = Membership.getUsersMemberships(username);
        for (Membership m : groups) {
            String goalOwner = m.getGroupName();
            //Perform an update on the goal where acitivty is bike(B)
            ArrayList<DistanceGoal> dgList =
                    DistanceGoal.getDistanceGoalsForGroup(goalOwner);
            for (DistanceGoal d : dgList) {
                int before = d.getAchievedDistance();
                int after = before + distance;
                d.setAchievedDistance(after);
                d.setIsGroup(1);
                d.setActivityType("W");
                d.addToGroupGoal(goalOwner);
            }

        }
    }

    public static ArrayList<Walk> getWalksForUser(String username) {

        ArrayList<Walk> walks = new ArrayList<Walk>();

        java.util.Date utilDate = new java.util.Date();
        java.sql.Date currentDate = new java.sql.Date(utilDate.getTime());

        try {
            Connection con = DBAccess.getConnection();
            Statement statement = con.createStatement();

            // I havent bothered using prepared statements here because theres no user input

            // Gets walks from the start of the year.
            String sql = "SELECT * FROM Activity WHERE username = '" + username + "' AND "
                    + "activityType = 'W' AND date >= '2014/01/01'";

            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                int distance = Integer.parseInt(rs.getString("distance"));
                int duration = Integer.parseInt(rs.getString("duration"));
                Date date = rs.getDate("date");
                Walk w = new Walk(distance, duration, username, date);
                walks.add(w);
            }

            con.close();

        } catch (Exception e) {
            System.out.println(e);
        }

        return walks;
    }
}
