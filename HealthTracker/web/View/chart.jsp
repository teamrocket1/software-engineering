<%-- 
    Document   : chart
    Created on : 30-Mar-2014, 18:15:52
    Author     : qju12etu
--%>

<!doctype html>
<html>
    <head>
   
    <link rel="stylesheet" type="text/css" href="../Chart.js-master/site/style.css">
    <p>Head</p>
    <!--insert Doughnut-->
    <title>Doughnut Chart</title>

    <script src="../Chart.js-master/Chart.js"></script>
    <meta name = "viewport" content = "initial-scale = 1, user-scalable = no">
    <style>
        canvas{
        }
    </style>
</head>
<body>


<div class = "mainWrapper">	

    <div class= "Menu">
        <p>Menu</p>
    </div>

    <div class = "stats">
        <p>Body</p>

    <!-- @start CHART 1 -->
    <div class="chart">
        <p> we in here</p>
        <canvas id="canvas" height="150" width="150"></canvas>							
    </div>
    <!-- @end CHART 1 -->

    <!-- @start CHART 2 -->
    <div class="chart">
        <p> we in here</p>
        <canvas id="canvas2" height="150" width="150"></canvas>
    </div>
    <!-- @end CHART 2 -->

    <!--@start CHART 3 -->
    <div class="chart">
        <p> we in here</p>
        <canvas id="canvas3" height="150" width="150"></canvas>
    </div>
    <!-- @end CHART 3 -->

</div>
</div>
</body>

<!-- Script for the Pie Charts-->
<script>
    var doughnutData = [
        {
            value: 30,
            color: "#F7464A"
        },
        {
            value: 50,
            color: "#46BFBD"
        },
        {
            value: 100,
            color: "#FDB45C"
        },
        {
            value: 40,
            color: "#949FB1"
        },
        {
            value: 120,
            color: "#4D5360"
        }
    ];
    var myDoughnut = new Chart(document.getElementById("canvas").getContext("2d")).Doughnut(doughnutData);
    var myDoughnut = new Chart(document.getElementById("canvas2").getContext("2d")).Doughnut(doughnutData);
    var myDoughnut = new Chart(document.getElementById("canvas3").getContext("2d")).Doughnut(doughnutData);
</script>

<div class="footer">
    <p>Footer</p>
</div>	
</div>
</html>