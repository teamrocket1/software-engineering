<%-- 
    Document   : Groups
    Created on : Mar 8, 2014, 12:20:14 AM
    Author     : Odie
--%>

<%@page import="java.util.Iterator"%>
<%@page import="Model.Membership"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:useBean id="aUser" type="Model.User" scope="session" />
        <jsp:useBean id="memberships" type="ArrayList<Membership>" scope="session" />
        
        
        <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400italic' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="css/style2.css" type="text/css" />

        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!---strat-slider---->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/slider-style.css" />
        <script type="text/javascript" src="js/modernizr.custom.28468.js"></script>
        <!---//start-slider---->
        <!---start-login-script-->
        <script src="js/login.js"></script>
        <!---//End-login-script--->
        <!--768px-menu---->
        <link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
        <script type="text/javascript" src="js/jquery.mmenu.js"></script>
        <script type="text/javascript">
            //	The menu on the left
            $(function() {
                $('nav#menu-left').mmenu();
            });
        </script>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Groups \(^_^)/</title>
    </head>
    <body>
        
        <div class="header">
            <div class="wrap">
                <!--<div class="header-left">
                        <div class="logo">
                                <a href="index.jsp">HealthTracker</a>
                        </div>
                </div>-->
                <div class="header-right2">
                    <div class="top-nav">
                        <ul>
                            <%                                                String username = aUser.getUsername();
                                String firstLetter = username.substring(0, 1);
                                String remainingLetters = username.substring(1, username.length());
                                username = firstLetter.toUpperCase() + remainingLetters;

                            %>

                            <li><a href="/HealthTracker/ProfilePageController?pageOwner=<%=aUser.getUsername()%>"><%= username%></a></li>
                            <li><a href="/HealthTracker/DataCaptureController?type=clicked">Update Activities</a></li>
                            <li><a href="/HealthTracker/GoalController?type=clicked">Goals</a></li>
                            <li><a href="/HealthTracker/GroupController?type=clicked">Groups</a></li>
                            <li><a href="/HealthTracker/HistoryController?type=history">History</a></li>
                            <li><a href="/HealthTracker/AchievmentController?type=clicked">Achievements</a></li>
                            <li><a href="/HealthTracker/HomeController?type=clickedEdit">Edit Profile</a></li>
                            <li><a href="/HealthTracker/HistoryController?type=activityStats">Activity Stats</a></li>
                        </ul>
                    </div>

                    <div class="clear"> </div>
                    </ul>
                </div>
                <div class="clear"> </div>
            </div>
            <div class="clear"> </div>
        </div>
        
                            <br />
                            
        <h1>Groups</h1>

        <a href="/HealthTracker/HomeController?type=clicked"><h3>Home</h3></a> <a style="margin-left: 70%" href="/HealthTracker/LoginController?type=logout">Log out</a><br />

        <table border="1">                
            <tr>
                <td>Group Name</td>
                <td>Role</td>
                <td>Permissions Level</td>               
            </tr>
            <%
                if (session.getAttribute("memberships") != null) {

                    ArrayList<Membership> mems = (ArrayList<Membership>) session.getAttribute("memberships");
                    Iterator it = mems.iterator();
                    while (it.hasNext()) {
                        
                        Membership m = (Membership) it.next();              
            %>
            
            <tr> 
                <td><a href="/HealthTracker/GroupController?type=groupPage&groupName=<%= m.getGroupName()%>"><%=m.getGroupName()%></a></td>
                <td><%=m.getRole()%></td>
                <td><%=m.getPermissionsLevel()%></td>
            </tr>


            <%
                }
            %>
        </table>
        
        
        <%
            }
        %>
<br /><br />
        
        
        
        
        
        <!-- Create Group ... This code should be moved to separate page later -->
        <!-- Group image maybe ... like a profile pic for a group? -->
        <form ACTION="/HealthTracker/GroupController" method="POST">

            <label for="groupName">Group Name:</label>
            <input type="text" name="groupName" placeholder="Group Name" /> <br />
            <input type = "hidden" value = "createGroup" name = "type" />
            <input class="sub" type="submit" value="Create Group" />

        </form>
        
        
        It would just be hidden under a button in that case.<br />


    </body>
</html>
