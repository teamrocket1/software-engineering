<%-- 
    Document   : Conversation
    Created on : Mar 31, 2014, 7:11:46 PM
    Author     : Odie
--%>

<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:useBean id="aUser" type="Model.User" scope="session" />
        <jsp:useBean id="messages" type="ArrayList<String>" scope="session" />
        <jsp:useBean id="ordering" type="ArrayList<String>" scope="session" />

        
        <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400italic' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="css/style2.css" type="text/css" />

        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!---strat-slider---->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/slider-style.css" />
        <script type="text/javascript" src="js/modernizr.custom.28468.js"></script>
        <!---//start-slider---->
        <!---start-login-script-->
        <script src="js/login.js"></script>
        <!---//End-login-script--->
        <!--768px-menu---->
        <link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
        <script type="text/javascript" src="js/jquery.mmenu.js"></script>
        <script type="text/javascript">
            //	The menu on the left
            $(function() {
                $('nav#menu-left').mmenu();
            });
        </script>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Messages</title>

        <style>

            #message {
                width: 100%;
            }

            .received {
                width: 100%;
                margin-left: 0%;

            }

            .reply {
                width: 100%;
                margin-left: 35%;

            }

            #convoform {
                width: 100%;
            }

            #submit {
                width: 100%;
                margin-left: 80%;
            }

            input {
                align: right;
            }

        </style>

    </head>
    <body>
        
        <div class="header">
        <div class="wrap">
            <div class="header-right2">
                <div class="top-nav">
                    <ul>
                        <%                                                String username = aUser.getUsername();
                            String firstLetter = username.substring(0, 1);
                            String remainingLetters = username.substring(1, username.length());
                            username = firstLetter.toUpperCase() + remainingLetters;

                        %>

                        <li><a href="/HealthTracker/ProfilePageController?pageOwner=<%=aUser.getUsername()%>"><%= username%></a></li>
                        <li><a href="/HealthTracker/DataCaptureController?type=clicked">Update Activities</a></li>
                        <li><a href="/HealthTracker/GoalController?type=clicked">Goals</a></li>
                        <li><a href="/HealthTracker/GroupController?type=clicked">Groups</a></li>
                        <li><a href="/HealthTracker/HistoryController?type=history">History</a></li>
                        <li><a href="/HealthTracker/AchievmentController?type=clicked">Achievements</a></li>
                        <li><a href="/HealthTracker/HistoryController?type=activityStats">Activity Stats</a></li>
                        <li><a href="/HealthTracker/MessageController?type=viewMessages">Messages</a></li>
                    </ul>
                </div>

                <div class="clear"> </div>
                </ul>
            </div>
            <div class="clear"> </div>
        </div>
        <div class="clear"> </div>
    </div>
    <br />
    <br />
        
        <h1>Convo wid a Ho!</h1>


        <%

            for(int i = 0; i < messages.size(); i++){
                
                if(ordering.get(i).equalsIgnoreCase("sent")){
                   %>
                    
                   <div class="received" style="background: lightgreen; border-color:hotpink; height:75px; width: 65%">
                    <%
                    out.println(messages.get(i));
                    %>
                </div><br /> 
                   
                   <%
                }
                
                else if(ordering.get(i).equalsIgnoreCase("received")){
                    %>
                    
                    <div class="reply" style="background:cyan; height:75px; width: 65%">
                    <%
                    out.println(messages.get(i));
                    %>
                </div><br /> 
                            
                            
                            <%
                    
                }
                
            }
            
        %>


        <form class ="convoform" action="MessageController" method="POST">
            <textarea style="width: 80%; margin-left: 10%; margin-right: 10%;" name="msg" cols="78" rows="5" ></textarea>
            <input type="hidden" value="postMessage" name="type"/>
            <input type="hidden" value="<%= aUser.getUsername()%>" name="sender" />
            <input type="hidden" value="<%= request.getParameter("sender")%>" name="firstSender" />
            <input type="hidden" value="<%= request.getParameter("sender")%>" name="recipient" />
            <input type="hidden" value="<%=request.getParameter("party")%>" name="party" />
            <div id="submit">
                <input  type="submit" value="REPLY" />
            </div>
        </form>
    </body>
</html>
