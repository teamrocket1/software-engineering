<%-- 
    Document   : Goals
    Created on : Mar 8, 2014, 12:29:56 AM
    Author     : Odie
--%>

<%@page import="Model.CustomGoal"%>
<%@page import="Model.FitnessGoal"%>
<%@page import="Model.DistanceGoal"%>
<%@page import="java.util.Iterator"%>
<%@page import="Model.WeightGoal"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:useBean id="aUser" type="Model.User" scope="session" />
        <jsp:useBean id="weightGoals" type="ArrayList<WeightGoal>" scope="session" />
        <jsp:useBean id="distanceGoals" type="ArrayList<DistanceGoal>" scope="session" />


        <link href='http://fonts.googleapis.com/css?family=Droid+Serif:400italic' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="css/style2.css" type="text/css" />

        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="images/fav-icon.png" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!---strat-slider---->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="css/slider-style.css" />
        <script type="text/javascript" src="js/modernizr.custom.28468.js"></script>
        <!---//start-slider---->
        <!---start-login-script-->
        <script src="js/login.js"></script>
        <!---//End-login-script--->
        <!--768px-menu---->
        <link type="text/css" rel="stylesheet" href="css/jquery.mmenu.all.css" />
        <script type="text/javascript" src="js/jquery.mmenu.js"></script>
        <script type="text/javascript">
            //	The menu on the left
            $(function() {
                $('nav#menu-left').mmenu();
            });
        </script>

        <script type="text/javascript">

            function goalCheck() {
                var dropdown = document.getElementById("goalSelect");
                var selected = dropdown.options[dropdown.selectedIndex].value;

                if (selected == 0) {
                    document.getElementById('weightForm').style.display = 'block';
                    document.getElementById('fitnessForm').style.display = 'none';
                    document.getElementById('customForm').style.display = 'none';
                    document.getElementById('distanceForm').style.display = 'none';


                    document.getElementById("weightType").value = "set";
                    document.getElementById("fitnessType").value = "!set";
                    document.getElementById("customType").value = "!set";
                    document.getElementById("customType").value = "!set";
                }

                if (selected == 1) {
                    document.getElementById('weightForm').style.display = 'none';
                    document.getElementById('fitnessForm').style.display = 'block';
                    document.getElementById('customForm').style.display = 'none';
                    document.getElementById('distanceForm').style.display = 'none';

                    document.getElementById("weightType").value = "!set";
                    document.getElementById("fitnessType").value = "set";
                    document.getElementById("customType").value = "!set";
                    document.getElementById("customType").value = "!set";
                }

                if (selected == 2) {
                    document.getElementById('weightForm').style.display = 'none';
                    document.getElementById('fitnessForm').style.display = 'none';
                    document.getElementById('customForm').style.display = 'block';
                    document.getElementById('distanceForm').style.display = 'none';

                    document.getElementById("weightType").value = "!set";
                    document.getElementById("fitnessType").value = "!set";
                    document.getElementById("customType").value = "set";
                    document.getElementById("distanceType").value = "!set";
                }

                if (selected == 3) {
                    document.getElementById('weightForm').style.display = 'none';
                    document.getElementById('fitnessForm').style.display = 'none';
                    document.getElementById('customForm').style.display = 'none';
                    document.getElementById('distanceForm').style.display = 'block';

                    document.getElementById("weightType").value = "!set";
                    document.getElementById("fitnessType").value = "!set";
                    document.getElementById("customType").value = "!set";
                    document.getElementById("distanceType").value = "set";
                }

            }

        </script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Goals</title>
    </head>
    <body>

        <div class="header">
            <div class="wrap">
                <!--<div class="header-left">
                        <div class="logo">
                                <a href="index.jsp">HealthTracker</a>
                        </div>
                </div>-->
                <div class="header-right2">
                    <div class="top-nav">
                        <ul>
                            <%                                                String username = aUser.getUsername();
                                String firstLetter = username.substring(0, 1);
                                String remainingLetters = username.substring(1, username.length());
                                username = firstLetter.toUpperCase() + remainingLetters;

                            %>

                            <li><a href="/HealthTracker/ProfilePageController?pageOwner=<%=aUser.getUsername()%>"><%= username%></a></li>
                            <li><a href="/HealthTracker/DataCaptureController?type=clicked">Update Activities</a></li>
                            <li><a href="/HealthTracker/GoalController?type=clicked">Goals</a></li>
                            <li><a href="/HealthTracker/GroupController?type=clicked">Groups</a></li>
                            <li><a href="/HealthTracker/HistoryController?type=history">History</a></li>
                            <li><a href="/HealthTracker/AchievmentController?type=clicked">Achievements</a></li>
                            <li><a href="/HealthTracker/HomeController?type=clickedEdit">Edit Profile</a></li>
                            <li><a href="/HealthTracker/HistoryController?type=activityStats">Activity Stats</a></li>
                        </ul>
                    </div>

                    <div class="clear"> </div>
                    </ul>
                </div>
                <div class="clear"> </div>
            </div>
            <div class="clear"> </div>
        </div>

        <br />


        <br /><br />

        <div class="user3">
            <h1>Edit Your Profile</h1>
            <div class="user">
                <div class ="profilepic">
                    <img src="http://www.sportspickle.com/wp-content/uploads/2012/11/169e1e366bf6a0a16ed19e3eb0bdf2ba.jpg" alt="some_text">
                </div>

                <ul>Hello <b>${aUser.firstName} ${aUser.lastName}</b></ul>
                <ul><b>${aUser.username}</b></ul>
                <!--<ul>and ... I wont tell anyone but ..your password. (>_>) (<_<) Its <b>${aUser.password}</b> (o_o) <br /><br /></ul>-->
                <!--<ul>Your email is <b>${aUser.email}</b> and you're a <b>${aUser.age}</b> year old <b>${aUser.sex}</b>. <br /><br /></ul>
                <ul>Your weight is <b>${aUser.weight}</b>, your height is <b>${aUser.height}</b> and your resting heart rate is <b>${aUser.heartRate}</b>.<br /><br/></ul>
                <ul>How'd I know all this? SESSIONS! THATS HOW BITCH! \(^_^)/</ul>-->
                <ul><b>Goals Achieved: 198</b></ul>
                <ul>
                    <%  double w = (double) aUser.getWeight();
                        double h = (double) aUser.getHeight();
                        double BMI = 0;

                        if (w != 0 && h != 0) {
                            BMI = w / Math.pow(h, 2);
                        }
                        String message = null;

                        if (BMI < 18.5) {
                            message = "Underweight";
                        } else if (BMI >= 18.5 && BMI < 25) {
                            message = "Healthy";
                        } else if (BMI >= 25 && BMI < 30) {
                            message = "Overweight";
                        } else {
                            message = "Very overweight";
                        }

                    %>
                    Your BMI is <b><%out.println(BMI);%></b>, this means you are <%out.println(message);%></ul><br>
                <hr>
                <br>

                <ul><li><a href="/HealthTracker/HomeController?type=clickedEdit">Edit Profile</a></li></ul>
                <ul>My Groups</ul>
                <ul><p>Uni Group</p><ul>
                        <ul><p>Family Group</p><ul>        
                                <br/>
                                <hr>
                                <br>
                                <ul><h3>Recent Achievements</h3></ul>
                                <ul>Ran 1000 miles</ul>
                                <ul>Walked a mile in my shoes</ul>
                                <ul>Lost 1 KG in weight</ul>

                                </div>

                                <div class="userEditProfile"> 
                                    <h1>Goals</h1>

                                    <!-- Create Goal ... This code should be moved to separate page later -->

                                    <form ACTION="/HealthTracker/GoalController" method="POST">
                                        Select Type of Goal

                                        <select id="goalSelect" onclick="javascript:goalCheck();">
                                            <option value="0">Weight Goal</option>
                                            <option value="1">Fitness Goal</option>
                                            <option value="3">Distance Goal</option>
                                            <option value="2">Custom Goal</option>

                                            <option value="unselected" selected="selected">Select a Goal Type</option>
                                        </select>
                                        <br />

                                        <div id="weightForm" style="display:none">
                                            <input type="text" name="goalName" placeholder="Goal Name" /> <br />
                                            <input type="text" name="targetWeight" placeholder="Target Weight" />
                                            <select name="weightUnit">
                                                <option value="Kg">Kg</option>
                                                <option value="lbs">lbs</option>
                                            </select><br /> 
                                            <label for="Date">Due Date:</label><br />        
                                            day: <input type="text" name="day_weight" size="3" maxlength="2">
                                            month: <select size="1" name="month_weight">
                                                <option value="01">January</option>
                                                <option value="02">February</option>
                                                <option value="03">March</option>
                                                <option value="04">April</option>
                                                <option value="05" >May</option>
                                                <option value="06">June</option>
                                                <option value="07">July</option>
                                                <option value="08">August</option>
                                                <option value="09">September</option>
                                                <option value="10" >October</option>
                                                <option value="11">November</option>
                                                <option value="12" >December</option>
                                                <option value="unselected" selected="selected">Select a month</option>
                                            </select>
                                            year: <select size="1" name="year_weight">
                                                <option value="2014">2014</option>
                                                <option value="2015">2015</option>
                                                <option value="2016">2016</option>
                                                <option value="unselected" selected="selected">Select a year</option>
                                            </select><br />

                                            <input id = "weightType" type = "hidden" value = "W" name = "type1" />
                                            <input class="sub" type="submit" value="Submit" />
                                        </div>



                                        <div id="fitnessForm" style="display:none">
                                            <label for="name">Goal Name:</label>
                                            <input type="text" name="goalName_fitness" placeholder="Goal Name" /> <br />
                                            <label for="targetHeartRate">Target Heart Rate:</label>
                                            <input type="text" name="targetHeartRate" placeholder="Target Heart Rate" /> <br />
                                            <label for="Due Date">Due Date:</label><br />

                                            day: <input type="text" name="day_fitness" size="3" maxlength="2">
                                            month: <select size="1" name="month_fitness">
                                                <option value="01">January</option>
                                                <option value="02">February</option>
                                                <option value="03">March</option>
                                                <option value="04">April</option>
                                                <option value="05" >May</option>
                                                <option value="06">June</option>
                                                <option value="07">July</option>
                                                <option value="08">August</option>
                                                <option value="09">September</option>
                                                <option value="10" >October</option>
                                                <option value="11">November</option>
                                                <option value="12" >December</option>
                                                <option value="unselected" selected="selected">Select a month</option>
                                            </select>
                                            year: <select size="1" name="year_fitness">
                                                <option value="2014">2014</option>
                                                <option value="2015">2015</option>
                                                <option value="2016">2016</option>
                                                <option value="unselected" selected="selected">Select a year</option>
                                            </select><br />
                                            <input id = "fitnessType" type = "hidden" value = "F" name = "type2" />
                                            <input class="sub" type="submit" value="Submit" />
                                        </div>

                                        <div id="customForm" style="display:none">
                                            <input type="text" name="goalName_custom" placeholder="Goal Name" /> <br />
                                            
                                            <input type="text" name="targetWeight_custom" placeholder="Target Weight" value="0" />
                                            <select name="weightUnit">
                                                <option value="Kg">Kg</option>
                                                <option value="lbs">lbs</option>
                                            </select><br /> 
                                            
                                            <input type="text" name="targetHeartRate_custom" placeholder="Target Heart Rate" value="0" /> <br />
                                                                                     
                                            <input type="text" name="targetDistance_custom" placeholder="Target Distance" value="0" /> 
                                            <select name="customDistanceUnit">
                                                <option value="Km">Km</option>
                                                <option value="mi">mi</option>
                                            </select><br />
                                            While: <select size="1" name="activityType_custom">
                                                <option value="W">Walking</option>
                                                <option value="R">Running</option>
                                                <option value="B">Cycling</option>
                                            </select><br />
                                            <label for="Due Date">Due Date:</label><br />        
                                            day: <input type="text" name="day_custom" size="3" maxlength="2">
                                            month: <select size="1" name="month_custom">
                                                <option value="01">January</option>
                                                <option value="02">February</option>
                                                <option value="03">March</option>
                                                <option value="04">April</option>
                                                <option value="05" >May</option>
                                                <option value="06">June</option>
                                                <option value="07">July</option>
                                                <option value="08">August</option>
                                                <option value="09">September</option>
                                                <option value="10" >October</option>
                                                <option value="11">November</option>
                                                <option value="12" >December</option>
                                                <option value="unselected" selected="selected">Select a month</option>
                                            </select>
                                            year: <select size="1" name="year_custom">
                                                <option value="2014">2014</option>
                                                <option value="2015">2015</option>
                                                <option value="2016">2016</option>
                                                <option value="unselected" selected="selected">Select a year</option>
                                            </select><br />
                                            <input id = "customType" type = "hidden" value = "C" name = "type3" />
                                            <input class="sub" type="submit" value="Submit" />


                                            <!-- This hidden field is used to check whether entering a controller from a form or link -->
                                            <input type = "hidden" value = "..." name = "type" /> 


                                        </div>

                                        <div id="distanceForm" style="display:none">
                                            <label for="name">Goal Name:</label>
                                            <input type="text" name="goalName_distance" placeholder="Goal Name" /> <br />
                                            <label for="targetDistance">Target Distance:</label>
                                            <input type="text" name="targetDistance_distance" placeholder="Target Distance" /> 
                                            <select name="distanceDistanceUnit">
                                                <option value="Km">Km</option>
                                                <option value="mi">mi</option>
                                            </select><br />
                                            While: <select size="1" name="activityType">
                                                <option value="W">Walking</option>
                                                <option value="R">Running</option>
                                                <option value="B">Cycling</option>
                                            </select><br />
                                            <label for="Due Date">Due Date:</label><br />        
                                            day: <input type="text" name="day_distance" size="3" maxlength="2">
                                            month: <select size="1" name="month_distance">
                                                <option value="01">January</option>
                                                <option value="02">February</option>
                                                <option value="03">March</option>
                                                <option value="04">April</option>
                                                <option value="05" >May</option>
                                                <option value="06">June</option>
                                                <option value="07">July</option>
                                                <option value="08">August</option>
                                                <option value="09">September</option>
                                                <option value="10" >October</option>
                                                <option value="11">November</option>
                                                <option value="12" >December</option>
                                                <option value="unselected" selected="selected">Select a month</option>
                                            </select>
                                            year: <select size="1" name="year_distance">
                                                <option value="2014">2014</option>
                                                <option value="2015">2015</option>
                                                <option value="2016">2016</option>
                                                <option value="unselected" selected="selected">Select a year</option>
                                            </select><br />
                                            <input id = "distanceType" type = "hidden" value = "C" name = "type4" />
                                            <input class="sub" type="submit" value="Submit" />


                                            <!-- This hidden field is used to check whether entering a controller from a form or link -->
                                            <input type = "hidden" value = "..." name = "type" /> 


                                        </div>
                                    </form>

                                    <a href="/HealthTracker/HomeController?type=clicked"><h3>Home</h3></a> <a style="margin-left: 70%" href="/HealthTracker/LoginController?type=logout">Log out</a><br />

                                    <form action="/HealthTracker/GoalController" method="POST">
                                        <table border="1">                
                                            <tr>
                                                <td>Goal Name</td>
                                                <td>Start Date</td>
                                                <td>Due Date</td>
                                                <td>Goal Type</td>
                                                <td>Percent Complete</td>
                                                <td>Delete Goal?</td>

                                            </tr>
                                            <%
                                                if (session.getAttribute("weightGoals") != null) {

                                                    ArrayList<WeightGoal> wg = (ArrayList<WeightGoal>) session.getAttribute("weightGoals");
                                                    Iterator it = wg.iterator();
                                                    while (it.hasNext()) {

                                                        WeightGoal g = (WeightGoal) it.next();

                                                        double v = (double) g.getTargetWeight();
                                                        double u = (double) aUser.getWeight();
                                                        double completion = 0.0;

                                                        if (u > v) {
                                                            completion = 100 - (((u - v + 1) / u) * 100);
                                                        } else {
                                                            completion = 100 + (((u - v + 1) / u) * 100);
                                                        }


                                            %>

                                            <tr>
                                                <td><a href="/HealthTracker/GoalPageController?goalName=<%=g.getGoalName()%>&goalOwner=${aUser.username}"><%out.println(g.getGoalName());%></a></td>
                                                <td><%=g.getStartDate()%></td>
                                                <td><%=g.getDueDate()%></td>
                                                <td>Weight Goal</td>
                                                <td><%=(int) completion%>%</td>
                                                <td><input value="<%=g.getGoalName()%>" type="checkbox" name="delete"></td>
                                            </tr>


                                            <%
                                                }
                                            %>

                                            <%
                                                }
                                            %>

                                            <%
                                                if (session.getAttribute("distanceGoals") != null) {

                                                    ArrayList<DistanceGoal> wg = (ArrayList<DistanceGoal>) session.getAttribute("distanceGoals");
                                                    Iterator it = wg.iterator();
                                                    while (it.hasNext()) {

                                                        DistanceGoal g = (DistanceGoal) it.next();
                                                        double v = (double) g.getTargetDistance();
                                                        double u = (double) g.getAchievedDistance();
                                                        double completion = 0.0;

                                                        completion = 100 - ((v - u) / v) * 100;

                                                        Object o = g;



                                            %>
                                            <%=o.getClass()%>
                                            <tr>
                                                <td><a href="/HealthTracker/GoalPageController?goalName=<%=g.getGoalName()%>&goalOwner=${aUser.username}"><%out.println(g.getGoalName());%></a></td>
                                                <td><%=g.getStartDate()%></td>
                                                <td><%=g.getDueDate()%></td>
                                                <td>Distance Goal</td>
                                                <td><%=(int) completion%>%</td>
                                                <td><input value="<%=g.getGoalName()%>" type="checkbox" name="delete"></td>
                                            </tr>


                                            <%
                                                }
                                            %>

                                            <%
                                                }
                                            %>

                                            <%
                                                if (session.getAttribute("fitnessGoals") != null) {

                                                    ArrayList<FitnessGoal> wg = (ArrayList<FitnessGoal>) session.getAttribute("fitnessGoals");
                                                    Iterator it = wg.iterator();
                                                    while (it.hasNext()) {

                                                        FitnessGoal g = (FitnessGoal) it.next();
                                                        double v = (double) g.getTargetHeartRate();
                                                        double u = (double) aUser.getHeartRate();
                                                        double completion = 0.0;

                                                        completion = 100 - ((v - u) / v) * 100;

                                                        Object o = g;



                                            %>
                                            <%=o.getClass()%>
                                            <tr>
                                                <td><a href="/HealthTracker/GoalPageController?goalName=<%=g.getGoalName()%>&goalOwner=${aUser.username}"><%out.println(g.getGoalName());%></a></td>
                                                <td><%=g.getStartDate()%></td>
                                                <td><%=g.getDueDate()%></td>
                                                <td>Fitness Goal</td>
                                                <td><%=(int) completion%>%</td>
                                                <td><input value="<%=g.getGoalName()%>" type="checkbox" name="delete"></td>
                                            </tr>


                                            <%
                                                }
                                            %>

                                            <%
                                                }
                                            %>

                                            <%
                                                if (session.getAttribute("customGoals") != null) {

                                                    ArrayList<CustomGoal> wg = (ArrayList<CustomGoal>) session.getAttribute("customGoals");
                                                    Iterator it = wg.iterator();
                                                    while (it.hasNext()) {

                                                        double completion1 = 0.0;
                                                        double completion2 = 0.0;
                                                        double completion3 = 0.0;
                                                        int n = 0;

                                                        CustomGoal g = (CustomGoal) it.next();
                                                        if (g.getTargetDistance() != 0) {
                                                            double v = (double) g.getTargetDistance();
                                                            double u = (double) g.getAchievedDistance();
                                                            completion1 = 0.0;
                                                            completion1 = 100 - ((v - u) / v) * 100;
                                                            n++;
                                                        }

                                                        if (g.getTargetWeight() != 0) {
                                                            double v = (double) g.getTargetWeight();
                                                            double u = (double) aUser.getWeight();
                                                            completion2 = 0.0;
                                                            completion2 = 100 - ((v - u) / v) * 100;
                                                            n++;
                                                        }

                                                        if (g.getTargetHeartRate() != 0) {
                                                            double v = (double) g.getTargetHeartRate();
                                                            double u = (double) aUser.getHeartRate();
                                                            completion3 = 0.0;
                                                            completion3 = 100 - ((v - u) / v) * 100;
                                                            n++;
                                                        }

                                                        double completion = (completion1 + completion2 + completion3) / n;

                                                        Object o = g;



                                            %>
                                            <%=o.getClass()%>
                                            <tr>
                                                <td><a href="/HealthTracker/GoalPageController?goalName=<%=g.getGoalName()%>&goalOwner=${aUser.username}"><%out.println(g.getGoalName());%></a></td>
                                                <td><%=g.getStartDate()%></td>
                                                <td><%=g.getDueDate()%></td>
                                                <td>Custom Goal</td>
                                                <td><%=(int) completion%>%</td>
                                                <td><input value="<%=g.getGoalName()%>" type="checkbox" name="delete"></td>
                                            </tr>


                                            <%
                                                }
                                            %>

                                            <%
                                                }
                                            %>

                                        </table>
                                        <input type="submit" value="Delete" name ="type">
                                    </form>
                                </div>
                                </div>




                                </body>
                                </html>
