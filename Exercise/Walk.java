package Model;

import Utilities.DBAccess;
import java.sql.Connection;
import java.sql.Statement;
//import java.util.Date;

/**
 *
 * @author Takomborerwa
 */
public class Walk extends Activity {

    private int heartRate;
    
    
    public Walk(int distance, int duration, String username, String sex, float weight, int age, int heartRate) {
        this.distance = distance;
        this.duration = duration;
        this.username = username;
        this.sex = sex;
        this.weight = weight;
        this.age = age;
        this.heartRate = heartRate;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }
    
    /**
     * A method to calculate the amount of calories burnt during a Walk.
     * 
     * @return Amount of calories burnt during a Walk rounded 
     * down to a whole number.
     */
    @Override
    public int getCaloriesBurnt() {

        double calories = 0;

        if (sex.equalsIgnoreCase("m")) {
            double a = (double) (age * 0.20175);
            double w = (double) (weight * 0.09036);
            double h = (double) (heartRate * 0.6309);
            calories = ((a + w + h) - 55.0969) + (duration / 4.184);            
        }

        else if (sex.equalsIgnoreCase("f")) {
            double a = (double) (age * 0.074);
            double w = (double) (weight * 0.05741);
            double h = (double) (heartRate * 0.4472);
            calories = ((a + w + h) - 55.0969) + (duration / 20.4022);
        }
        
        return (int) calories;

    }
    
    
    public boolean persist(){
        try {
            Connection con = DBAccess.getConnection();

            Statement statement = con.createStatement();
            
            java.util.Date utilDate = new java.util.Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            String sql = "INSERT INTO Activity VALUES('" + username + "', "
                    + distance + ", " + duration + ", " + 0 + ", " + getCaloriesBurnt()
                    + ", '" + sqlDate + "')";

            statement.executeUpdate(sql);

            con.close();

        } catch (Exception e) {
            return false;
        }

        return true;
    }
    
    

}
