/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Takomborerwa
 */
public class Run extends Activity {
 
    private int heartRate;
    
     public Run (int distance, int duration, int speed, String sex, float weight, int age, int heartRate){
        this.distance = distance;
        this.duration = duration;
        this.speed = speed;
        this.sex = sex;
        this.weight = weight;
        this.age = age;
        this.heartRate = heartRate;
    } 




    /**
     * A method to calculate the amount of calories burnt during a Run.
     * 
     * @return Amount of calories burnt during a Run rounded 
     * down to a whole number.
     */
    @Override
    public int getCaloriesBurnt() {

        // Currently using the same formula as that of a Walk.
        // If we're being technical, a run cant use the same formula as a walk
        // because this imploes that the intensity of a workout has no effect
        // on the outcome. Essentially this is saying a 5 min walk burns the same
        // calories as a 5 min run.
        // Might look into changing that if we can.
        
        double calories = 0;

        if (sex.equalsIgnoreCase("m")) {
            double a = (double) (age * 0.20175);
            double w = (double) (weight * 0.09036);
            double h = (double) (heartRate * 0.6309);
            calories = ((a + w + h) - 55.0969) + (duration / 4.184);    
        }

        else if (sex.equalsIgnoreCase("f")) {
            double a = (double) (age * 0.074);
            double w = (double) (weight * 0.05741);
            double h = (double) (heartRate * 0.4472);
            calories = ((a + w + h) - 55.0969) + (duration / 20.4022);
        }
        
        return (int) calories;

    }
     
}
