/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Takomborerwa
 */
public class Exercise implements Iterable {

    private ArrayList<Activity> activities;

    public Exercise() {
        activities = new ArrayList<Activity>();
    }

    public Exercise(Activity a) {
        activities = new ArrayList<Activity>();
        activities.add(a);
    }
    
    public void addActivity(Activity a){
        activities.add(a);
    }
  
    public void removeActivity(Activity a) {
        activities.remove(a);
    }
    
    public int sizeOfSession() {
        return activities.size();
    }
 
    public void setActivities(ArrayList<Activity> activities) {
        this.activities = activities;
    }
    
    @Override
    public Iterator<Activity> iterator() {
        Iterator<Activity> it = activities.iterator();
        return it;
    }

}
