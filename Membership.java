

public class Membership {

	public enum Role{R, A}; // R for regular, A for Admin.
	
	private Role role;
	private int permissionsLevel;
	
	// Interacts with db to get users membership details
	public Membership(String username){
		
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public int getPermissionsLevel() {
		return permissionsLevel;
	}

	public void setPermissionsLevel(int permissionsLevel) {
		this.permissionsLevel = permissionsLevel;
	}
	
	
	
}
