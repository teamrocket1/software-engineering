package Goals;

import java.util.Date;

public abstract class Goal {
	
	protected Date startDate;
	protected Date dueDate;
	protected boolean status;
	protected Diet diet;

	abstract boolean isCompleted();


}
