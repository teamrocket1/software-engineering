package Goals;

public class Fitnessgoal extends Goal {
	
	private int targetHeartRate;
	private Exercise exercise;

	@Override
	public boolean isCompleted() {
		// TODO Auto-generated method stub
		return false;
	}

	public int getTargetHeartRate() {
		return targetHeartRate;
	}

	public void setTargetHeartRate(int targetHeartRate) {
		this.targetHeartRate = targetHeartRate;
	}

	public Exercise getExercise() {
		return exercise;
	}

	public void setExercise(Exercise exercise) {
		this.exercise = exercise;
	}
	
	

}
