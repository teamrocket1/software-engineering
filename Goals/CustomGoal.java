package Goals;

import java.util.Date;

public class CustomGoal extends Goal {

	private Date startDate;
	private Date dueDate;
	private int targetWeight;
	private int targetBMI;
	private Exercise excersize ;
	
	@Override
	boolean isCompleted() {
		// TODO Auto-generated method stub
		return false;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public int getTargetWeight() {
		return targetWeight;
	}

	public void setTargetWeight(int targetWeight) {
		this.targetWeight = targetWeight;
	}

	public int getTargetBMI() {
		return targetBMI;
	}

	public void setTargetBMI(int targetBMI) {
		this.targetBMI = targetBMI;
	}

	public Exercise getExcersize() {
		return excersize;
	}

	public void setExcersize(Exercise excersize) {
		this.excersize = excersize;
	}
	
	

}
