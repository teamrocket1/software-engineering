import java.util.ArrayList;


public class User {

	private String firstName;
	private String lastName;
	private String username;
	private String password;
	private String email;
	private int weight;
	private int height;
	private int heartRate;
	private ArrayList<Goal> goals;
	
	public User(){
		
	}
	
	public boolean login(String u, String p){
		return false;
	}
	
	public int getBMI(){
		if(weight != 0 && height != 0){
			return (weight/height * height);
		}
		return 0;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getHeartRate() {
		return heartRate;
	}

	public void setHeartRate(int heartRate) {
		this.heartRate = heartRate;
	}

	public ArrayList<Goal> getGoals() {
		return goals;
	}

	public void setGoals(ArrayList<Goal> goals) {
		this.goals = goals;
	}
	
	public void addGoal(Goal g){
		goals.add(g);
	}
	
	public void removeGoal(Goal g){
		goals.remove(g);
	}
	
	
	
	
}
