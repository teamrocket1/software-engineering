import java.util.ArrayList;


public class Group {

	private String groupName;
	private ArrayList<User> members;
	private Goal groupGoal;
	
	// Creates a group using details from the db for a given group name.
	public Group(String name){
		groupName = name;
		members = new ArrayList<User>();
		// Fill arraylist with db info
	}
	
	public void setGoal(Goal goal){
		groupGoal = goal;
	}
	
	public void addMember(User u){
		
	}
	
	public void removeMember(User u){
		
	}
	
	public void setAdmin(String username){
		
	}
	
	public void cancelGroupGoal(){
		
	}
	
}
